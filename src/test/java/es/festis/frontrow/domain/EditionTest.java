package es.festis.frontrow.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import es.festis.frontrow.web.rest.TestUtil;

class EditionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Edition.class);
        Edition edition1 = new Edition();
        edition1.setId(1L);
        Edition edition2 = new Edition();
        edition2.setId(edition1.getId());
        assertThat(edition1).isEqualTo(edition2);
        edition2.setId(2L);
        assertThat(edition1).isNotEqualTo(edition2);
        edition1.setId(null);
        assertThat(edition1).isNotEqualTo(edition2);
    }
}
