package es.festis.frontrow.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import es.festis.frontrow.FrontrowApp;
import es.festis.frontrow.domain.Festival;
import es.festis.frontrow.repository.FestivalRepository;
import es.festis.frontrow.security.AuthoritiesConstants;
import es.festis.frontrow.service.FestivalService;
import es.festis.frontrow.service.dto.FestivalDTO;
import es.festis.frontrow.service.mapper.FestivalMapper;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link FestivalResource} REST controller.
 */
@SpringBootTest(classes = FrontrowApp.class)

@AutoConfigureMockMvc
@WithMockUser
class FestivalResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";

    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final byte[] DEFAULT_LOGO = TestUtil.createByteArray(1, "0");

    private static final byte[] UPDATED_LOGO = TestUtil.createByteArray(1, "1");

    private static final String DEFAULT_LOGO_CONTENT_TYPE = "image/jpg";

    private static final String UPDATED_LOGO_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_WEB = "AAAAAAAAAA";

    private static final String UPDATED_WEB = "BBBBBBBBBB";

    private static final String DEFAULT_FACEBOOK = "AAAAAAAAAA";

    private static final String UPDATED_FACEBOOK = "BBBBBBBBBB";

    private static final String DEFAULT_TWITTER = "AAAAAAAAAA";

    private static final String UPDATED_TWITTER = "BBBBBBBBBB";

    private static final String DEFAULT_INSTAGRAM = "AAAAAAAAAA";

    private static final String UPDATED_INSTAGRAM = "BBBBBBBBBB";

    @Autowired
    private FestivalRepository festivalRepository;

    @Autowired
    private FestivalMapper festivalMapper;

    @Autowired
    private FestivalService festivalService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFestivalMockMvc;

    private Festival festival;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if they test an entity which requires the current entity.
     */
    public static Festival createEntity(EntityManager em) {
        Festival festival = new Festival()
            .name(DEFAULT_NAME)
            .logo(DEFAULT_LOGO)
            .logoContentType(DEFAULT_LOGO_CONTENT_TYPE)
            .web(DEFAULT_WEB)
            .facebook(DEFAULT_FACEBOOK)
            .twitter(DEFAULT_TWITTER)
            .instagram(DEFAULT_INSTAGRAM);
        return festival;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if they test an entity which requires the current entity.
     */
    public static Festival createUpdatedEntity(EntityManager em) {
        Festival festival = new Festival()
            .name(UPDATED_NAME)
            .logo(UPDATED_LOGO)
            .logoContentType(UPDATED_LOGO_CONTENT_TYPE)
            .web(UPDATED_WEB)
            .facebook(UPDATED_FACEBOOK)
            .twitter(UPDATED_TWITTER)
            .instagram(UPDATED_INSTAGRAM);
        return festival;
    }

    @BeforeEach
    void initTest() {
        festival = createEntity(em);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void createFestival() throws Exception {
        int databaseSizeBeforeCreate = festivalRepository.findAll().size();

        // Create the Festival
        FestivalDTO festivalDTO = festivalMapper.toDto(festival);
        restFestivalMockMvc.perform(post("/api/festivals")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(festivalDTO)))
            .andExpect(status().isCreated());

        // Validate the Festival in the database
        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeCreate + 1);
        Festival testFestival = festivalList.get(festivalList.size() - 1);
        assertThat(testFestival.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFestival.getLogo()).isEqualTo(DEFAULT_LOGO);
        assertThat(testFestival.getLogoContentType()).isEqualTo(DEFAULT_LOGO_CONTENT_TYPE);
        assertThat(testFestival.getWeb()).isEqualTo(DEFAULT_WEB);
        assertThat(testFestival.getFacebook()).isEqualTo(DEFAULT_FACEBOOK);
        assertThat(testFestival.getTwitter()).isEqualTo(DEFAULT_TWITTER);
        assertThat(testFestival.getInstagram()).isEqualTo(DEFAULT_INSTAGRAM);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void createFestivalWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = festivalRepository.findAll().size();

        // Create the Festival with an existing ID
        festival.setId(1L);
        FestivalDTO festivalDTO = festivalMapper.toDto(festival);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFestivalMockMvc.perform(post("/api/festivals")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(festivalDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Festival in the database
        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = festivalRepository.findAll().size();
        // set the field null
        festival.setName(null);

        // Create the Festival, which fails.
        FestivalDTO festivalDTO = festivalMapper.toDto(festival);

        restFestivalMockMvc.perform(post("/api/festivals")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(festivalDTO)))
            .andExpect(status().isBadRequest());

        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllFestivals() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);

        // Get all the festivalList
        restFestivalMockMvc.perform(get("/api/festivals?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(festival.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].logoContentType").value(hasItem(DEFAULT_LOGO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].logo").value(hasItem(Base64Utils.encodeToString(DEFAULT_LOGO))))
            .andExpect(jsonPath("$.[*].web").value(hasItem(DEFAULT_WEB)))
            .andExpect(jsonPath("$.[*].facebook").value(hasItem(DEFAULT_FACEBOOK)))
            .andExpect(jsonPath("$.[*].twitter").value(hasItem(DEFAULT_TWITTER)))
            .andExpect(jsonPath("$.[*].instagram").value(hasItem(DEFAULT_INSTAGRAM)));
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getFestivalNames() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);

        // Get all the festival names list
        restFestivalMockMvc.perform(get("/api/festivals/names"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*]").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void followFestival() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);

        // Follow
        restFestivalMockMvc.perform(post("/api/festivals/{name}/follow", festival.getName()))
            .andExpect(status().isNoContent());
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void unfollowFestival() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);

        // Unfollow
        restFestivalMockMvc.perform(post("/api/festivals/{name}/unfollow", festival.getName()))
            .andExpect(status().isNoContent());
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getFollowedFestivals() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);

        // Get all the festival names list
        restFestivalMockMvc.perform(get("/api/festivals/followed"))
            .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getFestival() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);

        // Get the festival
        restFestivalMockMvc.perform(get("/api/festivals/{id}", festival.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(festival.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.logoContentType").value(DEFAULT_LOGO_CONTENT_TYPE))
            .andExpect(jsonPath("$.logo").value(Base64Utils.encodeToString(DEFAULT_LOGO)))
            .andExpect(jsonPath("$.web").value(DEFAULT_WEB))
            .andExpect(jsonPath("$.facebook").value(DEFAULT_FACEBOOK))
            .andExpect(jsonPath("$.twitter").value(DEFAULT_TWITTER))
            .andExpect(jsonPath("$.instagram").value(DEFAULT_INSTAGRAM));
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getNonExistingFestival() throws Exception {
        // Get the festival
        restFestivalMockMvc.perform(get("/api/festivals/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void updateFestival() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);

        int databaseSizeBeforeUpdate = festivalRepository.findAll().size();

        // Update the festival
        Festival updatedFestival = festivalRepository.findById(festival.getId()).get();
        // Disconnect from session so that the updates on updatedFestival are not directly saved in db
        em.detach(updatedFestival);
        updatedFestival
            .name(UPDATED_NAME)
            .logo(UPDATED_LOGO)
            .logoContentType(UPDATED_LOGO_CONTENT_TYPE)
            .web(UPDATED_WEB)
            .facebook(UPDATED_FACEBOOK)
            .twitter(UPDATED_TWITTER)
            .instagram(UPDATED_INSTAGRAM);
        FestivalDTO festivalDTO = festivalMapper.toDto(updatedFestival);

        restFestivalMockMvc.perform(put("/api/festivals")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(festivalDTO)))
            .andExpect(status().isOk());

        // Validate the Festival in the database
        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeUpdate);
        Festival testFestival = festivalList.get(festivalList.size() - 1);
        assertThat(testFestival.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFestival.getLogo()).isEqualTo(UPDATED_LOGO);
        assertThat(testFestival.getLogoContentType()).isEqualTo(UPDATED_LOGO_CONTENT_TYPE);
        assertThat(testFestival.getWeb()).isEqualTo(UPDATED_WEB);
        assertThat(testFestival.getFacebook()).isEqualTo(UPDATED_FACEBOOK);
        assertThat(testFestival.getTwitter()).isEqualTo(UPDATED_TWITTER);
        assertThat(testFestival.getInstagram()).isEqualTo(UPDATED_INSTAGRAM);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void updateNonExistingFestival() throws Exception {
        int databaseSizeBeforeUpdate = festivalRepository.findAll().size();

        // Create the Festival
        FestivalDTO festivalDTO = festivalMapper.toDto(festival);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFestivalMockMvc.perform(put("/api/festivals")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(festivalDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Festival in the database
        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void deleteFestival() throws Exception {
        // Initialize the database
        festivalRepository.saveAndFlush(festival);

        int databaseSizeBeforeDelete = festivalRepository.findAll().size();

        // Delete the festival
        restFestivalMockMvc.perform(delete("/api/festivals/{id}", festival.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Festival> festivalList = festivalRepository.findAll();
        assertThat(festivalList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
