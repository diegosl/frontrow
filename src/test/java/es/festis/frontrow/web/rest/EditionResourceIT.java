package es.festis.frontrow.web.rest;

import com.google.common.collect.Lists;
import es.festis.frontrow.FrontrowApp;
import es.festis.frontrow.domain.Edition;
import es.festis.frontrow.domain.Post;
import es.festis.frontrow.domain.Artist;
import es.festis.frontrow.domain.Festival;
import es.festis.frontrow.repository.EditionRepository;
import es.festis.frontrow.security.AuthoritiesConstants;
import es.festis.frontrow.service.EditionService;
import es.festis.frontrow.service.dto.*;
import es.festis.frontrow.service.mapper.EditionFestivalMapper;
import es.festis.frontrow.service.mapper.EditionMapper;
import es.festis.frontrow.service.EditionQueryService;
import es.festis.frontrow.service.mapper.FestivalMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EditionResource} REST controller.
 */
@SpringBootTest(classes = FrontrowApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class EditionResourceIT {

    private static final String DEFAULT_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION = "BBBBBBBBBB";

    private static final Float DEFAULT_LATITUDE = 1F;
    private static final Float UPDATED_LATITUDE = 2F;
    private static final Float SMALLER_LATITUDE = 1F - 1F;

    private static final Float DEFAULT_LONGITUDE = 1F;
    private static final Float UPDATED_LONGITUDE = 2F;
    private static final Float SMALLER_LONGITUDE = 1F - 1F;

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_START_DATE = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_END_DATE = LocalDate.ofEpochDay(-1L);

    private static final byte[] DEFAULT_POSTER = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_POSTER = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_POSTER_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_POSTER_CONTENT_TYPE = "image/png";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_SPOTIFY_PLAYLIST = "AAAAAAAAAA";
    private static final String UPDATED_SPOTIFY_PLAYLIST = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CANCELED = false;
    private static final Boolean UPDATED_CANCELED = true;

    private static final Boolean DEFAULT_VISIBLE = true;
    private static final Boolean UPDATED_VISIBLE = false;

    @Autowired
    private EditionRepository editionRepository;

    @Mock
    private EditionRepository editionRepositoryMock;

    @Autowired
    private EditionMapper editionMapper;

    @Autowired
    private FestivalMapper festivalMapper;

    @Autowired
    private EditionFestivalMapper editionFestivalMapper;

    @Mock
    private EditionService editionServiceMock;

    @Autowired
    private EditionService editionService;

    @Autowired
    private EditionQueryService editionQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEditionMockMvc;

    private Edition edition;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Edition createEntity(EntityManager em) {
        Edition edition = new Edition()
            .location(DEFAULT_LOCATION)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .poster(DEFAULT_POSTER)
            .posterContentType(DEFAULT_POSTER_CONTENT_TYPE)
            .description(DEFAULT_DESCRIPTION)
            .spotifyPlaylist(DEFAULT_SPOTIFY_PLAYLIST)
            .canceled(DEFAULT_CANCELED)
            .visible(DEFAULT_VISIBLE);
        // Add required entity
        Festival festival;
        if (TestUtil.findAll(em, Festival.class).isEmpty()) {
            festival = FestivalResourceIT.createEntity(em);
            em.persist(festival);
            em.flush();
        } else {
            festival = TestUtil.findAll(em, Festival.class).get(0);
        }
        edition.setFestival(festival);
        return edition;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Edition createUpdatedEntity(EntityManager em) {
        Edition edition = new Edition()
            .location(UPDATED_LOCATION)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .poster(UPDATED_POSTER)
            .posterContentType(UPDATED_POSTER_CONTENT_TYPE)
            .description(UPDATED_DESCRIPTION)
            .spotifyPlaylist(UPDATED_SPOTIFY_PLAYLIST)
            .canceled(UPDATED_CANCELED)
            .visible(UPDATED_VISIBLE);
        // Add required entity
        Festival festival;
        if (TestUtil.findAll(em, Festival.class).isEmpty()) {
            festival = FestivalResourceIT.createUpdatedEntity(em);
            em.persist(festival);
            em.flush();
        } else {
            festival = TestUtil.findAll(em, Festival.class).get(0);
        }
        edition.setFestival(festival);
        return edition;
    }

    @BeforeEach
    void initTest() {
        edition = createEntity(em);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void createEdition() throws Exception {
        int databaseSizeBeforeCreate = editionRepository.findAll().size();
        // Create the Edition
        EditionDTO editionDTO = editionMapper.toDto(edition);
        restEditionMockMvc.perform(post("/api/editions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(editionDTO)))
            .andExpect(status().isCreated());

        // Validate the Edition in the database
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeCreate + 1);
        Edition testEdition = editionList.get(editionList.size() - 1);
        assertThat(testEdition.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testEdition.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testEdition.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testEdition.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testEdition.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testEdition.getPoster()).isEqualTo(DEFAULT_POSTER);
        assertThat(testEdition.getPosterContentType()).isEqualTo(DEFAULT_POSTER_CONTENT_TYPE);
        assertThat(testEdition.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEdition.getSpotifyPlaylist()).isEqualTo(DEFAULT_SPOTIFY_PLAYLIST);
        assertThat(testEdition.isCanceled()).isEqualTo(DEFAULT_CANCELED);
        assertThat(testEdition.isVisible()).isEqualTo(DEFAULT_VISIBLE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void createEditionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = editionRepository.findAll().size();

        // Create the Edition with an existing ID
        edition.setId(1L);
        EditionDTO editionDTO = editionMapper.toDto(edition);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEditionMockMvc.perform(post("/api/editions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(editionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Edition in the database
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void createFestivalEdition() throws Exception {
        int databaseSizeBeforeCreate = editionRepository.findAll().size();
        // Create the Edition
        EditionFestivalDTO editionFestivalDTO = editionFestivalMapper.toDto(edition);
        restEditionMockMvc.perform(post("/api/editions/festival")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(editionFestivalDTO)))
            .andExpect(status().isCreated());

        // Validate the Edition in the database
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeCreate + 1);
        Edition testEdition = editionList.get(editionList.size() - 1);
        assertThat(testEdition.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testEdition.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testEdition.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testEdition.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testEdition.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testEdition.getPoster()).isEqualTo(DEFAULT_POSTER);
        assertThat(testEdition.getPosterContentType()).isEqualTo(DEFAULT_POSTER_CONTENT_TYPE);
        assertThat(testEdition.isVisible()).isEqualTo(Boolean.FALSE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void createFestivalEditionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = editionRepository.findAll().size();
        // Create the Edition
        EditionFestivalDTO editionFestivalDTO = editionFestivalMapper.toDto(edition);
        editionFestivalDTO.setId(1L);
        restEditionMockMvc.perform(post("/api/editions/festival")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(editionFestivalDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Edition in the database
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void checkStartDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = editionRepository.findAll().size();
        // set the field null
        edition.setStartDate(null);

        // Create the Edition, which fails.
        EditionDTO editionDTO = editionMapper.toDto(edition);


        restEditionMockMvc.perform(post("/api/editions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(editionDTO)))
            .andExpect(status().isBadRequest());

        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditions() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList
        restEditionMockMvc.perform(get("/api/editions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(edition.getId().intValue())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].posterContentType").value(hasItem(DEFAULT_POSTER_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].poster").value(hasItem(Base64Utils.encodeToString(DEFAULT_POSTER))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].spotifyPlaylist").value(hasItem(DEFAULT_SPOTIFY_PLAYLIST)))
            .andExpect(jsonPath("$.[*].canceled").value(hasItem(DEFAULT_CANCELED.booleanValue())))
            .andExpect(jsonPath("$.[*].visible").value(hasItem(DEFAULT_VISIBLE.booleanValue())));
    }

    @SuppressWarnings({"unchecked"})
    public void getAllEditionsWithEagerRelationshipsIsEnabled() throws Exception {
        when(editionServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restEditionMockMvc.perform(get("/api/editions?eagerload=true"))
            .andExpect(status().isOk());

        verify(editionServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllEditionsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(editionServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restEditionMockMvc.perform(get("/api/editions?eagerload=true"))
            .andExpect(status().isOk());

        verify(editionServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getEdition() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get the edition
        restEditionMockMvc.perform(get("/api/editions/{id}", edition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(edition.getId().intValue()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.posterContentType").value(DEFAULT_POSTER_CONTENT_TYPE))
            .andExpect(jsonPath("$.poster").value(Base64Utils.encodeToString(DEFAULT_POSTER)))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.spotifyPlaylist").value(DEFAULT_SPOTIFY_PLAYLIST))
            .andExpect(jsonPath("$.canceled").value(DEFAULT_CANCELED.booleanValue()))
            .andExpect(jsonPath("$.visible").value(DEFAULT_VISIBLE.booleanValue()));
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getFollowedEditions() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get the edition
        restEditionMockMvc.perform(get("/api/editions/followed/5"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getEditionsToValidate() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get the edition
        restEditionMockMvc.perform(get("/api/editions/validate"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));

        // Check, that the count call also returns 0
        restEditionMockMvc.perform(get("/api/editions/validate/count"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getEditionsByIdFiltering() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        Long id = edition.getId();

        defaultEditionShouldBeFound("id.equals=" + id);
        defaultEditionShouldNotBeFound("id.notEquals=" + id);

        defaultEditionShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultEditionShouldNotBeFound("id.greaterThan=" + id);

        defaultEditionShouldBeFound("id.lessThanOrEqual=" + id);
        defaultEditionShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLocationIsEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where location equals to DEFAULT_LOCATION
        defaultEditionShouldBeFound("location.equals=" + DEFAULT_LOCATION);

        // Get all the editionList where location equals to UPDATED_LOCATION
        defaultEditionShouldNotBeFound("location.equals=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLocationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where location not equals to DEFAULT_LOCATION
        defaultEditionShouldNotBeFound("location.notEquals=" + DEFAULT_LOCATION);

        // Get all the editionList where location not equals to UPDATED_LOCATION
        defaultEditionShouldBeFound("location.notEquals=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLocationIsInShouldWork() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where location in DEFAULT_LOCATION or UPDATED_LOCATION
        defaultEditionShouldBeFound("location.in=" + DEFAULT_LOCATION + "," + UPDATED_LOCATION);

        // Get all the editionList where location equals to UPDATED_LOCATION
        defaultEditionShouldNotBeFound("location.in=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLocationIsNullOrNotNull() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where location is not null
        defaultEditionShouldBeFound("location.specified=true");

        // Get all the editionList where location is null
        defaultEditionShouldNotBeFound("location.specified=false");
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLocationContainsSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where location contains DEFAULT_LOCATION
        defaultEditionShouldBeFound("location.contains=" + DEFAULT_LOCATION);

        // Get all the editionList where location contains UPDATED_LOCATION
        defaultEditionShouldNotBeFound("location.contains=" + UPDATED_LOCATION);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLocationNotContainsSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where location does not contain DEFAULT_LOCATION
        defaultEditionShouldNotBeFound("location.doesNotContain=" + DEFAULT_LOCATION);

        // Get all the editionList where location does not contain UPDATED_LOCATION
        defaultEditionShouldBeFound("location.doesNotContain=" + UPDATED_LOCATION);
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLatitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where latitude equals to DEFAULT_LATITUDE
        defaultEditionShouldBeFound("latitude.equals=" + DEFAULT_LATITUDE);

        // Get all the editionList where latitude equals to UPDATED_LATITUDE
        defaultEditionShouldNotBeFound("latitude.equals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLatitudeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where latitude not equals to DEFAULT_LATITUDE
        defaultEditionShouldNotBeFound("latitude.notEquals=" + DEFAULT_LATITUDE);

        // Get all the editionList where latitude not equals to UPDATED_LATITUDE
        defaultEditionShouldBeFound("latitude.notEquals=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLatitudeIsInShouldWork() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where latitude in DEFAULT_LATITUDE or UPDATED_LATITUDE
        defaultEditionShouldBeFound("latitude.in=" + DEFAULT_LATITUDE + "," + UPDATED_LATITUDE);

        // Get all the editionList where latitude equals to UPDATED_LATITUDE
        defaultEditionShouldNotBeFound("latitude.in=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLatitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where latitude is not null
        defaultEditionShouldBeFound("latitude.specified=true");

        // Get all the editionList where latitude is null
        defaultEditionShouldNotBeFound("latitude.specified=false");
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLatitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where latitude is greater than or equal to DEFAULT_LATITUDE
        defaultEditionShouldBeFound("latitude.greaterThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the editionList where latitude is greater than or equal to UPDATED_LATITUDE
        defaultEditionShouldNotBeFound("latitude.greaterThanOrEqual=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLatitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where latitude is less than or equal to DEFAULT_LATITUDE
        defaultEditionShouldBeFound("latitude.lessThanOrEqual=" + DEFAULT_LATITUDE);

        // Get all the editionList where latitude is less than or equal to SMALLER_LATITUDE
        defaultEditionShouldNotBeFound("latitude.lessThanOrEqual=" + SMALLER_LATITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLatitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where latitude is less than DEFAULT_LATITUDE
        defaultEditionShouldNotBeFound("latitude.lessThan=" + DEFAULT_LATITUDE);

        // Get all the editionList where latitude is less than UPDATED_LATITUDE
        defaultEditionShouldBeFound("latitude.lessThan=" + UPDATED_LATITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLatitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where latitude is greater than DEFAULT_LATITUDE
        defaultEditionShouldNotBeFound("latitude.greaterThan=" + DEFAULT_LATITUDE);

        // Get all the editionList where latitude is greater than SMALLER_LATITUDE
        defaultEditionShouldBeFound("latitude.greaterThan=" + SMALLER_LATITUDE);
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLongitudeIsEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where longitude equals to DEFAULT_LONGITUDE
        defaultEditionShouldBeFound("longitude.equals=" + DEFAULT_LONGITUDE);

        // Get all the editionList where longitude equals to UPDATED_LONGITUDE
        defaultEditionShouldNotBeFound("longitude.equals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLongitudeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where longitude not equals to DEFAULT_LONGITUDE
        defaultEditionShouldNotBeFound("longitude.notEquals=" + DEFAULT_LONGITUDE);

        // Get all the editionList where longitude not equals to UPDATED_LONGITUDE
        defaultEditionShouldBeFound("longitude.notEquals=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLongitudeIsInShouldWork() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where longitude in DEFAULT_LONGITUDE or UPDATED_LONGITUDE
        defaultEditionShouldBeFound("longitude.in=" + DEFAULT_LONGITUDE + "," + UPDATED_LONGITUDE);

        // Get all the editionList where longitude equals to UPDATED_LONGITUDE
        defaultEditionShouldNotBeFound("longitude.in=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLongitudeIsNullOrNotNull() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where longitude is not null
        defaultEditionShouldBeFound("longitude.specified=true");

        // Get all the editionList where longitude is null
        defaultEditionShouldNotBeFound("longitude.specified=false");
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLongitudeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where longitude is greater than or equal to DEFAULT_LONGITUDE
        defaultEditionShouldBeFound("longitude.greaterThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the editionList where longitude is greater than or equal to UPDATED_LONGITUDE
        defaultEditionShouldNotBeFound("longitude.greaterThanOrEqual=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLongitudeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where longitude is less than or equal to DEFAULT_LONGITUDE
        defaultEditionShouldBeFound("longitude.lessThanOrEqual=" + DEFAULT_LONGITUDE);

        // Get all the editionList where longitude is less than or equal to SMALLER_LONGITUDE
        defaultEditionShouldNotBeFound("longitude.lessThanOrEqual=" + SMALLER_LONGITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLongitudeIsLessThanSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where longitude is less than DEFAULT_LONGITUDE
        defaultEditionShouldNotBeFound("longitude.lessThan=" + DEFAULT_LONGITUDE);

        // Get all the editionList where longitude is less than UPDATED_LONGITUDE
        defaultEditionShouldBeFound("longitude.lessThan=" + UPDATED_LONGITUDE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByLongitudeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where longitude is greater than DEFAULT_LONGITUDE
        defaultEditionShouldNotBeFound("longitude.greaterThan=" + DEFAULT_LONGITUDE);

        // Get all the editionList where longitude is greater than SMALLER_LONGITUDE
        defaultEditionShouldBeFound("longitude.greaterThan=" + SMALLER_LONGITUDE);
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByStartDateIsEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where startDate equals to DEFAULT_START_DATE
        defaultEditionShouldBeFound("startDate.equals=" + DEFAULT_START_DATE);

        // Get all the editionList where startDate equals to UPDATED_START_DATE
        defaultEditionShouldNotBeFound("startDate.equals=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByStartDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where startDate not equals to DEFAULT_START_DATE
        defaultEditionShouldNotBeFound("startDate.notEquals=" + DEFAULT_START_DATE);

        // Get all the editionList where startDate not equals to UPDATED_START_DATE
        defaultEditionShouldBeFound("startDate.notEquals=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByStartDateIsInShouldWork() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where startDate in DEFAULT_START_DATE or UPDATED_START_DATE
        defaultEditionShouldBeFound("startDate.in=" + DEFAULT_START_DATE + "," + UPDATED_START_DATE);

        // Get all the editionList where startDate equals to UPDATED_START_DATE
        defaultEditionShouldNotBeFound("startDate.in=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByStartDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where startDate is not null
        defaultEditionShouldBeFound("startDate.specified=true");

        // Get all the editionList where startDate is null
        defaultEditionShouldNotBeFound("startDate.specified=false");
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByStartDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where startDate is greater than or equal to DEFAULT_START_DATE
        defaultEditionShouldBeFound("startDate.greaterThanOrEqual=" + DEFAULT_START_DATE);

        // Get all the editionList where startDate is greater than or equal to UPDATED_START_DATE
        defaultEditionShouldNotBeFound("startDate.greaterThanOrEqual=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByStartDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where startDate is less than or equal to DEFAULT_START_DATE
        defaultEditionShouldBeFound("startDate.lessThanOrEqual=" + DEFAULT_START_DATE);

        // Get all the editionList where startDate is less than or equal to SMALLER_START_DATE
        defaultEditionShouldNotBeFound("startDate.lessThanOrEqual=" + SMALLER_START_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByStartDateIsLessThanSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where startDate is less than DEFAULT_START_DATE
        defaultEditionShouldNotBeFound("startDate.lessThan=" + DEFAULT_START_DATE);

        // Get all the editionList where startDate is less than UPDATED_START_DATE
        defaultEditionShouldBeFound("startDate.lessThan=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByStartDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where startDate is greater than DEFAULT_START_DATE
        defaultEditionShouldNotBeFound("startDate.greaterThan=" + DEFAULT_START_DATE);

        // Get all the editionList where startDate is greater than SMALLER_START_DATE
        defaultEditionShouldBeFound("startDate.greaterThan=" + SMALLER_START_DATE);
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByEndDateIsEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where endDate equals to DEFAULT_END_DATE
        defaultEditionShouldBeFound("endDate.equals=" + DEFAULT_END_DATE);

        // Get all the editionList where endDate equals to UPDATED_END_DATE
        defaultEditionShouldNotBeFound("endDate.equals=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByEndDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where endDate not equals to DEFAULT_END_DATE
        defaultEditionShouldNotBeFound("endDate.notEquals=" + DEFAULT_END_DATE);

        // Get all the editionList where endDate not equals to UPDATED_END_DATE
        defaultEditionShouldBeFound("endDate.notEquals=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByEndDateIsInShouldWork() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where endDate in DEFAULT_END_DATE or UPDATED_END_DATE
        defaultEditionShouldBeFound("endDate.in=" + DEFAULT_END_DATE + "," + UPDATED_END_DATE);

        // Get all the editionList where endDate equals to UPDATED_END_DATE
        defaultEditionShouldNotBeFound("endDate.in=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByEndDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where endDate is not null
        defaultEditionShouldBeFound("endDate.specified=true");

        // Get all the editionList where endDate is null
        defaultEditionShouldNotBeFound("endDate.specified=false");
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByEndDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where endDate is greater than or equal to DEFAULT_END_DATE
        defaultEditionShouldBeFound("endDate.greaterThanOrEqual=" + DEFAULT_END_DATE);

        // Get all the editionList where endDate is greater than or equal to UPDATED_END_DATE
        defaultEditionShouldNotBeFound("endDate.greaterThanOrEqual=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByEndDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where endDate is less than or equal to DEFAULT_END_DATE
        defaultEditionShouldBeFound("endDate.lessThanOrEqual=" + DEFAULT_END_DATE);

        // Get all the editionList where endDate is less than or equal to SMALLER_END_DATE
        defaultEditionShouldNotBeFound("endDate.lessThanOrEqual=" + SMALLER_END_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByEndDateIsLessThanSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where endDate is less than DEFAULT_END_DATE
        defaultEditionShouldNotBeFound("endDate.lessThan=" + DEFAULT_END_DATE);

        // Get all the editionList where endDate is less than UPDATED_END_DATE
        defaultEditionShouldBeFound("endDate.lessThan=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByEndDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where endDate is greater than DEFAULT_END_DATE
        defaultEditionShouldNotBeFound("endDate.greaterThan=" + DEFAULT_END_DATE);

        // Get all the editionList where endDate is greater than SMALLER_END_DATE
        defaultEditionShouldBeFound("endDate.greaterThan=" + SMALLER_END_DATE);
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where description equals to DEFAULT_DESCRIPTION
        defaultEditionShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the editionList where description equals to UPDATED_DESCRIPTION
        defaultEditionShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where description not equals to DEFAULT_DESCRIPTION
        defaultEditionShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the editionList where description not equals to UPDATED_DESCRIPTION
        defaultEditionShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultEditionShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the editionList where description equals to UPDATED_DESCRIPTION
        defaultEditionShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where description is not null
        defaultEditionShouldBeFound("description.specified=true");

        // Get all the editionList where description is null
        defaultEditionShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where description contains DEFAULT_DESCRIPTION
        defaultEditionShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the editionList where description contains UPDATED_DESCRIPTION
        defaultEditionShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where description does not contain DEFAULT_DESCRIPTION
        defaultEditionShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the editionList where description does not contain UPDATED_DESCRIPTION
        defaultEditionShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsBySpotifyPlaylistIsEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where spotifyPlaylist equals to DEFAULT_SPOTIFY_PLAYLIST
        defaultEditionShouldBeFound("spotifyPlaylist.equals=" + DEFAULT_SPOTIFY_PLAYLIST);

        // Get all the editionList where spotifyPlaylist equals to UPDATED_SPOTIFY_PLAYLIST
        defaultEditionShouldNotBeFound("spotifyPlaylist.equals=" + UPDATED_SPOTIFY_PLAYLIST);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsBySpotifyPlaylistIsNotEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where spotifyPlaylist not equals to DEFAULT_SPOTIFY_PLAYLIST
        defaultEditionShouldNotBeFound("spotifyPlaylist.notEquals=" + DEFAULT_SPOTIFY_PLAYLIST);

        // Get all the editionList where spotifyPlaylist not equals to UPDATED_SPOTIFY_PLAYLIST
        defaultEditionShouldBeFound("spotifyPlaylist.notEquals=" + UPDATED_SPOTIFY_PLAYLIST);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsBySpotifyPlaylistIsInShouldWork() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where spotifyPlaylist in DEFAULT_SPOTIFY_PLAYLIST or UPDATED_SPOTIFY_PLAYLIST
        defaultEditionShouldBeFound("spotifyPlaylist.in=" + DEFAULT_SPOTIFY_PLAYLIST + "," + UPDATED_SPOTIFY_PLAYLIST);

        // Get all the editionList where spotifyPlaylist equals to UPDATED_SPOTIFY_PLAYLIST
        defaultEditionShouldNotBeFound("spotifyPlaylist.in=" + UPDATED_SPOTIFY_PLAYLIST);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsBySpotifyPlaylistIsNullOrNotNull() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where spotifyPlaylist is not null
        defaultEditionShouldBeFound("spotifyPlaylist.specified=true");

        // Get all the editionList where spotifyPlaylist is null
        defaultEditionShouldNotBeFound("spotifyPlaylist.specified=false");
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsBySpotifyPlaylistContainsSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where spotifyPlaylist contains DEFAULT_SPOTIFY_PLAYLIST
        defaultEditionShouldBeFound("spotifyPlaylist.contains=" + DEFAULT_SPOTIFY_PLAYLIST);

        // Get all the editionList where spotifyPlaylist contains UPDATED_SPOTIFY_PLAYLIST
        defaultEditionShouldNotBeFound("spotifyPlaylist.contains=" + UPDATED_SPOTIFY_PLAYLIST);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsBySpotifyPlaylistNotContainsSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where spotifyPlaylist does not contain DEFAULT_SPOTIFY_PLAYLIST
        defaultEditionShouldNotBeFound("spotifyPlaylist.doesNotContain=" + DEFAULT_SPOTIFY_PLAYLIST);

        // Get all the editionList where spotifyPlaylist does not contain UPDATED_SPOTIFY_PLAYLIST
        defaultEditionShouldBeFound("spotifyPlaylist.doesNotContain=" + UPDATED_SPOTIFY_PLAYLIST);
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByCanceledIsEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where canceled equals to DEFAULT_CANCELED
        defaultEditionShouldBeFound("canceled.equals=" + DEFAULT_CANCELED);

        // Get all the editionList where canceled equals to UPDATED_CANCELED
        defaultEditionShouldNotBeFound("canceled.equals=" + UPDATED_CANCELED);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByCanceledIsNotEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where canceled not equals to DEFAULT_CANCELED
        defaultEditionShouldNotBeFound("canceled.notEquals=" + DEFAULT_CANCELED);

        // Get all the editionList where canceled not equals to UPDATED_CANCELED
        defaultEditionShouldBeFound("canceled.notEquals=" + UPDATED_CANCELED);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByCanceledIsInShouldWork() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where canceled in DEFAULT_CANCELED or UPDATED_CANCELED
        defaultEditionShouldBeFound("canceled.in=" + DEFAULT_CANCELED + "," + UPDATED_CANCELED);

        // Get all the editionList where canceled equals to UPDATED_CANCELED
        defaultEditionShouldNotBeFound("canceled.in=" + UPDATED_CANCELED);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByCanceledIsNullOrNotNull() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where canceled is not null
        defaultEditionShouldBeFound("canceled.specified=true");

        // Get all the editionList where canceled is null
        defaultEditionShouldNotBeFound("canceled.specified=false");
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByVisibleIsEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where visible equals to DEFAULT_VISIBLE
        defaultEditionShouldBeFound("visible.equals=" + DEFAULT_VISIBLE);

        // Get all the editionList where visible equals to UPDATED_VISIBLE
        defaultEditionShouldNotBeFound("visible.equals=" + UPDATED_VISIBLE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByVisibleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where visible not equals to DEFAULT_VISIBLE
        defaultEditionShouldNotBeFound("visible.notEquals=" + DEFAULT_VISIBLE);

        // Get all the editionList where visible not equals to UPDATED_VISIBLE
        defaultEditionShouldBeFound("visible.notEquals=" + UPDATED_VISIBLE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByVisibleIsInShouldWork() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where visible in DEFAULT_VISIBLE or UPDATED_VISIBLE
        defaultEditionShouldBeFound("visible.in=" + DEFAULT_VISIBLE + "," + UPDATED_VISIBLE);

        // Get all the editionList where visible equals to UPDATED_VISIBLE
        defaultEditionShouldNotBeFound("visible.in=" + UPDATED_VISIBLE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByVisibleIsNullOrNotNull() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        // Get all the editionList where visible is not null
        defaultEditionShouldBeFound("visible.specified=true");

        // Get all the editionList where visible is null
        defaultEditionShouldNotBeFound("visible.specified=false");
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByPostIsEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);
        Post post = PostResourceIT.createEntity(em);
        em.persist(post);
        em.flush();
        edition.addPost(post);
        editionRepository.saveAndFlush(edition);
        Long postId = post.getId();

        // Get all the editionList where post equals to postId
        defaultEditionShouldBeFound("postId.equals=" + postId);

        // Get all the editionList where post equals to postId + 1
        defaultEditionShouldNotBeFound("postId.equals=" + (postId + 1));
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByArtistIsEqualToSomething() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);
        Artist artist = ArtistResourceIT.createEntity(em);
        em.persist(artist);
        em.flush();
        edition.addArtist(artist);
        editionRepository.saveAndFlush(edition);
        Long artistId = artist.getId();

        // Get all the editionList where artist equals to artistId
        defaultEditionShouldBeFound("artistId.equals=" + artistId);

        // Get all the editionList where artist equals to artistId + 1
        defaultEditionShouldNotBeFound("artistId.equals=" + (artistId + 1));
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getAllEditionsByFestivalIsEqualToSomething() throws Exception {
        // Get already existing entity
        Festival festival = edition.getFestival();
        editionRepository.saveAndFlush(edition);
        Long festivalId = festival.getId();

        // Get all the editionList where festival equals to festivalId
        defaultEditionShouldBeFound("festivalId.equals=" + festivalId);

        // Get all the editionList where festival equals to festivalId + 1
        defaultEditionShouldNotBeFound("festivalId.equals=" + (festivalId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEditionShouldBeFound(String filter) throws Exception {
        restEditionMockMvc.perform(get("/api/editions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(edition.getId().intValue())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].posterContentType").value(hasItem(DEFAULT_POSTER_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].poster").value(hasItem(Base64Utils.encodeToString(DEFAULT_POSTER))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].spotifyPlaylist").value(hasItem(DEFAULT_SPOTIFY_PLAYLIST)))
            .andExpect(jsonPath("$.[*].canceled").value(hasItem(DEFAULT_CANCELED.booleanValue())))
            .andExpect(jsonPath("$.[*].visible").value(hasItem(DEFAULT_VISIBLE.booleanValue())));

        // Check, that the count call also returns 1
        restEditionMockMvc.perform(get("/api/editions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEditionShouldNotBeFound(String filter) throws Exception {
        restEditionMockMvc.perform(get("/api/editions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEditionMockMvc.perform(get("/api/editions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void getNonExistingEdition() throws Exception {
        // Get the edition
        restEditionMockMvc.perform(get("/api/editions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void updateEdition() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        int databaseSizeBeforeUpdate = editionRepository.findAll().size();

        // Update the edition
        Edition updatedEdition = editionRepository.findById(edition.getId()).get();
        // Disconnect from session so that the updates on updatedEdition are not directly saved in db
        em.detach(updatedEdition);
        updatedEdition
            .location(UPDATED_LOCATION)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .poster(UPDATED_POSTER)
            .posterContentType(UPDATED_POSTER_CONTENT_TYPE)
            .description(UPDATED_DESCRIPTION)
            .spotifyPlaylist(UPDATED_SPOTIFY_PLAYLIST)
            .canceled(UPDATED_CANCELED)
            .visible(UPDATED_VISIBLE);
        EditionDTO editionDTO = editionMapper.toDto(updatedEdition);

        restEditionMockMvc.perform(put("/api/editions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(editionDTO)))
            .andExpect(status().isOk());

        // Validate the Edition in the database
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeUpdate);
        Edition testEdition = editionList.get(editionList.size() - 1);
        assertThat(testEdition.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testEdition.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testEdition.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testEdition.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testEdition.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testEdition.getPoster()).isEqualTo(UPDATED_POSTER);
        assertThat(testEdition.getPosterContentType()).isEqualTo(UPDATED_POSTER_CONTENT_TYPE);
        assertThat(testEdition.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testEdition.getSpotifyPlaylist()).isEqualTo(UPDATED_SPOTIFY_PLAYLIST);
        assertThat(testEdition.isCanceled()).isEqualTo(UPDATED_CANCELED);
        assertThat(testEdition.isVisible()).isEqualTo(UPDATED_VISIBLE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void updatePoster() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        int databaseSizeBeforeUpdate = editionRepository.findAll().size();

        FileDTO fileDTO = new FileDTO();
        fileDTO.setFile(UPDATED_POSTER);
        fileDTO.setFileContentType(UPDATED_POSTER_CONTENT_TYPE);

        restEditionMockMvc.perform(put("/api/editions/{id}/poster", edition.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fileDTO)))
            .andExpect(status().isOk());

        // Validate the Edition in the database
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeUpdate + 1);
        Edition testEdition = editionList.get(editionList.size() - 1);
        assertThat(testEdition.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testEdition.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testEdition.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testEdition.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testEdition.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testEdition.getPoster()).isEqualTo(UPDATED_POSTER);
        assertThat(testEdition.getPosterContentType()).isEqualTo(UPDATED_POSTER_CONTENT_TYPE);
        assertThat(testEdition.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEdition.getSpotifyPlaylist()).isEqualTo(DEFAULT_SPOTIFY_PLAYLIST);
        assertThat(testEdition.isCanceled()).isEqualTo(DEFAULT_CANCELED);
        assertThat(testEdition.isVisible()).isEqualTo(Boolean.FALSE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void sendReport() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        int databaseSizeBeforeUpdate = editionRepository.findAll().size();

        String report = "report";

        restEditionMockMvc.perform(put("/api/editions/{id}/report", edition.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(report))
            .andExpect(status().isOk());

        // Validate the Edition in the database
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeUpdate + 1);
        Edition testEdition = editionList.get(editionList.size() - 1);
        assertThat(testEdition.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testEdition.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testEdition.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testEdition.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testEdition.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testEdition.getPoster()).isEqualTo(DEFAULT_POSTER);
        assertThat(testEdition.getPosterContentType()).isEqualTo(DEFAULT_POSTER_CONTENT_TYPE);
        assertThat(testEdition.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEdition.getSpotifyPlaylist()).isEqualTo(DEFAULT_SPOTIFY_PLAYLIST);
        assertThat(testEdition.isCanceled()).isEqualTo(DEFAULT_CANCELED);
        assertThat(testEdition.getReport()).isEqualTo(report);
        assertThat(testEdition.isVisible()).isEqualTo(Boolean.FALSE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void updateArtists() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        int databaseSizeBeforeUpdate = editionRepository.findAll().size();

        ArtistDTO artistDTO = new ArtistDTO();
        artistDTO.setName("Artist name");
        List<ArtistDTO> artists = Lists.newArrayList(artistDTO);

        restEditionMockMvc.perform(put("/api/editions/{id}/artists", edition.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(artists)))
            .andExpect(status().isOk());

        // Validate the Edition in the database
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeUpdate + 1);
        Edition testEdition = editionList.get(editionList.size() - 1);
        assertThat(testEdition.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testEdition.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testEdition.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testEdition.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testEdition.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testEdition.getPoster()).isEqualTo(DEFAULT_POSTER);
        assertThat(testEdition.getPosterContentType()).isEqualTo(DEFAULT_POSTER_CONTENT_TYPE);
        assertThat(testEdition.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEdition.getSpotifyPlaylist()).isEqualTo(DEFAULT_SPOTIFY_PLAYLIST);
        assertThat(testEdition.isCanceled()).isEqualTo(DEFAULT_CANCELED);
        assertThat(testEdition.isVisible()).isEqualTo(Boolean.FALSE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void validateEdition() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        int databaseSizeBeforeUpdate = editionRepository.findAll().size();

        ArtistDTO artistDTO = new ArtistDTO();
        artistDTO.setName("Artist name");
        List<ArtistDTO> artists = Lists.newArrayList(artistDTO);

        restEditionMockMvc.perform(put("/api/editions/{id}/artists", edition.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(artists)))
            .andExpect(status().isOk());

        EditionDTO editionDTO = editionMapper.toDto(edition);
        editionDTO.setArtists(new HashSet<>(artists));

        restEditionMockMvc.perform(put("/api/editions/validate?createPost=true&awardPoints=true")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(editionDTO)))
            .andExpect(status().isOk());

        // Validate the Edition in the database
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeUpdate);
        Edition testEdition = editionList.get(editionList.size() - 1);
        assertThat(testEdition.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testEdition.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testEdition.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testEdition.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testEdition.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testEdition.getPoster()).isEqualTo(DEFAULT_POSTER);
        assertThat(testEdition.getPosterContentType()).isEqualTo(DEFAULT_POSTER_CONTENT_TYPE);
        assertThat(testEdition.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEdition.getSpotifyPlaylist()).isEqualTo(DEFAULT_SPOTIFY_PLAYLIST);
        assertThat(testEdition.isCanceled()).isEqualTo(DEFAULT_CANCELED);
        assertThat(testEdition.isVisible()).isEqualTo(Boolean.TRUE);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void updateNonExistingEdition() throws Exception {
        int databaseSizeBeforeUpdate = editionRepository.findAll().size();

        // Create the Edition
        EditionDTO editionDTO = editionMapper.toDto(edition);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEditionMockMvc.perform(put("/api/editions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(editionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Edition in the database
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    void deleteEdition() throws Exception {
        // Initialize the database
        editionRepository.saveAndFlush(edition);

        int databaseSizeBeforeDelete = editionRepository.findAll().size();

        // Delete the edition
        restEditionMockMvc.perform(delete("/api/editions/{id}", edition.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    void saveFromCrawler() throws Exception {
        int databaseSizeBeforeCreate = editionRepository.findAll().size();
        // Create the Edition
        EditionDTO editionDTO = editionMapper.toDto(edition);
        FestivalDTO festivalDTO = festivalMapper.toDto(edition.getFestival());
        editionService.saveFromCrawler(festivalDTO, editionDTO);

        // Validate the Edition in the database
        List<Edition> editionList = editionRepository.findAll();
        assertThat(editionList).hasSize(databaseSizeBeforeCreate + 1);
        Edition testEdition = editionList.get(editionList.size() - 1);
        assertThat(testEdition.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testEdition.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testEdition.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testEdition.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testEdition.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testEdition.getPoster()).isEqualTo(DEFAULT_POSTER);
        assertThat(testEdition.getPosterContentType()).isEqualTo(DEFAULT_POSTER_CONTENT_TYPE);
        assertThat(testEdition.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEdition.getSpotifyPlaylist()).isEqualTo(DEFAULT_SPOTIFY_PLAYLIST);
        assertThat(testEdition.isCanceled()).isEqualTo(DEFAULT_CANCELED);
        assertThat(testEdition.isVisible()).isEqualTo(UPDATED_VISIBLE);
    }
}
