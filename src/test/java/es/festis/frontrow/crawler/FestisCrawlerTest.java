package es.festis.frontrow.crawler;

import static org.junit.jupiter.api.Assertions.*;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.url.WebURL;
import es.festis.frontrow.service.EditionService;
import java.time.ZonedDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class FestisCrawlerTest {

    @Mock
    EditionService editionService;

    FestisCrawler festisCrawler;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        festisCrawler = new FestisCrawler(editionService, ZonedDateTime.now());
    }

    @Test
    void shouldVisitWhenIsPage() {
        WebURL url = new WebURL();
        url.setURL("https://festis.es/page/2/");
        Page referringPage = new Page(url);
        assertTrue(festisCrawler.shouldVisit(referringPage, url));
    }

    @Test
    void shouldVisitWhenIsVisitablePost() {
        WebURL url = new WebURL();
        url.setURL("https://festis.es/lollapalooza-berlin/primeras-confirmaciones-del-lollapalooza-berlin-2020/");
        Page referringPage = new Page(url);
        assertTrue(festisCrawler.shouldVisit(referringPage, url));
    }

    @Test
    void shouldNotVisitWhenIsReviewPost() {
        WebURL url = new WebURL();
        url.setURL("https://festis.es/sziget/cronica-del-sziget-2019-parte-iii/");
        Page referringPage = new Page(url);
        assertFalse(festisCrawler.shouldVisit(referringPage, url));
    }

    @Test
    void shouldNotVisitWhenIsMiscPost() {
        WebURL url = new WebURL();
        url.setURL("https://festis.es/miscelanea/resumen-de-festivales-afectados-por-el-coronavirus/");
        Page referringPage = new Page(url);
        assertFalse(festisCrawler.shouldVisit(referringPage, url));
    }
}
