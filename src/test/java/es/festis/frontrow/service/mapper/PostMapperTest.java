package es.festis.frontrow.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class PostMapperTest {

    private PostMapper postMapper;

    @BeforeEach
    void setUp() {
        postMapper = new PostMapperImpl();
    }

    @Test
    void testEntityFromId() {
        Long id = 1L;
        assertThat(postMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(postMapper.fromId(null)).isNull();
    }
}
