package es.festis.frontrow.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class EditionMapperTest {

    private EditionMapper editionMapper;

    @BeforeEach
    void setUp() {
        editionMapper = new EditionMapperImpl();
    }

    @Test
    void testEntityFromId() {
        Long id = 1L;
        assertThat(editionMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(editionMapper.fromId(null)).isNull();
    }
}
