package es.festis.frontrow.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class ContributionMapperTest {

    private ContributionMapper contributionMapper;

    @BeforeEach
    void setUp() {
        contributionMapper = new ContributionMapperImpl();
    }

    @Test
    void testEntityFromId() {
        Long id = 1L;
        assertThat(contributionMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contributionMapper.fromId(null)).isNull();
    }
}
