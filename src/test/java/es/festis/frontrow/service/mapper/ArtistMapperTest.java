package es.festis.frontrow.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class ArtistMapperTest {

    private ArtistMapper artistMapper;

    @BeforeEach
    void setUp() {
        artistMapper = new ArtistMapperImpl();
    }

    @Test
    void testEntityFromId() {
        Long id = 1L;
        assertThat(artistMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(artistMapper.fromId(null)).isNull();
    }
}
