package es.festis.frontrow.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class FestivalMapperTest {

    private FestivalMapper festivalMapper;

    @BeforeEach
    void setUp() {
        festivalMapper = new FestivalMapperImpl();
    }

    @Test
    void testEntityFromId() {
        Long id = 1L;
        assertThat(festivalMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(festivalMapper.fromId(null)).isNull();
    }
}
