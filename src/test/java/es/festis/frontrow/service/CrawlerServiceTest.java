package es.festis.frontrow.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.CrawlController.WebCrawlerFactory;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import es.festis.frontrow.crawler.CrawlService;
import java.time.ZonedDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

class CrawlerServiceTest {

    @Mock
    EditionService editionService;

    @Mock
    CrawlController crawlController;

    @Spy
    @InjectMocks
    CrawlService crawlService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void startCrawlingShouldStartFestisCrawler() throws Exception {
        doReturn(crawlController).when(crawlService)
            .makeCrawlController(any(CrawlConfig.class), any(PageFetcher.class), any(RobotstxtServer.class));
        when(editionService.getAndUpdateLastCrawlDate()).thenReturn(ZonedDateTime.now());
        doNothing().when(crawlController).startNonBlocking(any(WebCrawlerFactory.class), eq(7));
        crawlService.startCrawling();
        verify(crawlController, times(1)).startNonBlocking(any(WebCrawlerFactory.class), eq(7));
    }

}
