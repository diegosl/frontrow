package es.festis.frontrow.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import es.festis.frontrow.web.rest.TestUtil;

class FestivalDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FestivalDTO.class);
        FestivalDTO festivalDTO1 = new FestivalDTO();
        festivalDTO1.setId(1L);
        FestivalDTO festivalDTO2 = new FestivalDTO();
        assertThat(festivalDTO1).isNotEqualTo(festivalDTO2);
        festivalDTO2.setId(festivalDTO1.getId());
        assertThat(festivalDTO1).isEqualTo(festivalDTO2);
        festivalDTO2.setId(2L);
        assertThat(festivalDTO1).isNotEqualTo(festivalDTO2);
        festivalDTO1.setId(null);
        assertThat(festivalDTO1).isNotEqualTo(festivalDTO2);
    }
}
