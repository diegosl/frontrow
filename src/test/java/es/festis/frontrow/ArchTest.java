package es.festis.frontrow;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("es.festis.frontrow");

        noClasses()
            .that()
                .resideInAnyPackage("es.festis.frontrow.service..")
            .or()
                .resideInAnyPackage("es.festis.frontrow.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..es.festis.frontrow.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
