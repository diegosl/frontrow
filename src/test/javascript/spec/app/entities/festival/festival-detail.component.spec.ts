import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { FrontrowTestModule } from '../../../test.module';
import { FestivalDetailComponent } from 'app/entities/festival/festival-detail.component';
import { Festival } from 'app/shared/model/festival.model';

describe('Component Tests', () => {
  describe('Festival Management Detail Component', () => {
    let comp: FestivalDetailComponent;
    let fixture: ComponentFixture<FestivalDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ festival: new Festival(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FrontrowTestModule],
        declarations: [FestivalDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(FestivalDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FestivalDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load festival on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.festival).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
