import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FrontrowTestModule } from '../../../test.module';
import { FestivalUpdateComponent } from 'app/entities/festival/festival-update.component';
import { FestivalService } from 'app/entities/festival/festival.service';
import { Festival } from 'app/shared/model/festival.model';

describe('Component Tests', () => {
  describe('Festival Management Update Component', () => {
    let comp: FestivalUpdateComponent;
    let fixture: ComponentFixture<FestivalUpdateComponent>;
    let service: FestivalService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FrontrowTestModule],
        declarations: [FestivalUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(FestivalUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FestivalUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FestivalService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Festival(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Festival();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
