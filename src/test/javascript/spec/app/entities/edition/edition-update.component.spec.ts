import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { FrontrowTestModule } from '../../../test.module';
import { EditionUpdateComponent } from 'app/entities/edition/edition-update.component';
import { EditionService } from 'app/entities/edition/edition.service';
import { Edition } from 'app/shared/model/edition.model';

describe('Component Tests', () => {
  describe('Edition Management Update Component', () => {
    let comp: EditionUpdateComponent;
    let fixture: ComponentFixture<EditionUpdateComponent>;
    let service: EditionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FrontrowTestModule],
        declarations: [EditionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(EditionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EditionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EditionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Edition(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Edition();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
