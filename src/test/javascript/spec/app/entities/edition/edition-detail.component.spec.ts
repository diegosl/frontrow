import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { FrontrowTestModule } from '../../../test.module';
import { EditionDetailComponent } from 'app/entities/edition/edition-detail.component';
import { Edition } from 'app/shared/model/edition.model';

describe('Component Tests', () => {
  describe('Edition Management Detail Component', () => {
    let comp: EditionDetailComponent;
    let fixture: ComponentFixture<EditionDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ edition: new Edition(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [FrontrowTestModule],
        declarations: [EditionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(EditionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EditionDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load edition on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.edition).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
