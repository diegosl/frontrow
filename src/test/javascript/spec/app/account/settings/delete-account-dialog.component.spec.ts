import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { FrontrowTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { DeleteAccountDialogComponent } from 'app/account/settings/delete-account-dialog.component';
import { MockAccountService } from '../../../helpers/mock-account.service';
import { AccountService } from 'app/core/auth/account.service';
import { MockLoginService } from '../../../helpers/mock-login.service';
import { MockRouter } from '../../../helpers/mock-route.service';
import { LoginService } from 'app/core/login/login.service';
import { Router } from '@angular/router';

describe('Component Tests', () => {
  describe('Account Settings Delete Component', () => {
    let comp: DeleteAccountDialogComponent;
    let fixture: ComponentFixture<DeleteAccountDialogComponent>;
    let mockAccountService: MockAccountService;
    let mockLoginService: MockLoginService;
    let mockRouter: MockRouter;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [FrontrowTestModule],
        declarations: [DeleteAccountDialogComponent],
        providers: [
          {
            provide: AccountService,
            useClass: MockAccountService
          },
          {
            provide: LoginService,
            useClass: MockLoginService
          }
        ]
      })
        .overrideTemplate(DeleteAccountDialogComponent, '')
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(DeleteAccountDialogComponent);
      comp = fixture.componentInstance;
      mockAccountService = TestBed.get(AccountService);
      mockLoginService = TestBed.get(LoginService);
      mockRouter = TestBed.get(Router);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          mockAccountService.deleteSpy();
          mockLoginService.logoutSpy();

          // WHEN
          comp.confirmDelete();
          tick();

          // THEN
          expect(mockAccountService.deleteSpy).toHaveBeenCalled();
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
