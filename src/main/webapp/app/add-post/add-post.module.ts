import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FrontrowSharedModule } from 'app/shared/shared.module';
import { EditorModule } from 'primeng/editor';

import { AddPostComponent } from './add-post.component';

import { addPostRoute } from './add-post.route';

@NgModule({
  imports: [FrontrowSharedModule, RouterModule.forChild([addPostRoute]), EditorModule],
  declarations: [AddPostComponent]
})
export class AddPostModule {}
