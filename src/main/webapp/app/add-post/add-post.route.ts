import { Route } from '@angular/router';

import { AddPostComponent } from './add-post.component';

export const addPostRoute: Route = {
  path: '',
  component: AddPostComponent,
  data: {
    pageTitle: 'frontrowApp.post.home.title'
  }
};
