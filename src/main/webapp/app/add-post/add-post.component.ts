import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IEdition } from 'app/shared/model/edition.model';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { PostService } from 'app/entities/post/post.service';
import { EditionService } from 'app/entities/edition/edition.service';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { IPost, Post } from 'app/shared/model/post.model';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

@Component({
  selector: 'jhi-edition-update',
  templateUrl: './add-post.component.html',
  animations: [
    trigger('smoothCollapse', [
      state(
        'initial',
        style({
          height: '0',
          overflow: 'hidden',
          opacity: '0'
        })
      ),
      state(
        'final',
        style({
          overflow: 'hidden',
          opacity: '1'
        })
      ),
      transition('initial=>final', animate('750ms')),
      transition('final=>initial', animate('750ms'))
    ])
  ]
})
export class AddPostComponent implements OnInit {
  isCollapsed = true;
  isSaving = false;

  editions: IEdition[] = [];

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(100)]],
    content: [],
    editionId: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected postService: PostService,
    protected editionService: EditionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.updateForm(new Post());

    this.editionService
      .query()
      .pipe(
        map((res: HttpResponse<IEdition[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: IEdition[]) => (this.editions = resBody));
  }

  updateForm(post: IPost): void {
    this.editForm.patchValue({
      id: post.id,
      title: post.title,
      content: post.content,
      date: post.date != null ? post.date.format(DATE_TIME_FORMAT) : null,
      editionId: post.editionId
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('frontrowApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const post = this.createFromForm();
    if (post.id !== undefined) {
      this.subscribeToSaveResponse(this.postService.update(post));
    } else {
      this.subscribeToSaveResponse(this.postService.create(post));
    }
  }

  private createFromForm(): IPost {
    return {
      ...new Post(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      content: this.editForm.get(['content'])!.value,
      editionId: this.editForm.get(['editionId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPost>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
