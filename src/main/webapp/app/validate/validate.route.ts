import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { ValidateComponent } from './validate.component';
import { IEdition, Edition } from 'app/shared/model/edition.model';
import { EditionService } from 'app/entities/edition/edition.service';
import { ValidateUpdateComponent } from './validate-update.component';
import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

@Injectable({ providedIn: 'root' })
export class EditionResolve implements Resolve<IEdition> {
  constructor(private service: EditionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEdition> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((edition: HttpResponse<Edition>) => {
          if (edition.body) {
            if (edition.body.originalEdition) {
              return this.service.find(edition.body.originalEdition).pipe(
                flatMap((originalEditionInfo: HttpResponse<Edition>) => {
                  if (edition.body && originalEditionInfo.body) {
                    edition.body.originalEditionInfo = originalEditionInfo.body;
                    return of(edition.body);
                  } else {
                    this.router.navigate(['404']);
                    return EMPTY;
                  }
                })
              );
            }
            if (edition.body.visible) {
              edition.body.originalEditionInfo = edition.body;
            }
            return of(edition.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    this.router.navigate(['404']);
    return EMPTY;
  }
}

export const validateRoute: Routes = [
  {
    path: '',
    component: ValidateComponent,
    data: {
      pageTitle: 'global.menu.validate'
    }
  },
  {
    path: ':id',
    component: ValidateUpdateComponent,
    resolve: {
      edition: EditionResolve
    },
    data: {
      authorities: [Authority.EDITOR, Authority.ADMIN],
      pageTitle: 'global.menu.validate'
    },
    canActivate: [UserRouteAccessService]
  }
];
