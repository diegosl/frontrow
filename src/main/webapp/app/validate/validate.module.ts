import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FrontrowSharedModule } from 'app/shared/shared.module';

import { ValidateComponent } from './validate.component';

import { validateRoute } from './validate.route';
import { ValidateUpdateComponent } from 'app/validate/validate-update.component';
import { AutoCompleteModule } from 'primeng';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

@NgModule({
  imports: [FrontrowSharedModule, RouterModule.forChild(validateRoute), AutoCompleteModule, GooglePlaceModule],
  declarations: [ValidateComponent, ValidateUpdateComponent]
})
export class ValidateModule {}
