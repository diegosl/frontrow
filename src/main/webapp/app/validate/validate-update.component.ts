import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IEdition, Edition } from 'app/shared/model/edition.model';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { Artist, IArtist } from 'app/shared/model/artist.model';
import { ArtistService } from 'app/entities/artist/artist.service';
import { EditionService } from 'app/entities/edition/edition.service';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

@Component({
  selector: 'jhi-validate-update',
  templateUrl: './validate-update.component.html'
})
export class ValidateUpdateComponent implements OnInit {
  isSaving = false;
  startDateDp: any;
  endDateDp: any;
  edition: IEdition | undefined;
  filteredArtists: IArtist[] = [];
  currentDate = new Date();

  options: Options = new Options({ types: ['(cities)'], fields: ['place_id', 'geometry', 'formatted_address'] });

  editForm = this.fb.group({
    id: [null, [Validators.required]],
    location: [],
    latitude: [],
    longitude: [],
    startDate: [null, [Validators.required]],
    endDate: [],
    poster: [],
    posterContentType: [],
    description: [],
    spotifyPlaylist: [],
    canceled: [],
    visible: [],
    report: [],
    originalEdition: [],
    price: [null, [Validators.maxLength(7)]],
    schedule: [],
    artists: [],
    festivalId: [null, Validators.required],
    logo: [],
    logoContentType: [],
    festivalName: [null, [Validators.required]],
    web: [],
    facebook: [],
    twitter: [],
    instagram: [],
    createPost: []
  });

  get template(): string {
    let artists: IArtist[] = this.editForm.get(['artists'])!.value;
    if (this.edition!.originalEditionInfo && this.edition!.originalEditionInfo.artists) {
      artists = artists.filter(
        artist => !this.edition!.originalEditionInfo!.artists!.map(originalArtist => originalArtist.id).includes(artist.id)
      );
    }
    switch (artists.length) {
      case 0:
        return '';
      case 1:
        return 'frontrowApp.post.templates.oneArtist';
      case 2:
        return 'frontrowApp.post.templates.twoArtists';
      default:
        return 'frontrowApp.post.templates.moreArtists';
    }
  }

  get addedArtists(): string {
    const artists: IArtist[] = this.editForm.get(['artists'])!.value;
    if (this.edition!.originalEditionInfo && this.edition!.originalEditionInfo.artists) {
      return artists
        .filter(artist => !this.edition!.originalEditionInfo!.artists!.map(originalArtist => originalArtist.id).includes(artist.id))
        .map(artist => artist.name)
        .join(', ');
    } else {
      return artists.map(artist => artist.name).join(', ');
    }
  }

  get removedArtists(): string {
    const artists: IArtist[] = this.editForm.get(['artists'])!.value;
    if (this.edition!.originalEditionInfo && this.edition!.originalEditionInfo.artists) {
      return this.edition!.originalEditionInfo.artists.filter(artist => !artists.map(newArtist => newArtist.id).includes(artist.id))
        .map(artist => artist.name)
        .join(', ');
    } else {
      return '';
    }
  }

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected editionService: EditionService,
    protected artistService: ArtistService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ edition }) => {
      this.updateForm(edition);
      this.edition = edition;
    });
  }

  findArtists(event: any): void {
    this.filteredArtists = [new Artist(undefined, event.query, undefined)];
    if (event.query.length > 2) {
      this.artistService.findByName(event.query).subscribe((res: HttpResponse<IArtist[]>) => {
        if (res.body != null && res.body.length > 0) {
          this.filteredArtists =
            res.body[0].name!.toLowerCase() === event.query.toLowerCase() ? res.body : this.filteredArtists.concat(res.body);
        }
      });
    }
  }

  updateForm(edition: IEdition): void {
    this.editForm.patchValue({
      id: edition.id,
      location: edition.location,
      latitude: edition.latitude,
      longitude: edition.longitude,
      startDate: edition.startDate,
      endDate: edition.endDate,
      poster: edition.poster,
      posterContentType: edition.posterContentType,
      description: edition.description,
      spotifyPlaylist: edition.spotifyPlaylist,
      canceled: edition.canceled,
      visible: edition.visible,
      report: edition.report,
      originalEdition: edition.originalEdition,
      price: edition.price,
      schedule: edition.schedule,
      artists: edition.artists,
      festivalId: edition.festivalId,
      festivalName: edition.festivalName,
      web: edition.web,
      facebook: edition.facebook,
      twitter: edition.twitter,
      instagram: edition.instagram,
      logo: edition.logo,
      logoContentType: edition.logoContentType,
      createPost: true
    });
  }

  public handleAddressChange(address: Address): void {
    this.editForm.get(['latitude'])!.setValue(address.geometry.location.lat());
    this.editForm.get(['longitude'])!.setValue(address.geometry.location.lng());
    this.editForm.get(['location'])!.setValue(address.formatted_address);
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('frontrowApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  restorePoster(): void {
    this.editForm.get(['poster'])!.setValue(this.edition!.originalEditionInfo!.poster);
    this.editForm.get(['posterContentType'])!.setValue(this.edition!.originalEditionInfo!.posterContentType);
  }

  previousState(): void {
    window.history.back();
  }

  save(awardPoints: boolean): void {
    this.isSaving = true;
    const edition = this.createFromForm();
    this.subscribeToSaveResponse(this.editionService.validate(edition, this.editForm.get(['createPost'])!.value, awardPoints));
  }

  private createFromForm(): IEdition {
    return {
      ...new Edition(),
      id: this.editForm.get(['id'])!.value,
      location: this.editForm.get(['location'])!.value,
      latitude: this.editForm.get(['latitude'])!.value,
      longitude: this.editForm.get(['longitude'])!.value,
      startDate: this.editForm.get(['startDate'])!.value,
      endDate: this.editForm.get(['endDate'])!.value,
      posterContentType: this.editForm.get(['posterContentType'])!.value,
      poster: this.editForm.get(['poster'])!.value,
      description: this.editForm.get(['description'])!.value,
      spotifyPlaylist: this.editForm.get(['spotifyPlaylist'])!.value,
      canceled: this.editForm.get(['canceled'])!.value,
      visible: this.editForm.get(['visible'])!.value,
      report: this.editForm.get(['report'])!.value,
      originalEdition: this.editForm.get(['originalEdition'])!.value,
      price: this.editForm.get(['price'])!.value,
      schedule: this.editForm.get(['schedule'])!.value,
      artists: this.editForm.get(['artists'])!.value,
      festivalId: this.editForm.get(['festivalId'])!.value,
      festivalName: this.editForm.get(['festivalName'])!.value,
      web: this.editForm.get(['web'])!.value,
      facebook: this.editForm.get(['facebook'])!.value,
      twitter: this.editForm.get(['twitter'])!.value,
      instagram: this.editForm.get(['instagram'])!.value,
      logo: this.editForm.get(['logo'])!.value,
      logoContentType: this.editForm.get(['logoContentType'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEdition>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
