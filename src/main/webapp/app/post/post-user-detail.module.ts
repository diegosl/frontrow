import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FrontrowSharedModule } from 'app/shared/shared.module';
import { postUserRoute } from './post-user.route';
import { PostUserDetailComponent, SafeHtmlPipe } from './post-user-detail.component';

@NgModule({
  exports: [SafeHtmlPipe],
  imports: [FrontrowSharedModule, RouterModule.forChild(postUserRoute)],
  declarations: [PostUserDetailComponent, SafeHtmlPipe]
})
export class FrontrowPostUserModule {}
