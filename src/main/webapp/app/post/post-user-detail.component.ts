import { Component, OnDestroy, OnInit, Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils, JhiEventManager, JhiLanguageService, JhiParseLinks } from 'ng-jhipster';

import { IPost } from 'app/shared/model/post.model';
import { Moment } from 'moment';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { CommentService } from 'app/entities/comment/comment.service';
import { IComment } from 'app/shared/model/comment.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { FormBuilder, Validators } from '@angular/forms';

@Pipe({ name: 'safeHtml' })
export class SafeHtmlPipe implements PipeTransform {
  constructor(private sanitized: DomSanitizer) {}
  transform(value: any): SafeHtml {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}

@Component({
  selector: 'jhi-edition-detail',
  templateUrl: './post-user-detail.component.html'
})
export class PostUserDetailComponent implements OnInit, OnDestroy {
  post: IPost | null = null;
  comments: IComment[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  isSaving = false;

  commentForm = this.fb.group({
    text: [null, [Validators.required, Validators.minLength(2)]]
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    private languageService: JhiLanguageService,
    private commentService: CommentService,
    protected eventManager: JhiEventManager,
    protected parseLinks: JhiParseLinks,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.comments = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
  }

  loadComments(): void {
    this.commentService
      .getPostComments(this.post!.id!, {
        page: this.page,
        size: this.itemsPerPage,
        sort: ['date,desc']
      })
      .subscribe((res: HttpResponse<IComment[]>) => this.paginateComments(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.comments = [];
    this.loadComments();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadComments();
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ post }) => {
      this.post = post;
      this.loadComments();
      this.registerChangeInComments();
    });
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IComment): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInComments(): void {
    this.eventSubscriber = this.eventManager.subscribe('commentListModification' + this.post!.id, () => this.reset());
  }

  protected paginateComments(data: IComment[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.comments.push(data[i]);
      }
    }
  }

  postContent(content: string): string {
    return content.replace(/<img/, '<img style="max-width: 100%" ');
  }

  postDate(date: Moment | undefined): string {
    if (date) {
      date.locale(this.languageService.getCurrentLanguage());
      return date.format('D MMM YYYY');
    } else {
      return '';
    }
  }

  dateTime(date: Moment): string {
    date.locale(this.languageService.getCurrentLanguage());
    return date.format('D MMM YYYY HH:mm');
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  previousState(): void {
    window.history.back();
  }

  addComment(): void {
    this.isSaving = true;
    const comment = this.createFromForm();
    this.subscribeToSaveResponse(this.commentService.create(comment));
  }

  private createFromForm(): IComment {
    return {
      ...new Comment(),
      text: this.commentForm.get(['text'])!.value,
      postId: this.post!.id
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IComment>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.eventManager.broadcast('commentListModification' + this.post!.id);
    this.commentForm.reset();
    this.isSaving = false;
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
