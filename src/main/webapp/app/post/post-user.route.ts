import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { IPost, Post } from 'app/shared/model/post.model';
import { PostService } from 'app/entities/post/post.service';
import { PostUserDetailComponent } from './post-user-detail.component';

@Injectable({ providedIn: 'root' })
export class PostResolve implements Resolve<IPost> {
  constructor(private service: PostService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPost> | Observable<never> {
    const handle = route.params['handle'];
    if (handle) {
      return this.service.findByHandle(handle).pipe(
        flatMap((post: HttpResponse<Post>) => {
          if (post.body) {
            return of(post.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Post());
  }
}

export const postUserRoute: Routes = [
  {
    path: 'post/:handle',
    component: PostUserDetailComponent,
    resolve: {
      post: PostResolve
    },
    data: {
      pageTitle: 'frontrowApp.post.home.title'
    }
  }
];
