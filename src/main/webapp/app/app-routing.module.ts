import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { Authority } from 'app/shared/constants/authority.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

const LAYOUT_ROUTES = [navbarRoute, ...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            authorities: [Authority.ADMIN]
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule)
        },
        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule)
        },
        {
          path: 'add-festival',
          loadChildren: () => import('./add-festival/add-festival.module').then(m => m.AddFestivalModule)
        },
        {
          path: 'add-post',
          loadChildren: () => import('./add-post/add-post.module').then(m => m.AddPostModule)
        },
        {
          path: 'validate',
          data: {
            authorities: [Authority.ADMIN, Authority.EDITOR]
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./validate/validate.module').then(m => m.ValidateModule)
        },
        {
          path: 'explore',
          loadChildren: () => import('./explore/explore.module').then(m => m.ExploreModule)
        },
        ...LAYOUT_ROUTES
      ],
      { enableTracing: DEBUG_INFO_ENABLED }
    )
  ],
  exports: [RouterModule]
})
export class FrontrowAppRoutingModule {}
