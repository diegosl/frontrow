import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEdition } from 'app/shared/model/edition.model';
import { EditionService } from 'app/entities/edition/edition.service';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'jhi-account-delete-dialog',
  templateUrl: './send-report-dialog.component.html'
})
export class SendReportDialogComponent implements OnInit {
  isSaving = false;
  edition?: IEdition;

  reportForm = this.fb.group({
    report: ['', [Validators.required]]
  });

  constructor(private editionService: EditionService, public activeModal: NgbActiveModal, private fb: FormBuilder) {}

  ngOnInit(): void {}

  clear(): void {
    this.activeModal.dismiss();
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEdition>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.activeModal.close();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  save(): void {
    if (this.edition) {
      this.subscribeToSaveResponse(this.editionService.sendReport(this.edition.id!, this.reportForm.get('report')!.value));
    }
  }
}
