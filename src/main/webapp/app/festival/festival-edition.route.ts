import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { IEdition, Edition } from 'app/shared/model/edition.model';
import { EditionService } from 'app/entities/edition/edition.service';
import { FestivalEditionDetailComponent } from './festival-edition-detail.component';

@Injectable({ providedIn: 'root' })
export class EditionResolve implements Resolve<IEdition> {
  constructor(private service: EditionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEdition> | Observable<never> {
    const festival = route.params['festival'];
    const edition = route.params['edition'];
    if (festival && edition) {
      return this.service.findByName(festival, edition).pipe(
        flatMap((ed: HttpResponse<Edition>) => {
          if (ed.body) {
            return of(ed.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Edition());
  }
}

export const festivalEditionRoute: Routes = [
  {
    path: 'f/:festival/:edition',
    component: FestivalEditionDetailComponent,
    resolve: {
      edition: EditionResolve
    },
    data: {
      pageTitle: 'frontrowApp.festival.home.title'
    }
  }
];
