import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FrontrowSharedModule } from 'app/shared/shared.module';
import { festivalEditionRoute } from './festival-edition.route';
import { FestivalEditionDetailComponent } from './festival-edition-detail.component';
import { ManageArtistsDialogComponent } from './manage-artists-dialog.component';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { SendReportDialogComponent } from './send-report-dialog.component';
import { UpdatePosterDialogComponent } from 'app/festival/update-poster-dialog.component';
import { SafePipeModule } from 'safe-pipe';
import { FrontrowPostUserModule } from 'app/post/post-user-detail.module';

@NgModule({
  imports: [FrontrowSharedModule, RouterModule.forChild(festivalEditionRoute), AutoCompleteModule, SafePipeModule, FrontrowPostUserModule],
  declarations: [FestivalEditionDetailComponent, ManageArtistsDialogComponent, SendReportDialogComponent, UpdatePosterDialogComponent],
  entryComponents: [ManageArtistsDialogComponent, SendReportDialogComponent, UpdatePosterDialogComponent]
})
export class FrontrowFestivalEditionModule {}
