import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils, JhiEventManager, JhiLanguageService, JhiParseLinks } from 'ng-jhipster';

import { IEdition } from 'app/shared/model/edition.model';
import { Moment } from 'moment';
import { FestivalService } from 'app/entities/festival/festival.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManageArtistsDialogComponent } from 'app/festival/manage-artists-dialog.component';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { IComment } from 'app/shared/model/comment.model';
import { CommentService } from 'app/entities/comment/comment.service';
import { Observable, Subscription } from 'rxjs';
import { FormBuilder, Validators } from '@angular/forms';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { SendReportDialogComponent } from 'app/festival/send-report-dialog.component';
import { UpdatePosterDialogComponent } from 'app/festival/update-poster-dialog.component';
import { DomSanitizer } from '@angular/platform-browser';
import { IPost } from 'app/shared/model/post.model';
import { PostService } from 'app/entities/post/post.service';

@Component({
  selector: 'jhi-edition-detail',
  templateUrl: './festival-edition-detail.component.html'
})
export class FestivalEditionDetailComponent implements OnInit, OnDestroy {
  edition: IEdition | null = null;
  comments: IComment[];
  posts: IPost[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  isSaving = false;

  commentForm = this.fb.group({
    text: [null, [Validators.required, Validators.minLength(2)]]
  });

  constructor(
    private sanitizer: DomSanitizer,
    private festivalService: FestivalService,
    protected postService: PostService,
    private commentService: CommentService,
    private languageService: JhiLanguageService,
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected parseLinks: JhiParseLinks,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {
    this.comments = [];
    this.posts = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
  }

  postDate(date: Moment): string {
    date.locale(this.languageService.getCurrentLanguage());
    return date.format('D MMM YYYY HH:mm');
  }

  loadPosts(): void {
    this.postService.getLatestByEdition(5, this.edition!.id!).subscribe((res: HttpResponse<IPost[]>) => this.onSuccessPosts(res.body));
  }

  protected onSuccessPosts(data: IPost[] | null): void {
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.posts.push(data[i]);
      }
    }
  }

  trackPostId(index: number, item: IPost): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  loadComments(): void {
    this.commentService
      .getFestivalComments(this.edition!.festivalId!, {
        page: this.page,
        size: this.itemsPerPage,
        sort: ['date,desc']
      })
      .subscribe((res: HttpResponse<IComment[]>) => this.paginateComments(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.comments = [];
    this.loadComments();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadComments();
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ edition }) => {
      this.edition = edition;
      this.loadComments();
      this.registerChangeInComments();
      this.loadPosts();
    });
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IComment): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInComments(): void {
    this.eventSubscriber = this.eventManager.subscribe('commentListModification' + this.edition!.festivalId, () => this.reset());
  }

  protected paginateComments(data: IComment[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.comments.push(data[i]);
      }
    }
  }

  dateTime(date: Moment): string {
    date.locale(this.languageService.getCurrentLanguage());
    return date.format('D MMM YYYY HH:mm');
  }

  manageArtists(): void {
    if (this.edition) {
      const modalRef = this.modalService.open(ManageArtistsDialogComponent, { size: 'lg', backdrop: 'static' });
      modalRef.componentInstance.edition = this.edition;
    }
  }

  sendReport(): void {
    if (this.edition) {
      const modalRef = this.modalService.open(SendReportDialogComponent, { size: 'lg', backdrop: 'static' });
      modalRef.componentInstance.edition = this.edition;
    }
  }

  updatePoster(): void {
    if (this.edition) {
      const modalRef = this.modalService.open(UpdatePosterDialogComponent, { size: 'lg', backdrop: 'static' });
      modalRef.componentInstance.edition = this.edition;
    }
  }

  setFollow(follow: boolean): void {
    if (this.edition && this.edition.festivalName) {
      if (follow) {
        this.festivalService.follow(this.edition.festivalName).subscribe(() => this.setFollowed(true));
      } else {
        this.festivalService.unfollow(this.edition.festivalName).subscribe(() => this.setFollowed(false));
      }
    }
  }

  setFollowed(followed: boolean): void {
    if (this.edition) {
      this.edition.followed = followed;
    }
  }

  festivalDate(startDate: Moment | undefined, endDate: Moment | undefined): string {
    if (startDate && endDate) {
      startDate.locale(this.languageService.getCurrentLanguage());
      endDate.locale(this.languageService.getCurrentLanguage());
      return startDate.month() === endDate.month()
        ? `${startDate.format('D')} - ${endDate.format('D MMM')}`
        : `${startDate.format('D MMM')} - ${endDate.format('D MMM')}`;
    } else {
      return '';
    }
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  previousState(): void {
    window.history.back();
  }

  addComment(): void {
    this.isSaving = true;
    const comment = this.createFromForm();
    this.subscribeToSaveResponse(this.commentService.create(comment));
  }

  private createFromForm(): IComment {
    return {
      ...new Comment(),
      text: this.commentForm.get(['text'])!.value,
      festivalId: this.edition!.festivalId
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IComment>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.eventManager.broadcast('commentListModification' + this.edition!.festivalId);
    this.commentForm.reset();
    this.isSaving = false;
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
