import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiLanguageService } from 'ng-jhipster';

import { Router } from '@angular/router';
import { IEdition } from 'app/shared/model/edition.model';
import { EditionService } from 'app/entities/edition/edition.service';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { ArtistService } from 'app/entities/artist/artist.service';
import { Artist, IArtist } from 'app/shared/model/artist.model';

@Component({
  selector: 'jhi-account-delete-dialog',
  templateUrl: './manage-artists-dialog.component.html'
})
export class ManageArtistsDialogComponent implements OnInit {
  isSaving = false;
  edition?: IEdition;

  artists: IArtist[] = [];

  filteredArtists: IArtist[] = [];

  constructor(
    private editionService: EditionService,
    private artistService: ArtistService,
    private languageService: JhiLanguageService,
    public activeModal: NgbActiveModal,
    private eventManager: JhiEventManager,
    private router: Router,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.artists = [];
    if (this.edition!.artists) {
      this.artists = this.edition!.artists;
    }
  }

  findArtists(event: any): void {
    this.filteredArtists = [new Artist(undefined, event.query, undefined)];
    if (event.query.length > 2) {
      this.artistService.findByName(event.query).subscribe((res: HttpResponse<IArtist[]>) => {
        if (res.body != null && res.body.length > 0) {
          this.filteredArtists =
            res.body[0].name!.toLowerCase() === event.query.toLowerCase() ? res.body : this.filteredArtists.concat(res.body);
        }
      });
    }
  }

  clear(): void {
    this.activeModal.dismiss();
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEdition>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.activeModal.close();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  save(): void {
    if (this.edition) {
      this.edition.artists = this.artists;
      this.subscribeToSaveResponse(this.editionService.updateArtists(this.edition.id!, this.artists));
    }
  }
}
