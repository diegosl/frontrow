import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEdition } from 'app/shared/model/edition.model';
import { EditionService } from 'app/entities/edition/edition.service';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { JhiDataUtils, JhiEventManager, JhiEventWithContent, JhiFileLoadError } from 'ng-jhipster';
import { AlertError } from 'app/shared/alert/alert-error.model';

@Component({
  selector: 'jhi-account-delete-dialog',
  templateUrl: './update-poster-dialog.component.html'
})
export class UpdatePosterDialogComponent implements OnInit {
  isSaving = false;
  edition?: IEdition;

  posterForm = this.fb.group({
    poster: [null, [Validators.required]],
    posterContentType: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    private editionService: EditionService,
    public activeModal: NgbActiveModal,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {}

  clear(): void {
    this.activeModal.dismiss();
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEdition>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.activeModal.close();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.posterForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('frontrowApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  save(): void {
    if (this.edition) {
      this.subscribeToSaveResponse(
        this.editionService.updatePoster(
          this.edition.id!,
          this.posterForm.get('poster')!.value,
          this.posterForm.get('posterContentType')!.value
        )
      );
    }
  }
}
