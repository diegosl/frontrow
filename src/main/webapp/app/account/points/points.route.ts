import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Authority } from 'app/shared/constants/authority.constants';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { PointsComponent } from 'app/account/points/points.component';

export const pointsRoute: Route = {
  path: 'points',
  component: PointsComponent,
  resolve: {
    pagingParams: JhiResolvePagingParams
  },
  data: {
    authorities: [Authority.USER],
    defaultSort: 'date,desc',
    pageTitle: 'global.menu.account.points'
  },
  canActivate: [UserRouteAccessService]
};
