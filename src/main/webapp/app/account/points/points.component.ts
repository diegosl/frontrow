import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

import { IContribution } from 'app/shared/model/contribution.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { ContributionService } from 'app/entities/contribution/contribution.service';

@Component({
  selector: 'jhi-points',
  templateUrl: './points.component.html'
})
export class PointsComponent implements OnInit {
  contributions?: IContribution[];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  points: number | undefined;
  badges: Map<string, string> | undefined;

  constructor(protected contributionService: ContributionService, protected activatedRoute: ActivatedRoute, protected router: Router) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.page;

    this.contributionService
      .queryMine({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IContribution[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );

    this.contributionService.myPoints().subscribe((res: HttpResponse<number>) => {
      if (res.body) {
        this.points = res.body;
      }
    });

    this.contributionService.myBadges().subscribe((res: HttpResponse<Map<string, string>>) => {
      if (res.body) {
        this.badges = res.body;
      }
    });
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
  }

  trackId(index: number, item: IContribution): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: IContribution[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['account/points'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.contributions = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}
