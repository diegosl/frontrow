import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FrontrowSharedModule } from 'app/shared/shared.module';

import { PasswordStrengthBarComponent } from './password/password-strength-bar.component';
import { RegisterComponent } from './register/register.component';
import { ActivateComponent } from './activate/activate.component';
import { PasswordComponent } from './password/password.component';
import { PasswordResetInitComponent } from './password-reset/init/password-reset-init.component';
import { PasswordResetFinishComponent } from './password-reset/finish/password-reset-finish.component';
import { SettingsComponent } from './settings/settings.component';
import { accountState } from './account.route';
import { DeleteAccountDialogComponent } from './settings/delete-account-dialog.component';
import { PointsComponent } from 'app/account/points/points.component';

@NgModule({
  imports: [FrontrowSharedModule, RouterModule.forChild(accountState)],
  declarations: [
    ActivateComponent,
    RegisterComponent,
    PasswordComponent,
    PasswordStrengthBarComponent,
    PasswordResetInitComponent,
    PasswordResetFinishComponent,
    SettingsComponent,
    DeleteAccountDialogComponent,
    PointsComponent
  ],
  entryComponents: [DeleteAccountDialogComponent]
})
export class AccountModule {}
