import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Router } from '@angular/router';
import { LoginService } from 'app/core/login/login.service';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'jhi-account-delete-dialog',
  templateUrl: './delete-account-dialog.component.html'
})
export class DeleteAccountDialogComponent {
  constructor(
    private loginService: LoginService,
    private accountService: AccountService,
    public activeModal: NgbActiveModal,
    private eventManager: JhiEventManager,
    private router: Router
  ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(): void {
    this.accountService.delete().subscribe(() => {
      this.eventManager.broadcast('userListModification');
      this.activeModal.close();
      this.loginService.logout();
      this.router.navigate(['']);
    });
  }
}
