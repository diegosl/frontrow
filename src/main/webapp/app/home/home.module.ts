import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FrontrowSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

@NgModule({
  imports: [FrontrowSharedModule, RouterModule.forChild([HOME_ROUTE]), GooglePlaceModule],
  declarations: [HomeComponent]
})
export class FrontrowHomeModule {}
