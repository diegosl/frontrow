import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { IEdition } from 'app/shared/model/edition.model';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { EditionService } from 'app/entities/edition/edition.service';
import { JhiParseLinks, JhiLanguageService } from 'ng-jhipster';
import { Moment } from 'moment';
import { IPost } from 'app/shared/model/post.model';
import { PostService } from 'app/entities/post/post.service';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { IArtist } from 'app/shared/model/artist.model';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  editions: IEdition[];
  posts: IPost[];
  authSubscription?: Subscription;
  links: any;
  latitude: number | undefined;
  longitude: number | undefined;
  currentSearch = '';
  options: Options = new Options({ types: ['(cities)'], fields: ['place_id', 'geometry', 'formatted_address'] });

  constructor(
    private languageService: JhiLanguageService,
    protected editionService: EditionService,
    protected postService: PostService,
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    protected parseLinks: JhiParseLinks,
    protected activatedRoute: ActivatedRoute
  ) {
    this.editions = [];
    this.posts = [];
  }

  postDate(date: Moment): string {
    date.locale(this.languageService.getCurrentLanguage());
    return date.format('D MMM YYYY HH:mm');
  }

  festivalDate(startDate: Moment, endDate: Moment): string {
    startDate.locale(this.languageService.getCurrentLanguage());
    endDate.locale(this.languageService.getCurrentLanguage());
    return startDate.month() === endDate.month()
      ? `${startDate.format('D')} - ${endDate.format('D MMM')}`
      : `${startDate.format('D MMM')} - ${endDate.format('D MMM')}`;
  }

  commaSeparatedArtists(artists: IArtist[]): string {
    return artists.map(artist => artist.name).join(', ');
  }

  loadPosts(): void {
    this.postService.getLatest(20).subscribe((res: HttpResponse<IPost[]>) => this.onSuccessPosts(res.body));
  }

  loadFollowedEditions(): void {
    this.editionService.getFollowed(20).subscribe((res: HttpResponse<IEdition[]>) => this.onSuccessEditions(res.body));
  }

  protected onSuccessPosts(data: IPost[] | null): void {
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.posts.push(data[i]);
      }
    }
  }

  protected onSuccessEditions(data: IEdition[] | null): void {
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.editions.push(data[i]);
      }
    }
  }

  trackPostId(index: number, item: IPost): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  trackEditionId(index: number, item: IEdition): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  public handleAddressChange(address: Address): void {
    this.latitude = address.geometry.location.lat();
    this.longitude = address.geometry.location.lng();
    this.currentSearch = address.formatted_address;
  }

  ngOnInit(): void {
    this.loadPosts();
    this.loadFollowedEditions();
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }
}
