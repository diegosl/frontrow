import { Moment } from 'moment';

export interface IComment {
  id?: number;
  text?: string;
  date?: Moment;
  postId?: number;
  festivalId?: number;
  authorLogin?: string;
  authorImageUrl?: string;
  authorId?: number;
  authorBadges?: Map<string, string>;
}

export class Comment implements IComment {
  constructor(
    public id?: number,
    public text?: string,
    public date?: Moment,
    public postId?: number,
    public festivalId?: number,
    public authorLogin?: string,
    public authorImageUrl?: string,
    public authorId?: number,
    public authorBadges?: Map<string, string>
  ) {}
}
