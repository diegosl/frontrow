import { Moment } from 'moment';

export interface IPost {
  id?: number;
  title?: string;
  handle?: string;
  content?: any;
  template?: string;
  date?: Moment;
  authorLogin?: string;
  authorId?: number;
  editorLogin?: string;
  editorId?: number;
  editionName?: string;
  editionYear?: number;
  editionId?: number;
  logoContentType?: string;
  logo?: any;
}

export class Post implements IPost {
  constructor(
    public id?: number,
    public title?: string,
    public handle?: string,
    public content?: any,
    public template?: string,
    public date?: Moment,
    public authorLogin?: string,
    public authorId?: number,
    public editorLogin?: string,
    public editorId?: number,
    public editionName?: string,
    public editionYear?: number,
    public editionId?: number,
    public logoContentType?: string,
    public logo?: any
  ) {}
}
