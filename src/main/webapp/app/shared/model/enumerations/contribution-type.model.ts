export const enum ContributionType {
  COMMENT = 'COMMENT',

  REPORT = 'REPORT',

  COLLABORATION = 'COLLABORATION'
}
