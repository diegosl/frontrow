import { IEdition } from 'app/shared/model/edition.model';

export interface IFestival {
  id?: number;
  name?: string;
  logoContentType?: string;
  logo?: any;
  web?: string;
  facebook?: string;
  twitter?: string;
  instagram?: string;
  editions?: IEdition[];
}

export class Festival implements IFestival {
  constructor(
    public id?: number,
    public name?: string,
    public logoContentType?: string,
    public logo?: any,
    public web?: string,
    public facebook?: string,
    public twitter?: string,
    public instagram?: string,
    public editions?: IEdition[]
  ) {}
}
