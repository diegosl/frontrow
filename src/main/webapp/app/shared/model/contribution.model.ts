import { Moment } from 'moment';
import { ContributionType } from 'app/shared/model/enumerations/contribution-type.model';

export interface IContribution {
  id?: number;
  points?: number;
  date?: Moment;
  type?: ContributionType;
  festivalId?: number;
  festivalName?: string;
  userLogin?: string;
  userId?: number;
}

export class Contribution implements IContribution {
  constructor(
    public id?: number,
    public points?: number,
    public date?: Moment,
    public type?: ContributionType,
    public festivalId?: number,
    public festivalName?: string,
    public userLogin?: string,
    public userId?: number
  ) {}
}
