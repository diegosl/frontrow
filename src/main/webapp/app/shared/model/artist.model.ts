import { IEdition } from 'app/shared/model/edition.model';

export interface IArtist {
  id?: number;
  name?: string;
  editions?: IEdition[];
}

export class Artist implements IArtist {
  constructor(public id?: number, public name?: string, public editions?: IEdition[]) {}
}
