import { Moment } from 'moment';
import { IPost } from 'app/shared/model/post.model';
import { IArtist } from 'app/shared/model/artist.model';

export interface IEdition {
  id?: number;
  location?: string;
  latitude?: number;
  longitude?: number;
  startDate?: Moment;
  endDate?: Moment;
  posterContentType?: string;
  poster?: any;
  description?: string;
  spotifyPlaylist?: string;
  canceled?: boolean;
  visible?: boolean;
  report?: string;
  originalEdition?: number;
  price?: string;
  schedule?: string;
  posts?: IPost[];
  artists?: IArtist[];
  festivalName?: string;
  logoContentType?: string;
  logo?: any;
  web?: string;
  facebook?: string;
  twitter?: string;
  instagram?: string;
  festivalId?: number;
  followed?: boolean;
  originalEditionInfo?: IEdition;
}

export class Edition implements IEdition {
  constructor(
    public id?: number,
    public location?: string,
    public latitude?: number,
    public longitude?: number,
    public startDate?: Moment,
    public endDate?: Moment,
    public posterContentType?: string,
    public poster?: any,
    public description?: string,
    public spotifyPlaylist?: string,
    public canceled?: boolean,
    public visible?: boolean,
    public report?: string,
    public originalEdition?: number,
    public price?: string,
    public schedule?: string,
    public posts?: IPost[],
    public artists?: IArtist[],
    public festivalName?: string,
    public logoContentType?: string,
    public logo?: any,
    public web?: string,
    public facebook?: string,
    public twitter?: string,
    public instagram?: string,
    public festivalId?: number,
    public followed?: boolean,
    public originalEditionInfo?: IEdition
  ) {
    this.canceled = this.canceled || false;
    this.visible = this.visible || false;
  }
}

export interface IEditionUserForm {
  id?: number;
  location?: string;
  latitude?: number;
  longitude?: number;
  name?: string;
  startDate?: Moment;
  endDate?: Moment;
  posterContentType?: string;
  poster?: any;
  artists?: IArtist[];
}

export class EditionUserForm implements IEditionUserForm {
  constructor(
    public id?: number,
    public location?: string,
    public latitude?: number,
    public longitude?: number,
    public name?: string,
    public startDate?: Moment,
    public endDate?: Moment,
    public posterContentType?: string,
    public poster?: any,
    public artists?: IArtist[]
  ) {}
}
