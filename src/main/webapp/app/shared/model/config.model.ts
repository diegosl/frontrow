export interface IConfig {
  id?: number;
  name?: string;
  value?: string;
}

export class Config implements IConfig {
  constructor(public id?: number, public name?: string, public value?: string) {}
}
