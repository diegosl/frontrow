import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, debounceTime } from 'rxjs/operators';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IEdition, IEditionUserForm, EditionUserForm } from 'app/shared/model/edition.model';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { FestivalService } from 'app/entities/festival/festival.service';
import { EditionService } from 'app/entities/edition/edition.service';
import { Artist, IArtist } from 'app/shared/model/artist.model';
import { ArtistService } from 'app/entities/artist/artist.service';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

@Component({
  selector: 'jhi-edition-update',
  templateUrl: './add-festival.component.html'
})
export class AddFestivalComponent implements OnInit {
  isSaving = false;

  festivals: string[] = [];
  startDateDp: any;
  endDateDp: any;

  filteredArtists: IArtist[] = [];

  options: Options = new Options({ types: ['(cities)'], fields: ['place_id', 'geometry', 'formatted_address'] });

  editForm = this.fb.group({
    id: [],
    location: [],
    latitude: [],
    longitude: [],
    name: [null, [Validators.required]],
    startDate: [],
    endDate: [],
    poster: [],
    posterContentType: [],
    artists: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected editionService: EditionService,
    protected festivalService: FestivalService,
    protected artistService: ArtistService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  searchFestival = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => (term === '' ? [] : this.festivals.filter(f => f.toLowerCase().includes(term.toLowerCase())).slice(0, 10)))
    );

  ngOnInit(): void {
    this.updateForm(new EditionUserForm());

    this.festivalService
      .getNames()
      .pipe(
        map((res: HttpResponse<string[]>) => {
          return res.body ? res.body : [];
        })
      )
      .subscribe((resBody: string[]) => (this.festivals = resBody));
  }

  findArtists(event: any): void {
    this.filteredArtists = [new Artist(undefined, event.query, undefined)];
    if (event.query.length > 2) {
      this.artistService.findByName(event.query).subscribe((res: HttpResponse<IArtist[]>) => {
        if (res.body != null && res.body.length > 0) {
          this.filteredArtists =
            res.body[0].name!.toLowerCase() === event.query.toLowerCase() ? res.body : this.filteredArtists.concat(res.body);
        }
      });
    }
  }

  updateFullname(): void {
    document.getElementById('fullname')!.textContent =
      (this.editForm.get('name')!.value ? this.editForm.get('name')!.value : '') +
      '\u00a0' +
      (this.editForm.get('startDate')!.value ? this.editForm.get('startDate')!.value.year() : '');
    this.editForm.get('endDate')!.setValue(this.editForm.get('startDate')!.value);
  }

  updateForm(edition: IEditionUserForm): void {
    this.editForm.patchValue({
      id: edition.id,
      location: edition.location,
      latitude: edition.latitude,
      longitude: edition.longitude,
      name: edition.name,
      startDate: edition.startDate,
      endDate: edition.endDate,
      poster: edition.poster,
      posterContentType: edition.posterContentType,
      artists: edition.artists
    });
  }

  public handleAddressChange(address: Address): void {
    this.editForm.get(['latitude'])!.setValue(address.geometry.location.lat());
    this.editForm.get(['longitude'])!.setValue(address.geometry.location.lng());
    this.editForm.get(['location'])!.setValue(address.formatted_address);
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('frontrowApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const edition = this.createFromForm();
    this.subscribeToSaveResponse(this.editionService.createFestival(edition));
  }

  private createFromForm(): IEditionUserForm {
    return {
      ...new EditionUserForm(),
      id: this.editForm.get(['id'])!.value,
      location: this.editForm.get(['location'])!.value,
      latitude: this.editForm.get(['latitude'])!.value,
      longitude: this.editForm.get(['longitude'])!.value,
      name: this.editForm.get(['name'])!.value,
      startDate: this.editForm.get(['startDate'])!.value,
      endDate: this.editForm.get(['endDate'])!.value,
      posterContentType: this.editForm.get(['posterContentType'])!.value,
      poster: this.editForm.get(['poster'])!.value,
      artists: this.editForm.get(['artists'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEdition>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
