import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FrontrowSharedModule } from 'app/shared/shared.module';

import { AddFestivalComponent } from './add-festival.component';

import { addFestivalRoute } from './add-festival.route';
import { AutoCompleteModule } from 'primeng';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

@NgModule({
  imports: [FrontrowSharedModule, RouterModule.forChild([addFestivalRoute]), AutoCompleteModule, GooglePlaceModule],
  declarations: [AddFestivalComponent]
})
export class AddFestivalModule {}
