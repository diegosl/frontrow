import { Route } from '@angular/router';

import { AddFestivalComponent } from './add-festival.component';

export const addFestivalRoute: Route = {
  path: '',
  component: AddFestivalComponent,
  data: {
    pageTitle: 'frontrowApp.festival.home.title'
  }
};
