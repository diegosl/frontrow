import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { FrontrowSharedModule } from 'app/shared/shared.module';
import { FrontrowCoreModule } from 'app/core/core.module';
import { FrontrowAppRoutingModule } from './app-routing.module';
import { FrontrowHomeModule } from './home/home.module';
import { FrontrowEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FrontrowFestivalEditionModule } from './festival/festival-edition.module';
import { FrontrowPostUserModule } from 'app/post/post-user-detail.module';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FrontrowSharedModule,
    FrontrowCoreModule,
    FrontrowHomeModule,
    FrontrowFestivalEditionModule,
    FrontrowPostUserModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    FrontrowEntityModule,
    FrontrowAppRoutingModule,
    GooglePlaceModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent]
})
export class FrontrowAppModule {}
