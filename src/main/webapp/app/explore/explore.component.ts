import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { JhiParseLinks, JhiLanguageService } from 'ng-jhipster';

import { IEdition } from 'app/shared/model/edition.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { EditionService } from 'app/entities/edition/edition.service';
import { Moment } from 'moment';
import { ActivatedRoute } from '@angular/router';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { IArtist } from 'app/shared/model/artist.model';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { ArtistService } from 'app/entities/artist/artist.service';
import * as moment from 'moment';

@Component({
  selector: 'jhi-explore',
  templateUrl: './explore.component.html',
  animations: [
    trigger('smoothCollapse', [
      state(
        'initial',
        style({
          height: '0',
          overflow: '',
          opacity: '0'
        })
      ),
      state(
        'final',
        style({
          overflow: '',
          opacity: '1'
        })
      ),
      transition('initial=>final', animate('750ms')),
      transition('final=>initial', animate('750ms'))
    ])
  ]
})
export class ExploreComponent implements OnInit {
  isCollapsed = true;
  fromDateDp: any;
  toDateDp: any;
  editions: IEdition[];
  itemsPerPage: number;
  links: any;
  page: number;
  radio: number | undefined;
  latitude: number | undefined;
  longitude: number | undefined;
  fromDate: any;
  toDate: any;
  artists: IArtist[] = [];
  filteredArtists: IArtist[] = [];
  currentSearch = '';
  options: Options = new Options({ types: ['(cities)'], fields: ['place_id', 'geometry'] });

  constructor(
    private languageService: JhiLanguageService,
    protected editionService: EditionService,
    protected artistService: ArtistService,
    protected parseLinks: JhiParseLinks,
    protected activatedRoute: ActivatedRoute
  ) {
    this.editions = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.radio =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['radio']
        ? this.activatedRoute.snapshot.queryParams['radio']
        : undefined;
    this.latitude =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['lat']
        ? this.activatedRoute.snapshot.queryParams['lat']
        : undefined;
    this.longitude =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['lng']
        ? this.activatedRoute.snapshot.queryParams['lng']
        : undefined;
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['loc']
        ? this.activatedRoute.snapshot.queryParams['loc']
        : '';
    this.fromDate = moment(new Date());
  }

  festivalDate(startDate: Moment, endDate: Moment): string {
    startDate.locale(this.languageService.getCurrentLanguage());
    endDate.locale(this.languageService.getCurrentLanguage());
    return startDate.month() === endDate.month()
      ? `${startDate.format('D')} - ${endDate.format('D MMM')}`
      : `${startDate.format('D MMM')} - ${endDate.format('D MMM')}`;
  }

  commaSeparatedArtists(artists: IArtist[]): string {
    return artists.map(artist => artist.name).join(', ');
  }

  loadAll(): void {
    const req = {
      page: this.page,
      size: this.itemsPerPage
    };
    if (this.latitude && this.longitude) {
      this.radio = this.radio ? this.radio : 10;
      req['radio'] = this.radio;
      req['latitude'] = this.latitude;
      req['longitude'] = this.longitude;
    }
    if (this.fromDate) {
      req['fromDate'] = this.fromDate && this.fromDate.isValid() ? this.fromDate.format(DATE_FORMAT) : undefined;
    }
    if (this.toDate) {
      req['toDate'] = this.toDate && this.toDate.isValid() ? this.toDate.format(DATE_FORMAT) : undefined;
    }
    if (this.artists.length > 0) {
      req['artists'] = this.artists.map(artist => artist.id);
    }
    this.editionService.queryFiltered(req).subscribe((res: HttpResponse<IEdition[]>) => this.paginateEditions(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.editions = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  public handleAddressChange(address: Address): void {
    this.latitude = address.geometry.location.lat();
    this.longitude = address.geometry.location.lng();
  }

  search(): void {
    this.editions = [];
    this.links = {
      last: 0
    };
    this.page = 0;
    this.loadAll();
  }

  clear(): void {
    this.radio = undefined;
    this.latitude = undefined;
    this.longitude = undefined;
    this.currentSearch = '';
    this.fromDate = undefined;
    this.toDate = undefined;
    this.artists = [];
    this.search();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  findArtists(event: any): void {
    if (event.query.length > 2) {
      this.artistService.findByName(event.query).subscribe((res: HttpResponse<IArtist[]>) => {
        if (res.body != null && res.body.length > 0) {
          this.filteredArtists = res.body;
        }
      });
    }
  }

  trackId(index: number, item: IEdition): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  protected paginateEditions(data: IEdition[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.editions.push(data[i]);
      }
    }
  }
}
