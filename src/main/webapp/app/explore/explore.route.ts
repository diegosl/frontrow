import { Routes } from '@angular/router';

import { ExploreComponent } from './explore.component';

export const exploreRoute: Routes = [
  {
    path: '',
    component: ExploreComponent,
    data: {
      pageTitle: 'global.menu.explore'
    }
  }
];
