import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FrontrowSharedModule } from 'app/shared/shared.module';

import { ExploreComponent } from './explore.component';

import { exploreRoute } from './explore.route';
import { AutoCompleteModule } from 'primeng';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

@NgModule({
  imports: [FrontrowSharedModule, RouterModule.forChild(exploreRoute), AutoCompleteModule, GooglePlaceModule],
  declarations: [ExploreComponent]
})
export class ExploreModule {}
