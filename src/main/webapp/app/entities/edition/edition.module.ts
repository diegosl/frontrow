import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FrontrowSharedModule } from 'app/shared/shared.module';
import { EditionComponent } from './edition.component';
import { EditionDetailComponent } from './edition-detail.component';
import { EditionUpdateComponent } from './edition-update.component';
import { EditionDeleteDialogComponent } from './edition-delete-dialog.component';
import { editionRoute } from './edition.route';

@NgModule({
  imports: [FrontrowSharedModule, RouterModule.forChild(editionRoute)],
  declarations: [EditionComponent, EditionDetailComponent, EditionUpdateComponent, EditionDeleteDialogComponent],
  entryComponents: [EditionDeleteDialogComponent]
})
export class FrontrowEditionModule {}
