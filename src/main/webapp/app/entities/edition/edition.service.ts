import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IEdition, IEditionUserForm } from 'app/shared/model/edition.model';
import { IArtist } from 'app/shared/model/artist.model';

type EntityResponseType = HttpResponse<IEdition>;
type EntityArrayResponseType = HttpResponse<IEdition[]>;

@Injectable({ providedIn: 'root' })
export class EditionService {
  public resourceUrl = SERVER_API_URL + 'api/editions';

  constructor(protected http: HttpClient) {}

  create(edition: IEdition): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(edition);
    return this.http
      .post<IEdition>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  createFestival(edition: IEditionUserForm): Observable<EntityResponseType> {
    const copy: IEditionUserForm = Object.assign({}, edition, {
      startDate: edition.startDate && edition.startDate.isValid() ? edition.startDate.format(DATE_FORMAT) : undefined,
      endDate: edition.endDate && edition.endDate.isValid() ? edition.endDate.format(DATE_FORMAT) : undefined
    });
    return this.http
      .post<IEdition>(`${this.resourceUrl}/festival`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(edition: IEdition): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(edition);
    return this.http
      .put<IEdition>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  validate(edition: IEdition, createPost: boolean, awardPoints: boolean): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(edition);
    return this.http
      .put<IEdition>(`${this.resourceUrl}/validate`, copy, {
        observe: 'response',
        params: { createPost: createPost.toString(), awardPoints: awardPoints.toString() }
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  updateArtists(id: number, artists: IArtist[]): Observable<EntityResponseType> {
    return this.http
      .put<IEdition>(`${this.resourceUrl}/${id}/artists`, artists, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  updatePoster(id: number, file: any, fileContentType: string): Observable<EntityResponseType> {
    return this.http
      .put<IEdition>(`${this.resourceUrl}/${id}/poster`, { file, fileContentType }, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  sendReport(id: number, report: string): Observable<EntityResponseType> {
    return this.http
      .put<IEdition>(`${this.resourceUrl}/${id}/report`, report, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  getFollowed(items: number): Observable<EntityArrayResponseType> {
    return this.http
      .get<IEdition[]>(`${this.resourceUrl}/followed/${items}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IEdition>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  countValidations(): Observable<HttpResponse<number>> {
    return this.http.get<number>(`${this.resourceUrl}/validate/count`, { observe: 'response' });
  }

  findByName(festival: string, edition: number): Observable<EntityResponseType> {
    return this.http
      .get<IEdition>(`${this.resourceUrl}/${festival}/${edition}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  queryFiltered(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEdition[]>(`${this.resourceUrl}/filtered`, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEdition[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  getValidations(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEdition[]>(`${this.resourceUrl}/validate`, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(edition: IEdition): IEdition {
    const copy: IEdition = Object.assign({}, edition, {
      startDate: edition.startDate && edition.startDate.isValid() ? edition.startDate.format(DATE_FORMAT) : undefined,
      endDate: edition.endDate && edition.endDate.isValid() ? edition.endDate.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate ? moment(res.body.startDate) : undefined;
      res.body.endDate = res.body.endDate ? moment(res.body.endDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((edition: IEdition) => {
        edition.startDate = edition.startDate ? moment(edition.startDate) : undefined;
        edition.endDate = edition.endDate ? moment(edition.endDate) : undefined;
      });
    }
    return res;
  }
}
