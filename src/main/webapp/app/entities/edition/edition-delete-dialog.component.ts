import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEdition } from 'app/shared/model/edition.model';
import { EditionService } from './edition.service';

@Component({
  templateUrl: './edition-delete-dialog.component.html'
})
export class EditionDeleteDialogComponent {
  edition?: IEdition;

  constructor(protected editionService: EditionService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.editionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('editionListModification');
      this.activeModal.close();
    });
  }
}
