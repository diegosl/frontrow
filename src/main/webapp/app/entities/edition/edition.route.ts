import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IEdition, Edition } from 'app/shared/model/edition.model';
import { EditionService } from './edition.service';
import { EditionComponent } from './edition.component';
import { EditionDetailComponent } from './edition-detail.component';
import { EditionUpdateComponent } from './edition-update.component';

@Injectable({ providedIn: 'root' })
export class EditionResolve implements Resolve<IEdition> {
  constructor(private service: EditionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEdition> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((edition: HttpResponse<Edition>) => {
          if (edition.body) {
            return of(edition.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Edition());
  }
}

export const editionRoute: Routes = [
  {
    path: '',
    component: EditionComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'frontrowApp.edition.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: EditionDetailComponent,
    resolve: {
      edition: EditionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'frontrowApp.edition.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: EditionUpdateComponent,
    resolve: {
      edition: EditionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'frontrowApp.edition.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: EditionUpdateComponent,
    resolve: {
      edition: EditionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'frontrowApp.edition.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
