import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IEdition, Edition } from 'app/shared/model/edition.model';
import { EditionService } from './edition.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { IArtist } from 'app/shared/model/artist.model';
import { ArtistService } from 'app/entities/artist/artist.service';
import { IFestival } from 'app/shared/model/festival.model';
import { FestivalService } from 'app/entities/festival/festival.service';

type SelectableEntity = IArtist | IFestival;

@Component({
  selector: 'jhi-edition-update',
  templateUrl: './edition-update.component.html'
})
export class EditionUpdateComponent implements OnInit {
  isSaving = false;
  artists: IArtist[] = [];
  festivals: IFestival[] = [];
  startDateDp: any;
  endDateDp: any;

  editForm = this.fb.group({
    id: [],
    location: [],
    latitude: [],
    longitude: [],
    startDate: [null, [Validators.required]],
    endDate: [],
    poster: [],
    posterContentType: [],
    description: [],
    spotifyPlaylist: [],
    canceled: [],
    visible: [],
    report: [],
    originalEdition: [],
    price: [null, [Validators.maxLength(7)]],
    schedule: [],
    artists: [],
    festivalId: [null, Validators.required]
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected editionService: EditionService,
    protected artistService: ArtistService,
    protected festivalService: FestivalService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ edition }) => {
      this.updateForm(edition);

      this.artistService.query().subscribe((res: HttpResponse<IArtist[]>) => (this.artists = res.body || []));

      this.festivalService.query().subscribe((res: HttpResponse<IFestival[]>) => (this.festivals = res.body || []));
    });
  }

  updateForm(edition: IEdition): void {
    this.editForm.patchValue({
      id: edition.id,
      location: edition.location,
      latitude: edition.latitude,
      longitude: edition.longitude,
      startDate: edition.startDate,
      endDate: edition.endDate,
      poster: edition.poster,
      posterContentType: edition.posterContentType,
      description: edition.description,
      spotifyPlaylist: edition.spotifyPlaylist,
      canceled: edition.canceled,
      visible: edition.visible,
      report: edition.report,
      originalEdition: edition.originalEdition,
      price: edition.price,
      schedule: edition.schedule,
      artists: edition.artists,
      festivalId: edition.festivalId
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('frontrowApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const edition = this.createFromForm();
    if (edition.id !== undefined) {
      this.subscribeToSaveResponse(this.editionService.update(edition));
    } else {
      this.subscribeToSaveResponse(this.editionService.create(edition));
    }
  }

  private createFromForm(): IEdition {
    return {
      ...new Edition(),
      id: this.editForm.get(['id'])!.value,
      location: this.editForm.get(['location'])!.value,
      latitude: this.editForm.get(['latitude'])!.value,
      longitude: this.editForm.get(['longitude'])!.value,
      startDate: this.editForm.get(['startDate'])!.value,
      endDate: this.editForm.get(['endDate'])!.value,
      posterContentType: this.editForm.get(['posterContentType'])!.value,
      poster: this.editForm.get(['poster'])!.value,
      description: this.editForm.get(['description'])!.value,
      spotifyPlaylist: this.editForm.get(['spotifyPlaylist'])!.value,
      canceled: this.editForm.get(['canceled'])!.value,
      visible: this.editForm.get(['visible'])!.value,
      report: this.editForm.get(['report'])!.value,
      originalEdition: this.editForm.get(['originalEdition'])!.value,
      price: this.editForm.get(['price'])!.value,
      schedule: this.editForm.get(['schedule'])!.value,
      artists: this.editForm.get(['artists'])!.value,
      festivalId: this.editForm.get(['festivalId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEdition>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IArtist[], option: IArtist): IArtist {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
