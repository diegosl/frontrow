import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IContribution, Contribution } from 'app/shared/model/contribution.model';
import { ContributionService } from './contribution.service';
import { IFestival } from 'app/shared/model/festival.model';
import { FestivalService } from 'app/entities/festival/festival.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

type SelectableEntity = IFestival | IUser;

@Component({
  selector: 'jhi-contribution-update',
  templateUrl: './contribution-update.component.html'
})
export class ContributionUpdateComponent implements OnInit {
  isSaving = false;
  festivals: IFestival[] = [];
  users: IUser[] = [];
  dateDp: any;

  editForm = this.fb.group({
    id: [],
    points: [null, [Validators.required]],
    date: [null, [Validators.required]],
    type: [null, [Validators.required]],
    festivalId: [],
    userId: [null, Validators.required]
  });

  constructor(
    protected contributionService: ContributionService,
    protected festivalService: FestivalService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ contribution }) => {
      this.updateForm(contribution);

      this.festivalService.query().subscribe((res: HttpResponse<IFestival[]>) => (this.festivals = res.body || []));

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(contribution: IContribution): void {
    this.editForm.patchValue({
      id: contribution.id,
      points: contribution.points,
      date: contribution.date,
      type: contribution.type,
      festivalId: contribution.festivalId,
      userId: contribution.userId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const contribution = this.createFromForm();
    if (contribution.id !== undefined) {
      this.subscribeToSaveResponse(this.contributionService.update(contribution));
    } else {
      this.subscribeToSaveResponse(this.contributionService.create(contribution));
    }
  }

  private createFromForm(): IContribution {
    return {
      ...new Contribution(),
      id: this.editForm.get(['id'])!.value,
      points: this.editForm.get(['points'])!.value,
      date: this.editForm.get(['date'])!.value,
      type: this.editForm.get(['type'])!.value,
      festivalId: this.editForm.get(['festivalId'])!.value,
      userId: this.editForm.get(['userId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IContribution>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
