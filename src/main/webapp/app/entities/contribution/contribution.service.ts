import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IContribution } from 'app/shared/model/contribution.model';

type EntityResponseType = HttpResponse<IContribution>;
type EntityArrayResponseType = HttpResponse<IContribution[]>;

@Injectable({ providedIn: 'root' })
export class ContributionService {
  public resourceUrl = SERVER_API_URL + 'api/contributions';

  constructor(protected http: HttpClient) {}

  create(contribution: IContribution): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(contribution);
    return this.http
      .post<IContribution>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(contribution: IContribution): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(contribution);
    return this.http
      .put<IContribution>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IContribution>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IContribution[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  queryMine(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IContribution[]>(`${this.resourceUrl}/me`, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  myPoints(): Observable<HttpResponse<number>> {
    return this.http.get<number>(`${this.resourceUrl}/me/points`, { observe: 'response' });
  }

  myBadges(): Observable<HttpResponse<Map<string, string>>> {
    return this.http.get<Map<string, string>>(`${this.resourceUrl}/me/badges`, { observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(contribution: IContribution): IContribution {
    const copy: IContribution = Object.assign({}, contribution, {
      date: contribution.date && contribution.date.isValid() ? contribution.date.format(DATE_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? moment(res.body.date) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((contribution: IContribution) => {
        contribution.date = contribution.date ? moment(contribution.date) : undefined;
      });
    }
    return res;
  }
}
