import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFestival } from 'app/shared/model/festival.model';
import { FestivalService } from './festival.service';

@Component({
  templateUrl: './festival-delete-dialog.component.html'
})
export class FestivalDeleteDialogComponent {
  festival?: IFestival;

  constructor(protected festivalService: FestivalService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.festivalService.delete(id).subscribe(() => {
      this.eventManager.broadcast('festivalListModification');
      this.activeModal.close();
    });
  }
}
