import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFestival } from 'app/shared/model/festival.model';

type EntityResponseType = HttpResponse<IFestival>;
type EntityArrayResponseType = HttpResponse<IFestival[]>;

@Injectable({ providedIn: 'root' })
export class FestivalService {
  public resourceUrl = SERVER_API_URL + 'api/festivals';

  constructor(protected http: HttpClient) {}

  create(festival: IFestival): Observable<EntityResponseType> {
    return this.http.post<IFestival>(this.resourceUrl, festival, { observe: 'response' });
  }

  update(festival: IFestival): Observable<EntityResponseType> {
    return this.http.put<IFestival>(this.resourceUrl, festival, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFestival>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getNames(): Observable<HttpResponse<string[]>> {
    return this.http.get<string[]>(`${this.resourceUrl}/names`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFestival[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  follow(festival: string): Observable<HttpResponse<{}>> {
    return this.http.post(`${this.resourceUrl}/${festival}/follow`, null, { observe: 'response' });
  }

  unfollow(festival: string): Observable<HttpResponse<{}>> {
    return this.http.post(`${this.resourceUrl}/${festival}/unfollow`, null, { observe: 'response' });
  }
}
