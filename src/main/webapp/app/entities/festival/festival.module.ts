import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FrontrowSharedModule } from 'app/shared/shared.module';
import { FestivalComponent } from './festival.component';
import { FestivalDetailComponent } from './festival-detail.component';
import { FestivalUpdateComponent } from './festival-update.component';
import { FestivalDeleteDialogComponent } from './festival-delete-dialog.component';
import { festivalRoute } from './festival.route';

@NgModule({
  imports: [FrontrowSharedModule, RouterModule.forChild(festivalRoute)],
  declarations: [FestivalComponent, FestivalDetailComponent, FestivalUpdateComponent, FestivalDeleteDialogComponent],
  entryComponents: [FestivalDeleteDialogComponent]
})
export class FrontrowFestivalModule {}
