import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFestival, Festival } from 'app/shared/model/festival.model';
import { FestivalService } from './festival.service';
import { FestivalComponent } from './festival.component';
import { FestivalDetailComponent } from './festival-detail.component';
import { FestivalUpdateComponent } from './festival-update.component';

@Injectable({ providedIn: 'root' })
export class FestivalResolve implements Resolve<IFestival> {
  constructor(private service: FestivalService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFestival> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((festival: HttpResponse<Festival>) => {
          if (festival.body) {
            return of(festival.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Festival());
  }
}

export const festivalRoute: Routes = [
  {
    path: '',
    component: FestivalComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'frontrowApp.festival.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: FestivalDetailComponent,
    resolve: {
      festival: FestivalResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'frontrowApp.festival.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: FestivalUpdateComponent,
    resolve: {
      festival: FestivalResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'frontrowApp.festival.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: FestivalUpdateComponent,
    resolve: {
      festival: FestivalResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'frontrowApp.festival.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
