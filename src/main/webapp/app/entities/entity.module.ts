import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'artist',
        loadChildren: () => import('./artist/artist.module').then(m => m.FrontrowArtistModule)
      },
      {
        path: 'edition',
        loadChildren: () => import('./edition/edition.module').then(m => m.FrontrowEditionModule)
      },
      {
        path: 'festival',
        loadChildren: () => import('./festival/festival.module').then(m => m.FrontrowFestivalModule)
      },
      {
        path: 'post',
        loadChildren: () => import('./post/post.module').then(m => m.FrontrowPostModule)
      },
      {
        path: 'comment',
        loadChildren: () => import('./comment/comment.module').then(m => m.FrontrowCommentModule)
      },
      {
        path: 'contribution',
        loadChildren: () => import('./contribution/contribution.module').then(m => m.FrontrowContributionModule)
      },
      {
        path: 'config',
        loadChildren: () => import('./config/config.module').then(m => m.FrontrowConfigModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class FrontrowEntityModule {}
