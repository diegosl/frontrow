package es.festis.frontrow.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Edition.
 */
@Entity
@Table(name = "edition")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Edition extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "location")
    private String location;

    @Column(name = "latitude")
    private Float latitude;

    @Column(name = "longitude")
    private Float longitude;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Lob
    @Column(name = "poster")
    private byte[] poster;

    @Column(name = "poster_content_type")
    private String posterContentType;

    @Column(name = "description")
    private String description;

    @Column(name = "spotify_playlist")
    private String spotifyPlaylist;

    @Column(name = "canceled")
    private Boolean canceled;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "report")
    private String report;

    @Column(name = "original_edition")
    private Long originalEdition;

    @Size(max = 7)
    @Column(name = "price", length = 7)
    private String price;

    @Column(name = "schedule")
    private String schedule;

    @OneToMany(mappedBy = "edition")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Post> posts = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "edition_artist",
               joinColumns = @JoinColumn(name = "edition_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "artist_id", referencedColumnName = "id"))
    private Set<Artist> artists = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "editions", allowSetters = true)
    private Festival festival;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public Edition location(String location) {
        this.location = location;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Float getLatitude() {
        return latitude;
    }

    public Edition latitude(Float latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public Edition longitude(Float longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Edition startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Edition endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public byte[] getPoster() {
        return poster;
    }

    public Edition poster(byte[] poster) {
        this.poster = poster;
        return this;
    }

    public void setPoster(byte[] poster) {
        this.poster = poster;
    }

    public String getPosterContentType() {
        return posterContentType;
    }

    public Edition posterContentType(String posterContentType) {
        this.posterContentType = posterContentType;
        return this;
    }

    public void setPosterContentType(String posterContentType) {
        this.posterContentType = posterContentType;
    }

    public String getDescription() {
        return description;
    }

    public Edition description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpotifyPlaylist() {
        return spotifyPlaylist;
    }

    public Edition spotifyPlaylist(String spotifyPlaylist) {
        this.spotifyPlaylist = spotifyPlaylist;
        return this;
    }

    public void setSpotifyPlaylist(String spotifyPlaylist) {
        this.spotifyPlaylist = spotifyPlaylist;
    }

    public Boolean isCanceled() {
        return canceled;
    }

    public Edition canceled(Boolean canceled) {
        this.canceled = canceled;
        return this;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    public Boolean isVisible() {
        return visible;
    }

    public Edition visible(Boolean visible) {
        this.visible = visible;
        return this;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getReport() {
        return report;
    }

    public Edition report(String report) {
        this.report = report;
        return this;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public Long getOriginalEdition() {
        return originalEdition;
    }

    public Edition originalEdition(Long originalEdition) {
        this.originalEdition = originalEdition;
        return this;
    }

    public void setOriginalEdition(Long originalEdition) {
        this.originalEdition = originalEdition;
    }

    public String getPrice() {
        return price;
    }

    public Edition price(String price) {
        this.price = price;
        return this;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSchedule() {
        return schedule;
    }

    public Edition schedule(String schedule) {
        this.schedule = schedule;
        return this;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public Edition posts(Set<Post> posts) {
        this.posts = posts;
        return this;
    }

    public Edition addPost(Post post) {
        this.posts.add(post);
        post.setEdition(this);
        return this;
    }

    public Edition removePost(Post post) {
        this.posts.remove(post);
        post.setEdition(null);
        return this;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }

    public Set<Artist> getArtists() {
        return artists;
    }

    public Edition artists(Set<Artist> artists) {
        this.artists = artists;
        return this;
    }

    public Edition addArtist(Artist artist) {
        this.artists.add(artist);
        artist.getEditions().add(this);
        return this;
    }

    public Edition removeArtist(Artist artist) {
        this.artists.remove(artist);
        artist.getEditions().remove(this);
        return this;
    }

    public void setArtists(Set<Artist> artists) {
        this.artists = artists;
    }

    public Festival getFestival() {
        return festival;
    }

    public Edition festival(Festival festival) {
        this.festival = festival;
        return this;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Edition)) {
            return false;
        }
        return id != null && id.equals(((Edition) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Edition{" +
            "id=" + getId() +
            ", location='" + getLocation() + "'" +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", poster='" + getPoster() + "'" +
            ", posterContentType='" + getPosterContentType() + "'" +
            ", description='" + getDescription() + "'" +
            ", spotifyPlaylist='" + getSpotifyPlaylist() + "'" +
            ", canceled='" + isCanceled() + "'" +
            ", visible='" + isVisible() + "'" +
            ", report='" + getReport() + "'" +
            ", originalEdition=" + getOriginalEdition() +
            ", price='" + getPrice() + "'" +
            ", schedule='" + getSchedule() + "'" +
            "}";
    }
}
