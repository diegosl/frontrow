package es.festis.frontrow.domain.enumeration;

/**
 * The ContributionType enumeration.
 */
public enum ContributionType {
    COMMENT, REPORT, COLLABORATION
}
