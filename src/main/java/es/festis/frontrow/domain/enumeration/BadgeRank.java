package es.festis.frontrow.domain.enumeration;

public enum BadgeRank {
    GOLD ("#ecbe27"),
    SILVER ("#b6c2cc"),
    BRONZE ("#9c5843");

    private String color;

    BadgeRank(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }
}
