package es.festis.frontrow.domain.enumeration;

public enum Badge {
    HEADLINER ("crown"),
    LOYAL ("heart"),
    GLOBETROTTER ("globe-americas"),
    COMMENTATOR ("microphone"),
    COLLABORATOR ("hands-helping");

    private String icon;

    Badge(String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return this.icon;
    }
}

