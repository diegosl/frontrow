package es.festis.frontrow.domain;

public class PostTemplate {

    public static final String ONE_ARTIST_TEMPLATE = "frontrowApp.post.templates.oneArtist";

    public static final String TWO_ARTISTS_TEMPLATE = "frontrowApp.post.templates.twoArtists";

    public static final String MORE_ARTISTS_TEMPLATE = "frontrowApp.post.templates.moreArtists";

    private PostTemplate() {
        throw new IllegalStateException("Utility class");
    }

}
