package es.festis.frontrow.crawler;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import es.festis.frontrow.crawler.nominatim.NominatimClient;
import es.festis.frontrow.service.EditionService;
import es.festis.frontrow.service.dto.ArtistDTO;
import es.festis.frontrow.service.dto.EditionDTO;
import es.festis.frontrow.service.dto.FestivalDTO;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FestisCrawler extends WebCrawler {

    private final Logger log = LoggerFactory.getLogger(FestisCrawler.class);

    private static final Pattern VISIT_PATTERN = Pattern.compile("^https://festis.es/.*\\d\\d\\d\\d.*/$");

    private static final Pattern NOT_VISIT_PATTERN = Pattern.compile(".*(cronica|miscelanea).*");

    private static final Pattern PAGE_PATTERN = Pattern.compile("^https://festis.es/page/\\d*/*$");

    private static final Pattern LOGO_PATTERN = Pattern.compile("(.*)(-\\d*x\\d*)(\\.jpg|\\.png|\\.gif|\\.jpeg)$");

    private static final Pattern LOCATION_PATTERN = Pattern.compile(".*(?<=en )(.*)(?=\\.).*");

    private static final Pattern SPANISH_MONTHS_PATTERN =
        Pattern.compile("(enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiembre|octubre|noviembre|diciembre)");

    private static final Pattern YEAR_PATTERN = Pattern.compile("\\d\\d\\d\\d");

    private static final Pattern END_DATE_PATTERN = Pattern.compile("(\\d+ de (enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiembre|octubre|noviembre|diciembre).*)(?=( en))");

    private static final Locale LOCALE_ES = new Locale("es", "ES");

    private static final List<String> RESERVED_TAGS = Lists.newArrayList("internacional", "enero", "febrero", "marzo", "abril",
        "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre",
        "europa", "américa", "asia", "australia", "áfrica", "hungría", "dinamarca", "francia", "alemania", "italia", "bélgica",
        "suecia");

    private final XPath xPath = XPathFactory.newInstance().newXPath();

    private final EditionService editionService;

    private final ZonedDateTime lastCrawlDate;

    FestisCrawler(EditionService editionService, ZonedDateTime lastCrawlDate) {
        this.editionService = editionService;
        this.lastCrawlDate = lastCrawlDate;
    }

    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        return (VISIT_PATTERN.matcher(href).matches() || PAGE_PATTERN.matcher(href).matches())
            && !NOT_VISIT_PATTERN.matcher(href).matches();
    }

    @Override
    public void visit(Page page) {
        String url = page.getWebURL().getURL();
        log.debug("Visiting URL : {}", url);

        if (!(page.getParseData() instanceof HtmlParseData) || !VISIT_PATTERN.matcher(url).matches()) {
            return;
        }

        Document doc = null;
        try {
            String html = ((HtmlParseData) page.getParseData()).getHtml();
            HtmlCleaner cleaner = new HtmlCleaner();
            TagNode node = cleaner.clean(html);
            DomSerializer ser = new DomSerializer(cleaner.getProperties());
            doc = ser.createDOM(node);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return;
        }

        if (doc == null) {
            return;
        }

        try {
            String postDate = xPath.evaluate("//*[@class=\"posted-on\"]/time/@datetime", doc);
            if (ZonedDateTime.parse(postDate).isBefore(lastCrawlDate)) {
                return;
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
            return;
        }

        FestivalDTO festivalDTO = new FestivalDTO();
        EditionDTO editionDTO = new EditionDTO();
        try {
            fillNameLocationAndDates(doc, festivalDTO, editionDTO);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
            return;
        }

        fillFestivalData(doc, festivalDTO);
        fillEditionData(doc, festivalDTO, editionDTO);

        editionService.saveFromCrawler(festivalDTO, editionDTO);
    }

    private void fillNameLocationAndDates(Document doc, FestivalDTO festivalDTO, EditionDTO editionDTO)
        throws XPathExpressionException {
        String festivalName = xPath.evaluate("//*[@class=\"entry-content\"]/p[last()-2]/strong/text()", doc);
        festivalDTO.setName(festivalName.trim());
        String locationAndDates = xPath.evaluate("//*[@class=\"entry-content\"]/p[last()-2]/text()[2]", doc);
        if (!Strings.isNullOrEmpty(locationAndDates)) {
            Matcher locationMatcher = LOCATION_PATTERN.matcher(locationAndDates);
            if (locationMatcher.matches()) {
                String location = locationMatcher.group(1);
                editionDTO.setLocation(location);
                NominatimClient.geocode(location).ifPresent(geo -> {
                    editionDTO.setLatitude(geo.getLat().floatValue());
                    editionDTO.setLongitude(geo.getLon().floatValue());
                });
            }
            StringBuilder startDateStr = new StringBuilder(256);
            Matcher dayMatcher = Pattern.compile("\\d+").matcher(locationAndDates);
            Matcher monthMatcher = SPANISH_MONTHS_PATTERN.matcher(locationAndDates);
            Matcher yearMatcher = YEAR_PATTERN.matcher(locationAndDates);
            Matcher endDateMatcher = END_DATE_PATTERN.matcher(locationAndDates);
            if (dayMatcher.find() && monthMatcher.find() && yearMatcher.find() && endDateMatcher.find()) {
                startDateStr.append(dayMatcher.group(0)).append("-")
                    .append(monthMatcher.group(0)).append("-")
                    .append(yearMatcher.group(0));
                LocalDate startDate = LocalDate.parse(startDateStr, DateTimeFormatter.ofPattern("d-MMMM-yyyy")
                    .withLocale(LOCALE_ES));
                editionDTO.setStartDate(startDate);
                String endDateStr = endDateMatcher.group(0);
                LocalDate endDate = LocalDate.parse(endDateStr, DateTimeFormatter.ofPattern("d 'de' MMMM 'de' yyyy")
                    .withLocale(LOCALE_ES));
                editionDTO.setEndDate(endDate);
            }
        }
    }

    private void fillFestivalData(Document doc, FestivalDTO festivalDTO) {
        try {
            String web = xPath.evaluate("//*[@class=\"entry-content\"]/p[last()-1]/a[1]/@href", doc);
            festivalDTO.setWeb(web);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        try {
            String fb = xPath.evaluate("//*[@class=\"entry-content\"]/p[last()-1]/a[2]/@href", doc);
            festivalDTO.setFacebook(fb);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        try {
            String twitter = xPath.evaluate("//*[@class=\"entry-content\"]/p[last()-1]/a[3]/@href", doc);
            festivalDTO.setTwitter(twitter);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        try {
            String logo = xPath.evaluate("//*[@class=\"entry-thumbnail\"]/img[1]/@src", doc);
            Matcher logoMatcher = LOGO_PATTERN.matcher(logo);
            if (logoMatcher.matches()) {
                logo = logoMatcher.group(1) + logoMatcher.group(3);
                downloadImage(logo, festivalDTO, null);
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
    }

    private void fillEditionData(Document doc, FestivalDTO festivalDTO, EditionDTO editionDTO) {
        try {
            String poster = xPath.evaluate("//*[@class=\"entry-content\"]/p/img/@src", doc);
            downloadImage(poster, null, editionDTO);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        Set<ArtistDTO> artists = new HashSet<>();
        try {
            NodeList tagList =
                (NodeList) xPath.evaluate("//*[@class=\"tags-links\"]/a/text()", doc, XPathConstants.NODESET);
            for (int i = 0; i < tagList.getLength(); i++) {
                Node node = tagList.item(i);
                if (!node.getTextContent().equals(String.valueOf(editionDTO.getStartDate().getYear())) &&
                    !node.getTextContent().trim().equalsIgnoreCase(festivalDTO.getName().trim()) &&
                    !editionDTO.getLocation().toLowerCase().contains(node.getTextContent().toLowerCase()) &&
                    !RESERVED_TAGS.contains(node.getTextContent().toLowerCase())) {
                    ArtistDTO artist = new ArtistDTO();
                    artist.setName(node.getTextContent());
                    artists.add(artist);
                }
            }
            editionDTO.setArtists(artists);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
    }

    private static void downloadImage(String url, FestivalDTO festivalDTO, EditionDTO editionDTO) {
        try {
            URLConnection urlConnection = (new URL(url)).openConnection();

            // Read first bytes to use them for guessContentTypeFromStream
            int pushbackLimit = 100;
            InputStream urlStream = urlConnection.getInputStream();
            PushbackInputStream pushUrlStream = new PushbackInputStream(urlStream, pushbackLimit);
            byte[] firstBytes = new byte[pushbackLimit];
            if (pushUrlStream.read(firstBytes) < 0) {
                return;
            }
            pushUrlStream.unread(firstBytes);

            ByteArrayInputStream bais = new ByteArrayInputStream(firstBytes);
            String mimeType = URLConnection.guessContentTypeFromStream(bais);
            if (festivalDTO != null) {
                festivalDTO.setLogoContentType(mimeType);
            } else if (editionDTO != null ) {
                editionDTO.setPosterContentType(mimeType);
            }

            if (mimeType != null && mimeType.startsWith("image")) {
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                int nRead;
                byte[] data = new byte[1024];
                while ((nRead = pushUrlStream.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
                buffer.flush();
                byte[] image = buffer.toByteArray();

                if (festivalDTO != null) {
                    festivalDTO.setLogo(image);
                } else if (editionDTO != null ) {
                    editionDTO.setPoster(image);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
