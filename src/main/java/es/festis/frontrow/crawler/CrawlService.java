package es.festis.frontrow.crawler;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import es.festis.frontrow.service.EditionService;
import java.time.ZonedDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrawlService {

    @Autowired
    private EditionService editionService;

    public CrawlController makeCrawlController(CrawlConfig config, PageFetcher pageFetcher, RobotstxtServer robotstxtServer)
        throws Exception { //NOSONAR
        return new CrawlController(config, pageFetcher, robotstxtServer);
    }

    public void startCrawling() throws Exception { //NOSONAR
        String crawlStorageFolder = "./data/crawl/root";
        int numberOfCrawlers = 7;

        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(crawlStorageFolder);
        config.setPolitenessDelay(5000);
        config.setIncludeBinaryContentInCrawling(false);
        config.setUserAgentString("frontrow");

        // Instantiate the controller for this crawl.
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller = makeCrawlController(config, pageFetcher, robotstxtServer);

        // For each crawl, you need to add some seed urls. These are the first
        // URLs that are fetched and then the crawler starts following links
        // which are found in these pages
        controller.addSeed("https://festis.es");

        ZonedDateTime lastCrawlDate = editionService.getAndUpdateLastCrawlDate();

        // The factory which creates instances of crawlers.
        CrawlController.WebCrawlerFactory<FestisCrawler> factory = () -> new FestisCrawler(editionService, lastCrawlDate);

        controller.startNonBlocking(factory, numberOfCrawlers);
    }
}
