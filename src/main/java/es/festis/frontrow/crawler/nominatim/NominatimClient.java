package es.festis.frontrow.crawler.nominatim;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class NominatimClient {

    private static final String EMAIL = "d.lamas@udc.es";

    private static final String ENCODING = "UTF8";

    private NominatimClient() {
    }

    public static Optional<NominatimResponse> geocode(String query) {
        try {
            StringBuilder urlFactory = new StringBuilder(512);
            urlFactory.append("https://nominatim.openstreetmap.org/search?format=json&addressdetails=1&polygon=1")
                .append("&q=").append(URLEncoder.encode(query, ENCODING))
                .append("&email=").append(URLEncoder.encode(EMAIL, ENCODING));

            URLConnection connection = new URL(urlFactory.toString()).openConnection();

            Reader reader = new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8);
            Type listType = new TypeToken<ArrayList<NominatimResponse>>(){}.getType();
            List<NominatimResponse> result = new Gson().fromJson(reader, listType);
            return result.stream().findFirst();
        } catch (IOException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
