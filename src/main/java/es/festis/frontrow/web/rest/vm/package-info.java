/**
 * View Models used by Spring MVC REST controllers.
 */
package es.festis.frontrow.web.rest.vm;
