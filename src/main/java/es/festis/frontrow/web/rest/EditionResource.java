package es.festis.frontrow.web.rest;

import es.festis.frontrow.security.AuthoritiesConstants;
import es.festis.frontrow.service.EditionService;
import es.festis.frontrow.service.dto.ArtistDTO;
import es.festis.frontrow.service.dto.FileDTO;
import es.festis.frontrow.service.dto.SearchCriteria;
import es.festis.frontrow.web.rest.errors.BadRequestAlertException;
import es.festis.frontrow.service.dto.EditionDTO;
import es.festis.frontrow.service.dto.EditionFestivalDTO;
import es.festis.frontrow.service.dto.EditionCriteria;
import es.festis.frontrow.service.EditionQueryService;

import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link es.festis.frontrow.domain.Edition}.
 */
@RestController
@RequestMapping("/api")
public class EditionResource {

    private final Logger log = LoggerFactory.getLogger(EditionResource.class);

    private static final String ENTITY_NAME = "edition";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EditionService editionService;

    private final EditionQueryService editionQueryService;

    public EditionResource(EditionService editionService, EditionQueryService editionQueryService) {
        this.editionService = editionService;
        this.editionQueryService = editionQueryService;
    }

    /**
     * {@code POST  /editions} : Create a new edition.
     *
     * @param editionDTO the editionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new editionDTO, or with status {@code 400 (Bad Request)} if the edition has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/editions")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<EditionDTO> createEdition(@Valid @RequestBody EditionDTO editionDTO) throws URISyntaxException {
        log.debug("REST request to save Edition : {}", editionDTO);
        if (editionDTO.getId() != null) {
            throw new BadRequestAlertException("A new edition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EditionDTO result = editionService.save(editionDTO);
        return ResponseEntity.created(new URI("/api/editions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /editions/festival} : Create a new edition and a new festival (if the festival doesn't exist)
     *
     * @param editionFestivalDTO the editionFestivalDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new editionDTO, or with status {@code 400 (Bad Request)} if the edition has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/editions/festival")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\", \"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<EditionDTO> createEditionFestival(@Valid @RequestBody EditionFestivalDTO editionFestivalDTO) throws URISyntaxException {
        log.debug("REST request to save Edition and Festival : {}", editionFestivalDTO);
        if (editionFestivalDTO.getId() != null) {
            throw new BadRequestAlertException("A new edition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EditionDTO result = editionService.save(editionFestivalDTO);
        return ResponseEntity.created(new URI("/api/editions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME,
                result.getFestivalName() + ' ' + result.getStartDate().getYear()))
            .body(result);
    }

    /**
     * {@code PUT  /editions} : Updates an existing edition.
     *
     * @param editionDTO the editionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated editionDTO,
     * or with status {@code 400 (Bad Request)} if the editionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the editionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/editions")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<EditionDTO> updateEdition(@Valid @RequestBody EditionDTO editionDTO) throws URISyntaxException {
        log.debug("REST request to update Edition : {}", editionDTO);
        if (editionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EditionDTO result = editionService.save(editionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, editionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /editions/validate} : Validates an edition modification.
     *
     * @param editionDTO the editionDTO to update.
     * @param createPost whether to create news post or not.
     * @param awardPoints whether to award points or not.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated editionDTO,
     * or with status {@code 400 (Bad Request)} if the editionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the editionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/editions/validate")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\")")
    public ResponseEntity<EditionDTO> validateEdition(@Valid @RequestBody EditionDTO editionDTO, boolean createPost,
        boolean awardPoints) throws URISyntaxException {
        log.debug("REST request to validate edition : {} \n create post : {}", editionDTO, createPost);
        if (editionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EditionDTO result = editionService.validate(editionDTO, createPost, awardPoints);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, editionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /editions/:id/artists} : Updates the artists of the "id" edition.
     *
     * @param id the "id" of the edition.
     * @param artists the list of artists.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated editionDTO,
     * or with status {@code 500 (Internal Server Error)} if the edition couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/editions/{id}/artists")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\", \"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<EditionDTO> saveEditionArtists(@PathVariable Long id, @RequestBody List<ArtistDTO> artists) {
        log.debug("REST request to update artists of edition {}: {}", id, artists);
        EditionDTO result = editionService.saveArtists(id, artists);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .body(result);
    }

    /**
     * {@code PUT  /editions/:id/report} : Send feedback of "id" edition.
     *
     * @param id the "id" of the edition.
     * @param report the report.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated editionDTO,
     * or with status {@code 500 (Internal Server Error)} if the edition couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/editions/{id}/report")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\", \"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<EditionDTO> sendReport(@PathVariable Long id, @RequestBody String report) {
        log.debug("REST request to send report for edition {}: {}", id, report);
        EditionDTO result = editionService.sendReport(id, report);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .body(result);
    }


    /**
     * {@code PUT  /editions/:id/poster} : Submit new poster for "id" edition.
     *
     * @param id the "id" of the edition.
     * @param poster the new poster file.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated editionDTO,
     * or with status {@code 500 (Internal Server Error)} if the edition couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/editions/{id}/poster")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\", \"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<EditionDTO> updatePoster(@PathVariable Long id, @RequestBody FileDTO poster) {
        log.debug("REST request to submit new poster for edition {}: {}", id, poster);
        EditionDTO result = editionService.updatePoster(id, poster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .body(result);
    }

    /**
     * {@code GET  /editions/filtered} : get all the editions by filters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of editions in body.
     */
    @GetMapping("/editions/filtered")
    public ResponseEntity<List<EditionDTO>> getAllEditions(SearchCriteria criteria, Pageable pageable) {
        log.debug("REST request to get editions by criteria: {}", criteria);
        Page<EditionDTO> page = editionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /editions} : get all the editions.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of editions in body.
     */
    @GetMapping("/editions")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<EditionDTO>> getAllEditions(EditionCriteria criteria, Pageable pageable) {
        log.debug("REST request to get editions by criteria: {}", criteria);
        return paginatedEditions(criteria, pageable);
    }

    /**
     * {@code GET  /editions/count} : count all the editions.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/editions/count")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Long> countEditions(EditionCriteria criteria) {
        log.debug("REST request to count Editions by criteria: {}", criteria);
        return ResponseEntity.ok().body(editionQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /editions/validate/count} : count the pending validations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/editions/validate/count")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\")")
    public ResponseEntity<Long> countValidations() {
        log.debug("REST request to count editions to validate");
        EditionCriteria criteria = new EditionCriteria();
        criteria.setVisible((BooleanFilter) new BooleanFilter().setEquals(Boolean.FALSE));
        return ResponseEntity.ok().body(editionQueryService.countByCriteria(criteria));
    }

    private ResponseEntity<List<EditionDTO>> paginatedEditions(EditionCriteria criteria, Pageable pageable) {
        Page<EditionDTO> page = editionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /editions/validate} : get editions to validate.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of editions in body.
     */
    @GetMapping("/editions/validate")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\")")
    public ResponseEntity<List<EditionDTO>> getEditionsToValidate(Pageable pageable) {
        log.debug("REST request to get a page of editions to validate");
        EditionCriteria criteria = new EditionCriteria();
        criteria.setVisible((BooleanFilter) new BooleanFilter().setEquals(Boolean.FALSE));
        return paginatedEditions(criteria, pageable);
    }

    /**
     * {@code GET  /editions/followed/:items} : get the "item" next editions followed.
     *
     * @param items the number of editions to retrieve
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of posts in body.
     */
    @GetMapping("/editions/followed/{items}")
    public ResponseEntity<List<EditionDTO>> getFollowed(@PathVariable int items, Long edition) {
        log.debug("REST request to get the next {} editions followed", items);
        List<EditionDTO> editions = editionService.findFollowed(items);
        return ResponseEntity.ok().body(editions);
    }


    /**
     * {@code GET  /editions/:id} : get the "id" edition.
     *
     * @param id the id of the editionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the editionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/editions/{id}")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\")")
    public ResponseEntity<EditionDTO> getEdition(@PathVariable Long id) {
        log.debug("REST request to get Edition : {}", id);
        Optional<EditionDTO> editionDTO = editionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(editionDTO);
    }

    /**
     * {@code GET  /editions/:festival/:edition} : get the "festival" edition from the year "edition".
     *
     * @param festival the name of the festival of the editionDTO to retrieve.
     * @param edition the year of the festival to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the editionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/editions/{festival}/{edition}")
    public ResponseEntity<EditionDTO> getEdition(@PathVariable String festival, @PathVariable int edition) {
        log.debug("REST request to get festival edition : {} {}", festival, edition);
        Optional<EditionDTO> editionDTO = editionService.findFestivalEdition(festival, edition);
        return ResponseUtil.wrapOrNotFound(editionDTO);
    }

    /**
     * {@code DELETE  /editions/:id} : delete the "id" edition.
     *
     * @param id the id of the editionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/editions/{id}")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\")")
    public ResponseEntity<Void> deleteEdition(@PathVariable Long id) {
        log.debug("REST request to delete Edition : {}", id);

        editionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
