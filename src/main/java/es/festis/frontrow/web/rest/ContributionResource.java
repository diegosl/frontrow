package es.festis.frontrow.web.rest;

import es.festis.frontrow.security.AuthoritiesConstants;
import es.festis.frontrow.service.ContributionService;
import es.festis.frontrow.web.rest.errors.BadRequestAlertException;
import es.festis.frontrow.service.dto.ContributionDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link es.festis.frontrow.domain.Contribution}.
 */
@RestController
@RequestMapping("/api")
public class ContributionResource {

    private final Logger log = LoggerFactory.getLogger(ContributionResource.class);

    private static final String ENTITY_NAME = "contribution";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContributionService contributionService;

    public ContributionResource(ContributionService contributionService) {
        this.contributionService = contributionService;
    }

    /**
     * {@code POST  /contributions} : Create a new contribution.
     *
     * @param contributionDTO the contributionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contributionDTO, or with status {@code 400 (Bad Request)} if the contribution has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contributions")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ContributionDTO> createContribution(@Valid @RequestBody ContributionDTO contributionDTO) throws URISyntaxException {
        log.debug("REST request to save Contribution : {}", contributionDTO);
        if (contributionDTO.getId() != null) {
            throw new BadRequestAlertException("A new contribution cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContributionDTO result = contributionService.save(contributionDTO);
        return ResponseEntity.created(new URI("/api/contributions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contributions} : Updates an existing contribution.
     *
     * @param contributionDTO the contributionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contributionDTO,
     * or with status {@code 400 (Bad Request)} if the contributionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contributionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contributions")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ContributionDTO> updateContribution(@Valid @RequestBody ContributionDTO contributionDTO) throws URISyntaxException {
        log.debug("REST request to update Contribution : {}", contributionDTO);
        if (contributionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContributionDTO result = contributionService.save(contributionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contributionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contributions} : get all the contributions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contributions in body.
     */
    @GetMapping("/contributions")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<ContributionDTO>> getAllContributions(Pageable pageable) {
        log.debug("REST request to get a page of Contributions");
        Page<ContributionDTO> page = contributionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contributions/me} : get all my contributions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contributions in body.
     */
    @GetMapping("/contributions/me")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\", \"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<List<ContributionDTO>> getMyContributions(Pageable pageable) {
        log.debug("REST request to get a page of my Contributions");
        Page<ContributionDTO> page = contributionService.findMine(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contributions/me/points} : get my total of points.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the total of points in body.
     */
    @GetMapping("/contributions/me/points")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\", \"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Long> getMyTotalPoints() {
        log.debug("REST request to get my total of points");
        Long totalOfPoints = contributionService.getMyTotalOfPoints();
        return ResponseEntity.ok().body(totalOfPoints);
    }

    /**
     * {@code GET  /contributions/me/badges} : get my badges.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the map of badges in body.
     */
    @GetMapping("/contributions/me/badges")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\", \"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Map<String, String>> getMyBadges() {
        log.debug("REST request to get my badges");
        Map<String, String> badges = contributionService.getMyBadges();
        return ResponseEntity.ok().body(badges);
    }

    /**
     * {@code GET  /contributions/:id} : get the "id" contribution.
     *
     * @param id the id of the contributionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contributionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contributions/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ContributionDTO> getContribution(@PathVariable Long id) {
        log.debug("REST request to get Contribution : {}", id);
        Optional<ContributionDTO> contributionDTO = contributionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contributionDTO);
    }

    /**
     * {@code DELETE  /contributions/:id} : delete the "id" contribution.
     *
     * @param id the id of the contributionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contributions/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteContribution(@PathVariable Long id) {
        log.debug("REST request to delete Contribution : {}", id);

        contributionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
