package es.festis.frontrow.web.rest.errors;

public class EmailAlreadyUsedException extends BadRequestAlertException { //NOSONAR

    private static final long serialVersionUID = 1L;

    public EmailAlreadyUsedException() {
        super(ErrorConstants.EMAIL_ALREADY_USED_TYPE, "Email is already in use!", "userManagement", "emailexists");
    }
}
