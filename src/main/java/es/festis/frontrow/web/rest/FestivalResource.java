package es.festis.frontrow.web.rest;

import es.festis.frontrow.crawler.CrawlService;
import es.festis.frontrow.security.AuthoritiesConstants;
import es.festis.frontrow.service.FestivalService;
import es.festis.frontrow.web.rest.errors.BadRequestAlertException;
import es.festis.frontrow.service.dto.FestivalDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link es.festis.frontrow.domain.Festival}.
 */
@RestController
@RequestMapping("/api")
public class FestivalResource {

    private final Logger log = LoggerFactory.getLogger(FestivalResource.class);

    private static final String ENTITY_NAME = "festival";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FestivalService festivalService;

    private final CrawlService crawlService;

    public FestivalResource(FestivalService festivalService, CrawlService crawlService) {
        this.festivalService = festivalService;
        this.crawlService = crawlService;
    }

    /**
     * {@code POST  /festivals} : Create a new festival.
     *
     * @param festivalDTO the festivalDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new festivalDTO, or with status {@code 400 (Bad Request)} if the festival has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/festivals")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<FestivalDTO> createFestival(@Valid @RequestBody FestivalDTO festivalDTO) throws URISyntaxException {
        log.debug("REST request to save Festival : {}", festivalDTO);
        if (festivalDTO.getId() != null) {
            throw new BadRequestAlertException("A new festival cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FestivalDTO result = festivalService.save(festivalDTO);
        return ResponseEntity.created(new URI("/api/festivals/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /festivals} : Updates an existing festival.
     *
     * @param festivalDTO the festivalDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated festivalDTO,
     * or with status {@code 400 (Bad Request)} if the festivalDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the festivalDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/festivals")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<FestivalDTO> updateFestival(@Valid @RequestBody FestivalDTO festivalDTO) throws URISyntaxException {
        log.debug("REST request to update Festival : {}", festivalDTO);
        if (festivalDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FestivalDTO result = festivalService.save(festivalDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, festivalDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /festivals} : get all the festivals.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of festivals in body.
     */
    @GetMapping("/festivals")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<List<FestivalDTO>> getAllFestivals(Pageable pageable) {
        log.debug("REST request to get a page of Festivals");
        Page<FestivalDTO> page = festivalService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /festivals/names} : get all the names of festivals.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of names of festivals in body.
     */
    @GetMapping("/festivals/names")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\", \"" + AuthoritiesConstants.USER + "\")")
    public List<String> getAllFestivalNames() {
        log.debug("REST request to get all festival names");
        return festivalService.findAllNames();
    }

    /**
     * {@code GET  /festivals/:id} : get the "id" festival.
     *
     * @param id the id of the festivalDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the festivalDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/festivals/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<FestivalDTO> getFestival(@PathVariable Long id) {
        log.debug("REST request to get Festival : {}", id);
        Optional<FestivalDTO> festivalDTO = festivalService.findOne(id);
        return ResponseUtil.wrapOrNotFound(festivalDTO);
    }

    /**
     * {@code POST  /festivals/:name/follow} : follow the "name" festival.
     *
     * @param name the name of the festivalDTO to follow.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @PostMapping("/festivals/{name}/follow")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\", \"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Void> followFestival(@PathVariable String name) {
        log.debug("REST request to follow Festival : {}", name);
        festivalService.follow(name);
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, "festival.followed", name)).build();
    }

    /**
     * {@code POST  /festivals/:name/unfollow} : unfollow the "name" festival.
     *
     * @param name the name of the festivalDTO to unfollow.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @PostMapping("/festivals/{name}/unfollow")
    @PreAuthorize("hasAnyAuthority(\"" + AuthoritiesConstants.ADMIN + "\", \"" + AuthoritiesConstants.EDITOR + "\", \"" + AuthoritiesConstants.USER + "\")")
    public ResponseEntity<Void> unfollowFestival(@PathVariable String name) {
        log.debug("REST request to unfollow Festival : {}", name);
        festivalService.unfollow(name);
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, "festival.unfollowed", name)).build();
    }

    /**
     * {@code POST  /festivals/crawl} : starts crawling festival data and storing it in the db.
     *
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @PostMapping("/festivals/crawl")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> crawlFestivals() {
        log.debug("REST request to start crawling festival data");
        try {
            crawlService.startCrawling();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, "festival.crawlingStarted",
            LocalDate.now().toString())).build();
    }

    /**
     * {@code GET  /festivals/followed} : get the followed festivals.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of festivals in body.
     */
    @GetMapping("/festivals/followed")
    public List<FestivalDTO> getFollowedFestivals() {
        log.debug("REST request to get the followed festivals");
        return festivalService.findFollowed();
    }

    /**
     * {@code DELETE  /festivals/:id} : delete the "id" festival.
     *
     * @param id the id of the festivalDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/festivals/{id}")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteFestival(@PathVariable Long id) {
        log.debug("REST request to delete Festival : {}", id);
        festivalService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
