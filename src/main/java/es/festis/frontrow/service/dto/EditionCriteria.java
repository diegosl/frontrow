package es.festis.frontrow.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link es.festis.frontrow.domain.Edition} entity. This class is used
 * in {@link es.festis.frontrow.web.rest.EditionResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /editions?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EditionCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter location;

    private FloatFilter latitude;

    private FloatFilter longitude;

    private LocalDateFilter startDate;

    private LocalDateFilter endDate;

    private StringFilter description;

    private StringFilter spotifyPlaylist;

    private BooleanFilter canceled;

    private BooleanFilter visible;

    private StringFilter report;

    private LongFilter originalEdition;

    private StringFilter price;

    private StringFilter schedule;

    private LongFilter postId;

    private LongFilter artistId;

    private LongFilter festivalId;

    public EditionCriteria() {
    }

    public EditionCriteria(EditionCriteria other) { //NOSONAR
        this.id = other.id == null ? null : other.id.copy();
        this.location = other.location == null ? null : other.location.copy();
        this.latitude = other.latitude == null ? null : other.latitude.copy();
        this.longitude = other.longitude == null ? null : other.longitude.copy();
        this.startDate = other.startDate == null ? null : other.startDate.copy();
        this.endDate = other.endDate == null ? null : other.endDate.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.spotifyPlaylist = other.spotifyPlaylist == null ? null : other.spotifyPlaylist.copy();
        this.canceled = other.canceled == null ? null : other.canceled.copy();
        this.visible = other.visible == null ? null : other.visible.copy();
        this.report = other.report == null ? null : other.report.copy();
        this.originalEdition = other.originalEdition == null ? null : other.originalEdition.copy();
        this.price = other.price == null ? null : other.price.copy();
        this.schedule = other.schedule == null ? null : other.schedule.copy();
        this.postId = other.postId == null ? null : other.postId.copy();
        this.artistId = other.artistId == null ? null : other.artistId.copy();
        this.festivalId = other.festivalId == null ? null : other.festivalId.copy();
    }

    @Override
    public EditionCriteria copy() {
        return new EditionCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLocation() {
        return location;
    }

    public void setLocation(StringFilter location) {
        this.location = location;
    }

    public FloatFilter getLatitude() {
        return latitude;
    }

    public void setLatitude(FloatFilter latitude) {
        this.latitude = latitude;
    }

    public FloatFilter getLongitude() {
        return longitude;
    }

    public void setLongitude(FloatFilter longitude) {
        this.longitude = longitude;
    }

    public LocalDateFilter getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateFilter startDate) {
        this.startDate = startDate;
    }

    public LocalDateFilter getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateFilter endDate) {
        this.endDate = endDate;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getSpotifyPlaylist() {
        return spotifyPlaylist;
    }

    public void setSpotifyPlaylist(StringFilter spotifyPlaylist) {
        this.spotifyPlaylist = spotifyPlaylist;
    }

    public BooleanFilter getCanceled() {
        return canceled;
    }

    public void setCanceled(BooleanFilter canceled) {
        this.canceled = canceled;
    }

    public BooleanFilter getVisible() {
        return visible;
    }

    public void setVisible(BooleanFilter visible) {
        this.visible = visible;
    }

    public StringFilter getReport() {
        return report;
    }

    public void setReport(StringFilter report) {
        this.report = report;
    }

    public LongFilter getOriginalEdition() {
        return originalEdition;
    }

    public void setOriginalEdition(LongFilter originalEdition) {
        this.originalEdition = originalEdition;
    }

    public StringFilter getPrice() {
        return price;
    }

    public void setPrice(StringFilter price) {
        this.price = price;
    }

    public StringFilter getSchedule() {
        return schedule;
    }

    public void setSchedule(StringFilter schedule) {
        this.schedule = schedule;
    }

    public LongFilter getPostId() {
        return postId;
    }

    public void setPostId(LongFilter postId) {
        this.postId = postId;
    }

    public LongFilter getArtistId() {
        return artistId;
    }

    public void setArtistId(LongFilter artistId) {
        this.artistId = artistId;
    }

    public LongFilter getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(LongFilter festivalId) {
        this.festivalId = festivalId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EditionCriteria that = (EditionCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(location, that.location) &&
            Objects.equals(latitude, that.latitude) &&
            Objects.equals(longitude, that.longitude) &&
            Objects.equals(startDate, that.startDate) &&
            Objects.equals(endDate, that.endDate) &&
            Objects.equals(description, that.description) &&
            Objects.equals(spotifyPlaylist, that.spotifyPlaylist) &&
            Objects.equals(canceled, that.canceled) &&
            Objects.equals(visible, that.visible) &&
            Objects.equals(report, that.report) &&
            Objects.equals(originalEdition, that.originalEdition) &&
            Objects.equals(price, that.price) &&
            Objects.equals(schedule, that.schedule) &&
            Objects.equals(postId, that.postId) &&
            Objects.equals(artistId, that.artistId) &&
            Objects.equals(festivalId, that.festivalId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        location,
        latitude,
        longitude,
        startDate,
        endDate,
        description,
        spotifyPlaylist,
        canceled,
        visible,
        report,
        originalEdition,
        price,
        schedule,
        postId,
        artistId,
        festivalId
        );
    }

    // prettier-ignore
    @Override
    public String toString() { //NOSONAR
        return "EditionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (location != null ? "location=" + location + ", " : "") +
                (latitude != null ? "latitude=" + latitude + ", " : "") +
                (longitude != null ? "longitude=" + longitude + ", " : "") +
                (startDate != null ? "startDate=" + startDate + ", " : "") +
                (endDate != null ? "endDate=" + endDate + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (spotifyPlaylist != null ? "spotifyPlaylist=" + spotifyPlaylist + ", " : "") +
                (canceled != null ? "canceled=" + canceled + ", " : "") +
                (visible != null ? "visible=" + visible + ", " : "") +
                (report != null ? "report=" + report + ", " : "") +
                (originalEdition != null ? "originalEdition=" + originalEdition + ", " : "") +
                (price != null ? "price=" + price + ", " : "") +
                (schedule != null ? "schedule=" + schedule + ", " : "") +
                (postId != null ? "postId=" + postId + ", " : "") +
                (artistId != null ? "artistId=" + artistId + ", " : "") +
                (festivalId != null ? "festivalId=" + festivalId + ", " : "") +
            "}";
    }

}
