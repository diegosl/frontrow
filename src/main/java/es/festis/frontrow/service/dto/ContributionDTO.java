package es.festis.frontrow.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import es.festis.frontrow.domain.enumeration.ContributionType;

/**
 * A DTO for the {@link es.festis.frontrow.domain.Contribution} entity.
 */
public class ContributionDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer points;

    @NotNull
    private LocalDate date;

    @NotNull
    private ContributionType type;


    private Long festivalId;

    private String festivalName;

    private Long userId;

    private String userLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public ContributionType getType() {
        return type;
    }

    public void setType(ContributionType type) {
        this.type = type;
    }

    public Long getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(Long festivalId) {
        this.festivalId = festivalId;
    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContributionDTO)) {
            return false;
        }

        return id != null && id.equals(((ContributionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContributionDTO{" +
            "id=" + getId() +
            ", points=" + getPoints() +
            ", date='" + getDate() + "'" +
            ", type='" + getType() + "'" +
            ", festivalId=" + getFestivalId() +
            ", festivalName=" + getFestivalName() +
            ", userId=" + getUserId() +
            ", userLogin='" + getUserLogin() + "'" +
            "}";
    }
}
