package es.festis.frontrow.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link es.festis.frontrow.domain.Edition} entity.
 */
public class EditionDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String location;

    private Float latitude;

    private Float longitude;

    @NotNull
    private LocalDate startDate;

    private LocalDate endDate;

    @Lob
    private byte[] poster;

    private String posterContentType;

    private String description;

    private String spotifyPlaylist;

    private Boolean canceled;

    private Boolean visible;

    private Boolean followed;

    private String report;

    private Long originalEdition;

    @Size(max = 7)
    private String price;

    private String schedule;

    private Set<ArtistDTO> artists = new HashSet<>();

    private Long festivalId;

    private String festivalName;

    private String web;

    private String facebook;

    private String instagram;

    private String twitter;

    @Lob
    private byte[] logo;

    private String logoContentType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public byte[] getPoster() {
        return poster;
    }

    public void setPoster(byte[] poster) {
        this.poster = poster;
    }

    public String getPosterContentType() {
        return posterContentType;
    }

    public void setPosterContentType(String posterContentType) {
        this.posterContentType = posterContentType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpotifyPlaylist() {
        return spotifyPlaylist;
    }

    public void setSpotifyPlaylist(String spotifyPlaylist) {
        this.spotifyPlaylist = spotifyPlaylist;
    }

    public Boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    public Boolean isVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean isFollowed() {
        return followed;
    }

    public String getReport() {
        return report;
    }

    public void setFollowed(Boolean followed) {
        this.followed = followed;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public Long getOriginalEdition() {
        return originalEdition;
    }

    public void setOriginalEdition(Long originalEdition) {
        this.originalEdition = originalEdition;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public Set<ArtistDTO> getArtists() {
        return artists;
    }

    public void setArtists(Set<ArtistDTO> artists) {
        this.artists = artists;
    }

    public Long getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(Long festivalId) {
        this.festivalId = festivalId;
    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EditionDTO)) {
            return false;
        }

        return id != null && id.equals(((EditionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EditionDTO{" +
            "id=" + getId() +
            ", location='" + getLocation() + "'" +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", description='" + getDescription() + "'" +
            ", spotifyPlaylist='" + getSpotifyPlaylist() + "'" +
            ", canceled='" + isCanceled() + "'" +
            ", visible='" + isVisible() + "'" +
            ", report='" + getReport() + "'" +
            ", originalEdition=" + getOriginalEdition() +
            ", price='" + getPrice() + "'" +
            ", schedule='" + getSchedule() + "'" +
            ", artists='" + getArtists() + "'" +
            ", followed='" + isFollowed() + "'" +
            ", festivalId=" + getFestivalId() +
            ", festivalName='" + getFestivalName() + "'" +
            "}";
    }
}
