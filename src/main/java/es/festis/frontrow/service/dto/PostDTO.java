package es.festis.frontrow.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link es.festis.frontrow.domain.Post} entity.
 */
public class PostDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 10, max = 100)
    private String title;

    @Size(min = 5, max = 50)
    private String handle;

    @Lob
    private String content;

    private String template;

    private Instant date;

    private Long authorId;

    private String authorLogin;

    private Long editorId;

    private String editorLogin;

    private Long editionId;

    private String editionName;

    @Lob
    private byte[] logo;

    private String logoContentType;

    private int editionYear;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long userId) {
        this.authorId = userId;
    }

    public String getAuthorLogin() {
        return authorLogin;
    }

    public void setAuthorLogin(String userLogin) {
        this.authorLogin = userLogin;
    }

    public Long getEditorId() {
        return editorId;
    }

    public void setEditorId(Long userId) {
        this.editorId = userId;
    }

    public String getEditorLogin() {
        return editorLogin;
    }

    public void setEditorLogin(String userLogin) {
        this.editorLogin = userLogin;
    }

    public Long getEditionId() {
        return editionId;
    }

    public void setEditionId(Long editionId) {
        this.editionId = editionId;
    }

    public String getEditionName() {
        return editionName;
    }

    public void setEditionName(String editionName) {
        this.editionName = editionName;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    public int getEditionYear() {
        return editionYear;
    }

    public void setEditionYear(int editionYear) {
        this.editionYear = editionYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PostDTO postDTO = (PostDTO) o;
        if (postDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), postDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PostDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", handle='" + getHandle() + "'" +
            ", content='" + getContent() + "'" +
            ", template='" + getTemplate() + "'" +
            ", date='" + getDate() + "'" +
            ", authorId=" + getAuthorId() +
            ", authorLogin='" + getAuthorLogin() + "'" +
            ", editorId=" + getEditorId() +
            ", editorLogin='" + getEditorLogin() + "'" +
            ", editionId=" + getEditionId() +
            ", editionName='" + getEditionName() + "'" +
            "}";
    }
}
