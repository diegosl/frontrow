package es.festis.frontrow.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Lob;

/**
 * A DTO for {@link es.festis.frontrow.domain.Edition} and {@link es.festis.frontrow.domain.Festival} entities.
 */
public class EditionFestivalDTO implements Serializable {

    private Long id;

    private String location;

    private Float latitude;

    private Float longitude;

    private LocalDate startDate;

    private LocalDate endDate;

    @Lob
    private byte[] poster;

    private String posterContentType;

    private String spotifyPlaylist;

    private String name;

    private Set<ArtistDTO> artists = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public byte[] getPoster() {
        return poster;
    }

    public void setPoster(byte[] poster) {
        this.poster = poster;
    }

    public String getPosterContentType() {
        return posterContentType;
    }

    public void setPosterContentType(String posterContentType) {
        this.posterContentType = posterContentType;
    }

    public String getSpotifyPlaylist() {
        return spotifyPlaylist;
    }

    public void setSpotifyPlaylist(String spotifyPlaylist) {
        this.spotifyPlaylist = spotifyPlaylist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ArtistDTO> getArtists() {
        return artists;
    }

    public void setArtists(Set<ArtistDTO> artists) {
        this.artists = artists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EditionDTO editionDTO = (EditionDTO) o;
        if (editionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), editionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EditionFestivalDTO{" +
            "id=" + getId() +
            ", location='" + getLocation() + "'" +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", poster='" + getPoster() + "'" +
            ", spotifyPlaylist='" + getSpotifyPlaylist() + "'" +
            ", name='" + getName() + "'" +
            ", artists='" + getArtists() + "'" +
            "}";
    }
}
