package es.festis.frontrow.service.dto;

import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

public class FileDTO {

    @Lob
    @NotNull
    private byte[] file;

    private String fileContentType;

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FileDTO{" +
            "file=" + getFile() +
            ", fileContentType='" + getFileContentType() + "'" +
            "}";
    }

}
