package es.festis.frontrow.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class SearchCriteria implements Serializable {

    private Float latitude;

    private Float longitude;

    private LocalDate fromDate;

    private LocalDate toDate;

    private Long radio;

    private Set<Long> artists = new HashSet<>();

    public Set<Long> getArtists() {
        return artists;
    }

    public void setArtists(Set<Long> artists) {
        this.artists = artists;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public Long getRadio() {
        return radio;
    }

    public void setRadio(Long radio) {
        this.radio = radio;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SearchCriteria{" +
            "latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", fromDate='" + getFromDate() + "'" +
            ", toDate='" + getToDate() + "'" +
            ", radio='" + getRadio() + "'" +
            ", artists='" + getArtists() + "'" +
            "}";
    }

}
