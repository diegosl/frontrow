package es.festis.frontrow.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * A DTO for the {@link es.festis.frontrow.domain.Comment} entity.
 */
public class CommentDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 2, max = 2000)
    private String text;

    private Instant date;

    private Long postId;

    private Long festivalId;

    private Long authorId;

    private String authorLogin;

    private String authorImageUrl;

    private Map<String, String> authorBadges = new HashMap<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Long getFestivalId() {
        return festivalId;
    }

    public void setFestivalId(Long festivalId) {
        this.festivalId = festivalId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long userId) {
        this.authorId = userId;
    }

    public String getAuthorLogin() {
        return authorLogin;
    }

    public void setAuthorLogin(String userLogin) {
        this.authorLogin = userLogin;
    }

    public String getAuthorImageUrl() {
        return authorImageUrl;
    }

    public void setAuthorImageUrl(String userImageUrl) {
        this.authorImageUrl = userImageUrl;
    }

    public Map<String, String> getAuthorBadges() {
        return authorBadges;
    }

    public void setAuthorBadges(Map<String, String> authorBadges) {
        this.authorBadges = authorBadges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CommentDTO commentDTO = (CommentDTO) o;
        if (commentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), commentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CommentDTO{" +
            "id=" + getId() +
            ", text='" + getText() + "'" +
            ", date='" + getDate() + "'" +
            ", postId=" + getPostId() +
            ", festivalId=" + getFestivalId() +
            ", authorId=" + getAuthorId() +
            ", authorLogin='" + getAuthorLogin() + "'" +
            ", authorImageUrl='" + getAuthorImageUrl() + "'" +
            ", authorBadges='" + getAuthorBadges() + "'" +
            "}";
    }
}
