package es.festis.frontrow.service;

import es.festis.frontrow.domain.Post;
import es.festis.frontrow.repository.PostRepository;
import es.festis.frontrow.service.dto.PostDTO;
import es.festis.frontrow.service.mapper.PostMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Post}.
 */
@Service
@Transactional
public class PostService {

    private final Logger log = LoggerFactory.getLogger(PostService.class);

    private final PostRepository postRepository;

    private final PostMapper postMapper;

    public PostService(PostRepository postRepository, PostMapper postMapper) {
        this.postRepository = postRepository;
        this.postMapper = postMapper;
    }

    /**
     * Save a post.
     *
     * @param postDTO the entity to save.
     * @return the persisted entity.
     */
    public PostDTO save(PostDTO postDTO) {
        log.debug("Request to save Post : {}", postDTO);
        Post post = postMapper.toEntity(postDTO);
        if (post.getHandle() == null) {
            post.setHandle(getAvailableHandle(post.getTitle()));
        }
        post = postRepository.save(post);
        return postMapper.toDto(post);
    }

    private String getNextHandle(String handle, int i) {
        String nextHandle = handle + "-" + i;
        if (postRepository.findByHandle(handle).isPresent()) {
            return getNextHandle(handle, i+1);
        }
        return nextHandle;
    }

    private String getAvailableHandle(String title) {
        String handle = title.toLowerCase().trim().replace(' ', '-');
        if (postRepository.findByHandle(handle).isPresent()) {
            handle = getNextHandle(handle, 1);
        }
        return handle;
    }

    /**
     * Get all the posts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PostDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Posts");
        return postRepository.findAll(pageable)
            .map(postMapper::toDto);
    }

    /**
     * Get the last "items" posts.
     *
     * @param items the number of posts to retrieve.
     * @param edition the edition to search posts by.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PostDTO> findLatest(int items, Long edition) {
        log.debug("Request to get latest {} Posts", items);
        return edition == null ? postRepository.findLatest(items).stream()
            .map(postMapper::toDto).collect(Collectors.toList())
            : postRepository.findLatestByEdition(items, edition).stream()
            .map(postMapper::toDto).collect(Collectors.toList());
    }

    /**
     * Get one post by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PostDTO> findOne(Long id) {
        log.debug("Request to get Post : {}", id);
        return postRepository.findById(id)
            .map(postMapper::toDto);
    }

    /**
     * Get one post by handle.
     *
     * @param handle the handle of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PostDTO> findOneByHandle(String handle) {
        log.debug("Request to get Post : {}", handle);
        return postRepository.findByHandle(handle)
            .map(postMapper::toDto);
    }

    /**
     * Delete the post by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Post : {}", id);
        postRepository.deleteById(id);
    }
}
