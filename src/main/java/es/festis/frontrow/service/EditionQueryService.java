package es.festis.frontrow.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import es.festis.frontrow.service.dto.SearchCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import es.festis.frontrow.domain.Edition;
import es.festis.frontrow.domain.*; // for static metamodels
import es.festis.frontrow.repository.EditionRepository;
import es.festis.frontrow.service.dto.EditionCriteria;
import es.festis.frontrow.service.dto.EditionDTO;
import es.festis.frontrow.service.mapper.EditionMapper;

/**
 * Service for executing complex queries for {@link Edition} entities in the database.
 * The main input is a {@link EditionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EditionDTO} or a {@link Page} of {@link EditionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EditionQueryService extends QueryService<Edition> {

    private final Logger log = LoggerFactory.getLogger(EditionQueryService.class);

    private final EditionRepository editionRepository;

    private final EditionMapper editionMapper;

    public EditionQueryService(EditionRepository editionRepository, EditionMapper editionMapper) {
        this.editionRepository = editionRepository;
        this.editionMapper = editionMapper;
    }

    /**
     * Return a {@link List} of {@link EditionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EditionDTO> findByCriteria(EditionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Edition> specification = createSpecification(criteria);
        return editionMapper.toDto(editionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EditionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EditionDTO> findByCriteria(EditionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Edition> specification = createSpecification(criteria);
        return editionRepository.findAll(specification, page)
            .map(editionMapper::toDto);
    }

    /**
     * Return a {@link Page} of {@link EditionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EditionDTO> findByCriteria(SearchCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        return editionRepository.findBySearchCriteriaCustom(criteria.getLatitude(), criteria.getLongitude(), criteria.getRadio(),
            criteria.getFromDate(), criteria.getToDate(),  criteria.getArtists(), page).map(editionMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EditionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Edition> specification = createSpecification(criteria);
        return editionRepository.count(specification);
    }

    /**
     * Function to convert {@link EditionCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Edition> createSpecification(EditionCriteria criteria) { //NOSONAR
        Specification<Edition> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null && specification != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Edition_.id));
            }
            if (criteria.getLocation() != null && specification != null) {
                specification = specification.and(buildStringSpecification(criteria.getLocation(), Edition_.location));
            }
            if (criteria.getLatitude() != null && specification != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLatitude(), Edition_.latitude));
            }
            if (criteria.getLongitude() != null && specification != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLongitude(), Edition_.longitude));
            }
            if (criteria.getStartDate() != null && specification != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartDate(), Edition_.startDate));
            }
            if (criteria.getEndDate() != null && specification != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndDate(), Edition_.endDate));
            }
            if (criteria.getDescription() != null && specification != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Edition_.description));
            }
            if (criteria.getSpotifyPlaylist() != null && specification != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpotifyPlaylist(), Edition_.spotifyPlaylist));
            }
            if (criteria.getCanceled() != null && specification != null) {
                specification = specification.and(buildSpecification(criteria.getCanceled(), Edition_.canceled));
            }
            if (criteria.getVisible() != null && specification != null) {
                specification = specification.and(buildSpecification(criteria.getVisible(), Edition_.visible));
            }
            if (criteria.getReport() != null && specification != null) {
                specification = specification.and(buildStringSpecification(criteria.getReport(), Edition_.report));
            }
            if (criteria.getOriginalEdition() != null && specification != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOriginalEdition(), Edition_.originalEdition));
            }
            if (criteria.getPrice() != null && specification != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrice(), Edition_.price));
            }
            if (criteria.getSchedule() != null && specification != null) {
                specification = specification.and(buildStringSpecification(criteria.getSchedule(), Edition_.schedule));
            }
            if (criteria.getPostId() != null && specification != null) {
                specification = specification.and(buildSpecification(criteria.getPostId(),
                    root -> root.join(Edition_.posts, JoinType.LEFT).get(Post_.id)));
            }
            if (criteria.getArtistId() != null && specification != null) {
                specification = specification.and(buildSpecification(criteria.getArtistId(),
                    root -> root.join(Edition_.artists, JoinType.LEFT).get(Artist_.id)));
            }
            if (criteria.getFestivalId() != null && specification != null) {
                specification = specification.and(buildSpecification(criteria.getFestivalId(),
                    root -> root.join(Edition_.festival, JoinType.LEFT).get(Festival_.id)));
            }
        }
        return specification;
    }
}
