package es.festis.frontrow.service;

import com.google.common.collect.Lists;
import es.festis.frontrow.domain.*;
import es.festis.frontrow.repository.*;
import es.festis.frontrow.security.SecurityUtils;
import es.festis.frontrow.service.dto.*;
import es.festis.frontrow.service.mapper.ArtistMapper;
import es.festis.frontrow.service.mapper.EditionFestivalMapper;
import es.festis.frontrow.service.mapper.EditionMapper;

import es.festis.frontrow.service.mapper.FestivalMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.collect.Sets;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static es.festis.frontrow.domain.PostTemplate.*;

/**
 * Service Implementation for managing {@link Edition}.
 */
@Service
@Transactional
public class EditionService {

    private final Logger log = LoggerFactory.getLogger(EditionService.class);

    @PersistenceContext
    EntityManager entityManager;

    private final EditionRepository editionRepository;

    private final FestivalRepository festivalRepository;

    private final ArtistRepository artistRepository;

    private final PostRepository postRepository;

    private final ConfigRepository configRepository;

    private final FestivalMapper festivalMapper;

    private final EditionMapper editionMapper;

    private final ArtistMapper artistMapper;

    private final EditionFestivalMapper editionFestivalMapper;

    private final UserRepository userRepository;

    private final ContributionService contributionService;

    private static final String EDITION_NOT_FOUND = "Couldn't find edition";


    public EditionService(EditionRepository editionRepository, FestivalRepository festivalRepository, PostRepository postRepository, //NOSONAR
        ConfigRepository configRepository, EditionMapper editionMapper, EditionFestivalMapper editionFestivalMapper, FestivalMapper festivalMapper,
        UserRepository userRepository, ArtistRepository artistRepository, ArtistMapper artistMapper, ContributionService contributionService) {
        this.editionRepository = editionRepository;
        this.festivalRepository = festivalRepository;
        this.postRepository = postRepository;
        this.configRepository = configRepository;
        this.festivalMapper = festivalMapper;
        this.editionMapper = editionMapper;
        this.editionFestivalMapper = editionFestivalMapper;
        this.userRepository = userRepository;
        this.artistRepository = artistRepository;
        this.artistMapper = artistMapper;
        this.contributionService = contributionService;
    }

    /**
     * Save a edition.
     *
     * @param editionDTO the entity to save.
     * @return the persisted entity.
     */
    public EditionDTO save(EditionDTO editionDTO) {
        log.debug("Request to save Edition : {}", editionDTO);
        Edition edition = editionMapper.toEntity(editionDTO);
        edition = editionRepository.save(edition);
        return editionMapper.toDto(edition);
    }

    /**
     * Validates an edition.
     *
     * @param editionDTO the edition to validate.
     * @param createPost whether to create news post or not.
     * @param awardPoints whether to award points or not.
     * @return the persisted entity.
     */
    public EditionDTO validate(EditionDTO editionDTO, boolean createPost, boolean awardPoints) {
        log.debug("Request to validate edition : {}", editionDTO);

        String authorLogin = editionRepository.getOne(editionDTO.getId()).getCreatedBy();

        Festival festival = festivalRepository.getOne(editionDTO.getFestivalId());
        festival = festival.facebook(editionDTO.getFacebook()).instagram(editionDTO.getInstagram()).logo(editionDTO.getLogo())
            .logoContentType(editionDTO.getLogoContentType()).name(editionDTO.getFestivalName()).twitter(editionDTO.getTwitter())
            .web(editionDTO.getWeb());
        festivalRepository.save(festival);

        Edition edition;
        if (editionDTO.getOriginalEdition() != null) {
            edition = editionRepository.findOneWithEagerRelationships(editionDTO.getOriginalEdition()).orElseThrow(() ->
                new RuntimeException("Invalid original edition id"));
            edition = edition.canceled(editionDTO.isCanceled()).description(editionDTO.getDescription()).startDate(editionDTO.getStartDate())
                .endDate(editionDTO.getEndDate()).latitude(editionDTO.getLatitude()).longitude(editionDTO.getLongitude())
                .location(editionDTO.getLocation()).spotifyPlaylist(editionDTO.getSpotifyPlaylist()).poster(editionDTO.getPoster())
                .posterContentType(editionDTO.getPosterContentType()).schedule(editionDTO.getSchedule()).price(editionDTO.getPrice());
            editionRepository.deleteById(editionDTO.getId());
        } else if (Boolean.TRUE.equals(editionDTO.isVisible())) {
            edition = editionRepository.findOneWithEagerRelationships(editionDTO.getId()).orElseThrow(() ->
                new RuntimeException("Invalid edition id"));
            edition = edition.canceled(editionDTO.isCanceled()).description(editionDTO.getDescription()).startDate(editionDTO.getStartDate())
                .endDate(editionDTO.getEndDate()).latitude(editionDTO.getLatitude()).longitude(editionDTO.getLongitude())
                .location(editionDTO.getLocation()).spotifyPlaylist(editionDTO.getSpotifyPlaylist()).poster(editionDTO.getPoster())
                .posterContentType(editionDTO.getPosterContentType()).schedule(editionDTO.getSchedule()).price(editionDTO.getPrice());
        } else {
            edition = editionMapper.toEntity(editionDTO);
            edition.setVisible(Boolean.TRUE);
            edition.setReport(StringUtils.EMPTY);
        }

        List<Artist> artists = artistMapper.toEntity(new ArrayList<>(editionDTO.getArtists()));

        if (createPost) {
            List<String> artistNamesBeforeUpdate = edition.getArtists().stream().map(Artist::getName).collect(Collectors.toList());
            List<String> newArtists = artists.stream().filter(a -> !artistNamesBeforeUpdate.contains(a.getName())).map(Artist::getName)
                .collect(Collectors.toList());
            postArtists(newArtists, edition);
        }

        edition.setArtists(Sets.newHashSet());
        for (Artist artist: artists) {
            edition.addArtist(artist.getId() != null ? artist : artistRepository.findByNameIgnoreCase(artist.getName()).orElseGet(() -> artistRepository.save(artist)));
        }

        edition = editionRepository.save(edition);
        if (Boolean.FALSE.equals(editionDTO.isVisible()) && awardPoints) {
            contributionService.createContributionByValidatedEdition(authorLogin, editionDTO);
        }
        return editionMapper.toDto(edition);
    }

    private void postArtists(List<String> artists, Edition edition) {
        if (!artists.isEmpty()) {
            Post post = new Post().date(Instant.now()).edition(edition).title(String.join(", ", artists));
            SecurityUtils.getCurrentUserLogin()
                .flatMap(userRepository::findOneWithFollowsByLogin)
                .ifPresent(post::setAuthor);
            switch (artists.size()) {
                case 1:
                    post.setTemplate(ONE_ARTIST_TEMPLATE);
                    break;
                case 2:
                    post.setTemplate(TWO_ARTISTS_TEMPLATE);
                    break;
                default:
                    post.setTitle(String.join(", ", artists.subList(0, 2)));
                    post.setTemplate(MORE_ARTISTS_TEMPLATE);
                    break;
            }
            postRepository.save(post);
        }
    }

    private EditionDTO createValidation(Long id, Edition edition) {
        edition.setId(null);
        edition.setVisible(Boolean.FALSE);
        edition.setOriginalEdition(id);
        edition = editionRepository.save(edition);
        return editionMapper.toDto(edition);
    }

    private void setEditionArtists(Edition edition, List<ArtistDTO> artistsDTO) {
        List<Artist> artists = artistMapper.toEntity(artistsDTO);
        edition.setArtists(Sets.newHashSet());
        for (Artist artist: artists) {
            edition.addArtist(artist.getId() != null ? artist : artistRepository.findByNameIgnoreCase(artist.getName()).orElseGet(() -> artistRepository.save(artist)));
        }
    }

    /**
     * Update artists of edition.
     *
     * @param id the id of the editiono.
     * @param artistsDTO the lists of artists.
     * @return the persisted entity.
     */
    public EditionDTO saveArtists(Long id, List<ArtistDTO> artistsDTO) {
        log.debug("Request to update artists of edition {}: {}", id, artistsDTO);
        Edition edition = editionRepository.findOneWithEagerRelationships(id)
            .orElseThrow(() -> new RuntimeException(EDITION_NOT_FOUND));
        entityManager.detach(edition);

        setEditionArtists(edition, artistsDTO);
        return createValidation(id, edition);
    }

    /**
     * Update poster of edition.
     *
     * @param id the id of the editiono.
     * @param posterDTO the new poster.
     * @return the persisted entity.
     */
    public EditionDTO updatePoster(Long id, FileDTO posterDTO) {
        log.debug("Request to update poster of edition {}: {}", id, posterDTO);
        Edition edition = editionRepository.findOneWithEagerRelationships(id)
            .orElseThrow(() -> new RuntimeException(EDITION_NOT_FOUND));
        entityManager.detach(edition);
        edition.getArtists().forEach(artist -> entityManager.detach(artist));
        edition.setPoster(posterDTO.getFile());
        edition.setPosterContentType(posterDTO.getFileContentType());

        return createValidation(id, edition);
    }

    /**
     * Send report for edition.
     *
     * @param id the id of the editiono.
     * @param report the report.
     * @return the persisted entity.
     */
    public EditionDTO sendReport(Long id, String report) {
        log.debug("Request to send report for edition {}: {}", id, report);
        Edition edition = editionRepository.findOneWithEagerRelationships(id)
            .orElseThrow(() -> new RuntimeException(EDITION_NOT_FOUND));
        entityManager.detach(edition);
        edition.getArtists().forEach(artist -> entityManager.detach(artist));
        edition.setReport(report);

        return createValidation(id, edition);
    }

    /**
     * Save a edition-festival.
     *
     * @param editionDTO the entity to save.
     * @return the persisted entity.
     */
    public EditionDTO save(EditionFestivalDTO editionDTO) {
        log.debug("Request to save EditionFestival : {}", editionDTO);
        Edition edition = editionFestivalMapper.toEntity(editionDTO);
        Festival festival = festivalRepository
            .findByNameIgnoreCase(editionDTO.getName())
            .orElseGet(() -> festivalRepository.save(new Festival().name(editionDTO.getName())));
        edition.setFestival(festival);
        edition.setVisible(Boolean.FALSE);
        setEditionArtists(edition, Lists.newArrayList(editionDTO.getArtists()));
        edition = editionRepository.save(edition);
        return editionMapper.toDto(edition);
    }

    public ZonedDateTime getAndUpdateLastCrawlDate() {
        Optional<Config> config = configRepository.findByName("LAST_CRAWL_DATE");
        ZonedDateTime result = null;
        String now = ZonedDateTime.now().toString();
        if (config.isPresent()) {
            result = ZonedDateTime.parse(config.get().getValue());
            configRepository.save(config.get().value(now));
        } else {
            result = ZonedDateTime.of(2018, 6, 1, 1, 1, 1, 1, ZoneId.systemDefault());
            configRepository.save(new Config().name("LAST_CRAWL_DATE").value(now));
        }
        return result;
    }

    private void buildFestivalCrawled(Festival festival, FestivalDTO festivalDTO) {
        if (festival.getLogo() == null && festivalDTO.getLogo() != null) {
            festival.logo(festivalDTO.getLogo()).logoContentType(festivalDTO.getLogoContentType());
        }
        if (festival.getWeb() == null) {
            festival.web(festivalDTO.getWeb());
        }
        if (festival.getFacebook() == null) {
            festival.facebook(festivalDTO.getFacebook());
        }
        if (festival.getTwitter() == null) {
            festival.twitter(festivalDTO.getTwitter());
        }
    }

    public void saveFromCrawler(FestivalDTO festivalDTO, EditionDTO editionDTO) {
        log.debug("Request to save festival/edition data obtained from crawler: {} - {}", festivalDTO, editionDTO);
        Optional<Festival> festivalOpt = festivalRepository
            .findByNameIgnoreCase(festivalDTO.getName());

        Festival festival = null;
        if (festivalOpt.isPresent()) {
            festival = festivalOpt.get();
            buildFestivalCrawled(festival, festivalDTO);
        } else {
            festival = festivalMapper.toEntity(festivalDTO);
        }
        festival = festivalRepository.save(festival);

        Optional<Edition> editionCrawledOpt = editionRepository
            .findByFestivalAndStartDateBetweenAndCreatedByAndVisibleIsFalse(festival,
                editionDTO.getStartDate().minusDays(1), editionDTO.getEndDate(), "system");

        if (editionCrawledOpt.isPresent()) {
            Edition edition = editionCrawledOpt.get();
            if (edition.getPoster() == null && editionDTO.getPoster() != null) {
                edition.posterContentType(editionDTO.getPosterContentType()).poster(editionDTO.getPoster());
            }
            editionDTO.getArtists().addAll(artistMapper.toDto(Lists.newArrayList(edition.getArtists())));
            setEditionArtists(edition, Lists.newArrayList(editionDTO.getArtists()));
            editionRepository.save(edition);
        } else {
            Optional<Edition> editionOpt = editionRepository
                .findByFestivalNameIgnoreCaseAndStartDateBetweenAndVisibleIsTrue(festival.getName(),
                    editionDTO.getStartDate().minusDays(1), editionDTO.getEndDate());
            if (editionOpt.isPresent()) {
                Edition edition = editionOpt.get();
                entityManager.detach(edition);
                editionDTO.getArtists().addAll(artistMapper.toDto(Lists.newArrayList(edition.getArtists())));
                setEditionArtists(edition, Lists.newArrayList(editionDTO.getArtists()));
                edition.poster(editionDTO.getPoster()).posterContentType(editionDTO.getPosterContentType());
                createValidation(edition.getId(), edition);
            } else {
                Edition edition = editionMapper.toEntity(editionDTO);
                edition.setFestival(festival);
                edition.setVisible(Boolean.FALSE);
                setEditionArtists(edition, Lists.newArrayList(editionDTO.getArtists()));
                editionRepository.save(edition);
            }
        }
    }

    /**
     * Get the "items" next followed editions.
     *
     * @param items the number of editions to retrieve.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<EditionDTO> findFollowed(int items) {
        log.debug("Request to get the next {} followed editions", items);
        List<EditionDTO> followedEditions = new ArrayList<>();
        SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithFollowsByLogin).ifPresent(user ->
            followedEditions.addAll(editionMapper.toDto(editionRepository.findNextEditionsByFestivalIn(items, user.getFollows()))));
        return followedEditions;
    }

    /**
     * Get all the editions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<EditionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Editions");
        return editionRepository.findAll(pageable)
            .map(editionMapper::toDto);
    }

    /**
     * Get all the editions with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<EditionDTO> findAllWithEagerRelationships(Pageable pageable) {
        return editionRepository.findAllWithEagerRelationships(pageable).map(editionMapper::toDto);
    }

    /**
     * Get one edition by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EditionDTO> findOne(Long id) {
        log.debug("Request to get Edition : {}", id);
        return editionRepository.findOneWithEagerRelationships(id)
            .map(editionMapper::toDto);
    }

    /**
     * Get one edition by festival and year.
     *
     * @param festival the name of the festival.
     * @param edition the year of the festival.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EditionDTO> findFestivalEdition(String festival, int edition) {
        log.debug("Request to get festival edition : {} {}", festival, edition);
        LocalDate from = LocalDate.of(edition, 1, 1);
        LocalDate to = LocalDate.of(edition, 12, 31);
        Optional<EditionDTO> editionDTO = editionRepository
            .findByFestivalNameIgnoreCaseAndStartDateBetweenAndVisibleIsTrue(festival, from, to)
            .map(editionMapper::toDto);
        editionDTO.flatMap(ed -> SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithFollowsByLogin))
            .ifPresent(user -> editionDTO.get().setFollowed(user.getFollows().stream()
                .anyMatch(f -> f.getId().equals(editionDTO.get().getFestivalId()))));
        return editionDTO;
    }

    /**
     * Delete the edition by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Edition : {}", id);
        editionRepository.deleteById(id);
    }
}
