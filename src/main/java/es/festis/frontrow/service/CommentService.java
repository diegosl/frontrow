package es.festis.frontrow.service;

import es.festis.frontrow.domain.Comment;
import es.festis.frontrow.repository.CommentRepository;
import es.festis.frontrow.service.dto.CommentDTO;
import es.festis.frontrow.service.mapper.CommentMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Comment}.
 */
@Service
@Transactional
public class CommentService {

    private final Logger log = LoggerFactory.getLogger(CommentService.class);

    private final CommentRepository commentRepository;

    private final CommentMapper commentMapper;

    private final ContributionService contributionService;

    public CommentService(CommentRepository commentRepository, CommentMapper commentMapper,
        ContributionService contributionService) {
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
        this.contributionService = contributionService;
    }

    public Page<CommentDTO> findByFestival(Long festivalId, Pageable pageable) {
        log.debug("Request to get festival {} comments", festivalId);
        return commentRepository.findByFestivalId(festivalId, pageable)
            .map(commentMapper::toDto);
    }

    public Page<CommentDTO> findByPost(Long postId, Pageable pageable) {
        log.debug("Request to get post {} comments", postId);
        return commentRepository.findByPostId(postId, pageable)
            .map(commentMapper::toDto);
    }

    /**
     * Save a comment.
     *
     * @param commentDTO the entity to save.
     * @return the persisted entity.
     */
    public CommentDTO save(CommentDTO commentDTO) {
        log.debug("Request to save Comment : {}", commentDTO);
        Comment comment = commentMapper.toEntity(commentDTO);
        if (commentDTO.getId() == null) {
            contributionService.createContributionByComment(commentDTO);
        }
        comment = commentRepository.save(comment);
        return commentMapper.toDto(comment);
    }

    /**
     * Get all the comments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CommentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Comments");
        return commentRepository.findAll(pageable)
            .map(commentMapper::toDto);
    }

    /**
     * Get one comment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CommentDTO> findOne(Long id) {
        log.debug("Request to get Comment : {}", id);
        return commentRepository.findById(id)
            .map(commentMapper::toDto);
    }

    /**
     * Delete the comment by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Comment : {}", id);
        commentRepository.deleteById(id);
    }
}
