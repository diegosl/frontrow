package es.festis.frontrow.service.mapper;


import es.festis.frontrow.domain.*;
import es.festis.frontrow.service.dto.ArtistDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Artist} and its DTO {@link ArtistDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ArtistMapper extends EntityMapper<ArtistDTO, Artist> {


    @Mapping(target = "editions", ignore = true)
    @Mapping(target = "removeEdition", ignore = true)
    @Mapping(target = "name", expression = "java( artistDTO.getName().trim() )")
    Artist toEntity(ArtistDTO artistDTO);

    default Artist fromId(Long id) {
        if (id == null) {
            return null;
        }
        Artist artist = new Artist();
        artist.setId(id);
        return artist;
    }
}
