package es.festis.frontrow.service.mapper;


import es.festis.frontrow.domain.*;
import es.festis.frontrow.service.dto.ContributionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Contribution} and its DTO {@link ContributionDTO}.
 */
@Mapper(componentModel = "spring", uses = {FestivalMapper.class, UserMapper.class})
public interface ContributionMapper extends EntityMapper<ContributionDTO, Contribution> {

    @Mapping(source = "festival.id", target = "festivalId")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "festival.name", target = "festivalName")
    ContributionDTO toDto(Contribution contribution);

    @Mapping(source = "festivalId", target = "festival")
    @Mapping(source = "userId", target = "user")
    Contribution toEntity(ContributionDTO contributionDTO);

    default Contribution fromId(Long id) {
        if (id == null) {
            return null;
        }
        Contribution contribution = new Contribution();
        contribution.setId(id);
        return contribution;
    }
}
