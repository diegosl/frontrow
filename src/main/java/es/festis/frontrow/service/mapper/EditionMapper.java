package es.festis.frontrow.service.mapper;


import es.festis.frontrow.domain.*;
import es.festis.frontrow.service.dto.EditionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Edition} and its DTO {@link EditionDTO}.
 */
@Mapper(componentModel = "spring", uses = {ArtistMapper.class, FestivalMapper.class})
public interface EditionMapper extends EntityMapper<EditionDTO, Edition> {

    @Mapping(source = "festival.id", target = "festivalId")
    @Mapping(source = "festival.name", target = "festivalName")
    @Mapping(source = "festival.logo", target = "logo")
    @Mapping(source = "festival.logoContentType", target = "logoContentType")
    @Mapping(source = "festival.web", target = "web")
    @Mapping(source = "festival.facebook", target = "facebook")
    @Mapping(source = "festival.twitter", target = "twitter")
    @Mapping(source = "festival.instagram", target = "instagram")
    EditionDTO toDto(Edition edition);

    @Mapping(target = "posts", ignore = true)
    @Mapping(target = "artists", ignore = true)
    @Mapping(target = "removePost", ignore = true)
    @Mapping(target = "removeArtist", ignore = true)
    @Mapping(source = "festivalId", target = "festival")
    Edition toEntity(EditionDTO editionDTO);

    default Edition fromId(Long id) {
        if (id == null) {
            return null;
        }
        Edition edition = new Edition();
        edition.setId(id);
        return edition;
    }
}
