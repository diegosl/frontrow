package es.festis.frontrow.service.mapper;

import es.festis.frontrow.domain.*;
import es.festis.frontrow.service.dto.EditionDTO;
import es.festis.frontrow.service.dto.EditionFestivalDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Edition} and its DTO {@link EditionDTO}.
 */
@Mapper(componentModel = "spring")
public interface EditionFestivalMapper extends EntityMapper<EditionFestivalDTO, Edition> {

    @Mapping(source = "festival.name", target = "name")
    EditionFestivalDTO toDto(Edition edition);

    @Mapping(target = "artists", ignore = true)
    Edition toEntity(EditionFestivalDTO editionDTO);
}
