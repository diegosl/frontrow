package es.festis.frontrow.service.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.festis.frontrow.domain.*;
import es.festis.frontrow.domain.enumeration.Badge;
import es.festis.frontrow.domain.enumeration.BadgeRank;
import es.festis.frontrow.service.dto.CommentDTO;

import org.mapstruct.*;

import java.util.EnumMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Mapper for the entity {@link Comment} and its DTO {@link CommentDTO}.
 */
@Mapper(componentModel = "spring", uses = {PostMapper.class, FestivalMapper.class, UserMapper.class})
public interface CommentMapper extends EntityMapper<CommentDTO, Comment> {

    @Mapping(source = "post.id", target = "postId")
    @Mapping(source = "festival.id", target = "festivalId")
    @Mapping(source = "author.id", target = "authorId")
    @Mapping(source = "author.login", target = "authorLogin")
    @Mapping(source = "author.imageUrl", target = "authorImageUrl")
    @Mapping(source = "author.badges", target = "authorBadges")
    CommentDTO toDto(Comment comment);

    @Mapping(source = "postId", target = "post")
    @Mapping(source = "festivalId", target = "festival")
    @Mapping(source = "authorId", target = "author")
    Comment toEntity(CommentDTO commentDTO);

    default EnumMap<Badge, BadgeRank> mapBadgesToEnumMap(String badges) {
        ObjectMapper mapper = new ObjectMapper();
        EnumMap<Badge, BadgeRank> userBadges = new EnumMap<>(Badge.class);
        if (badges != null) {
            try {
                userBadges = mapper.readValue(badges, new TypeReference<EnumMap<Badge, BadgeRank>>(){});
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return userBadges;
    }

    default Map<String, String> map(String badges) {
        EnumMap<Badge, BadgeRank> userBadges = mapBadgesToEnumMap(badges);
        return userBadges.entrySet().stream()
            .collect(Collectors.toMap(
                e -> e.getKey().getIcon(),
                e-> e.getValue().getColor()
            ));
    }

    default Comment fromId(Long id) {
        if (id == null) {
            return null;
        }
        Comment comment = new Comment();
        comment.setId(id);
        return comment;
    }
}
