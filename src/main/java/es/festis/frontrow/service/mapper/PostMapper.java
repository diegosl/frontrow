package es.festis.frontrow.service.mapper;

import es.festis.frontrow.domain.*;
import es.festis.frontrow.service.dto.PostDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Post} and its DTO {@link PostDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, EditionMapper.class})
public interface PostMapper extends EntityMapper<PostDTO, Post> {

    @Mapping(source = "author.id", target = "authorId")
    @Mapping(source = "author.login", target = "authorLogin")
    @Mapping(source = "editor.id", target = "editorId")
    @Mapping(source = "editor.login", target = "editorLogin")
    @Mapping(source = "edition.id", target = "editionId")
    @Mapping(source = "edition.festival.logoContentType", target = "logoContentType")
    @Mapping(source = "edition.festival.logo", target = "logo")
    @Mapping(source = "edition.festival.name", target = "editionName")
    @Mapping(source = "edition.startDate.year", target = "editionYear")
    PostDTO toDto(Post post);

    @Mapping(source = "authorId", target = "author")
    @Mapping(source = "editorId", target = "editor")
    @Mapping(source = "editionId", target = "edition")
    Post toEntity(PostDTO postDTO);

    default Post fromId(Long id) {
        if (id == null) {
            return null;
        }
        Post post = new Post();
        post.setId(id);
        return post;
    }
}
