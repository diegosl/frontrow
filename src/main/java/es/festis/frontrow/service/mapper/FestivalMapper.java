package es.festis.frontrow.service.mapper;


import es.festis.frontrow.domain.*;
import es.festis.frontrow.service.dto.FestivalDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Festival} and its DTO {@link FestivalDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FestivalMapper extends EntityMapper<FestivalDTO, Festival> {


    @Mapping(target = "editions", ignore = true)
    @Mapping(target = "removeEdition", ignore = true)
    Festival toEntity(FestivalDTO festivalDTO);

    default Festival fromId(Long id) {
        if (id == null) {
            return null;
        }
        Festival festival = new Festival();
        festival.setId(id);
        return festival;
    }
}
