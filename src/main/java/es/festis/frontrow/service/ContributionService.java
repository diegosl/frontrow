package es.festis.frontrow.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import es.festis.frontrow.domain.Contribution;
import es.festis.frontrow.domain.User;
import es.festis.frontrow.domain.enumeration.Badge;
import es.festis.frontrow.domain.enumeration.BadgeRank;
import es.festis.frontrow.domain.enumeration.ContributionType;
import es.festis.frontrow.repository.ContributionRepository;
import es.festis.frontrow.repository.UserRepository;
import es.festis.frontrow.security.SecurityUtils;
import es.festis.frontrow.service.dto.CommentDTO;
import es.festis.frontrow.service.dto.ContributionDTO;
import es.festis.frontrow.service.dto.EditionDTO;
import es.festis.frontrow.service.mapper.CommentMapper;
import es.festis.frontrow.service.mapper.ContributionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Contribution}.
 */
@Service
@Transactional
public class ContributionService {

    private final Logger log = LoggerFactory.getLogger(ContributionService.class);

    private final ContributionRepository contributionRepository;

    private final ContributionMapper contributionMapper;

    private final UserRepository userRepository;

    private final CommentMapper commentMapper;

    private final ObjectMapper mapper = new ObjectMapper();

    public ContributionService(ContributionRepository contributionRepository, ContributionMapper contributionMapper,
        UserRepository userRepository, CommentMapper commentMapper) {
        this.contributionRepository = contributionRepository;
        this.contributionMapper = contributionMapper;
        this.userRepository = userRepository;
        this.commentMapper = commentMapper;
    }

    /**
     * Save a contribution.
     *
     * @param contributionDTO the entity to save.
     * @return the persisted entity.
     */
    public ContributionDTO save(ContributionDTO contributionDTO) {
        log.debug("Request to save Contribution : {}", contributionDTO);
        Contribution contribution = contributionMapper.toEntity(contributionDTO);
        contribution = contributionRepository.save(contribution);
        return contributionMapper.toDto(contribution);
    }

    /**
     * Get all the contributions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContributionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Contributions");
        return contributionRepository.findAll(pageable)
            .map(contributionMapper::toDto);
    }

    /**
     * Get all my contributions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContributionDTO> findMine(Pageable pageable) {
        log.debug("Request to get my Contributions");
        return contributionRepository.findByUserIsCurrentUser(pageable)
            .map(contributionMapper::toDto);
    }

    /**
     * Get my total of points.
     *
     * @return the total of points.
     */
    @Transactional(readOnly = true)
    public Long getMyTotalOfPoints() {
        log.debug("Request to get my total of points");
        return contributionRepository.sumPointsByUserIsCurrentUser();
    }

    /**
     * Get my badges.
     *
     * @return the badges.
     */
    @Transactional(readOnly = true)
    public Map<String, String> getMyBadges() {
        log.debug("Request to get my badges");
        return SecurityUtils.getCurrentUserLogin()
            .map(login -> userRepository.findOneByLogin(login)
                .map(user -> commentMapper.map(user.getBadges())))
            .orElseGet(() -> Optional.of(new HashMap<>()))
            .orElseGet(HashMap::new);
    }

    /**
     * Get one contribution by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContributionDTO> findOne(Long id) {
        log.debug("Request to get Contribution : {}", id);
        return contributionRepository.findById(id)
            .map(contributionMapper::toDto);
    }

    /**
     * Delete the contribution by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Contribution : {}", id);

        contributionRepository.deleteById(id);
    }

    @Transactional
    public void createContributionByComment(CommentDTO commentDTO) {
        userRepository.findById(commentDTO.getAuthorId()).ifPresent(user -> {
            ContributionDTO contribution = new ContributionDTO();
            contribution.setDate(LocalDate.now());
            contribution.setFestivalId(commentDTO.getFestivalId());
            contribution.setUserId(user.getId());
            contribution.setType(ContributionType.COMMENT);
            contribution.setPoints(5);
            contributionRepository.save(contributionMapper.toEntity(contribution));
            updateUserBadges(user, commentDTO.getFestivalId(), Boolean.FALSE);
        });
    }

    @Transactional
    public void createContributionByValidatedEdition(String authorLogin, EditionDTO editionDTO) {
        userRepository.findOneByLogin(authorLogin).ifPresent(user -> {
            ContributionDTO contribution = new ContributionDTO();
            contribution.setDate(LocalDate.now());
            contribution.setFestivalId(editionDTO.getFestivalId());
            contribution.setUserId(user.getId());
            if (Strings.isNullOrEmpty(editionDTO.getReport())) {
                contribution.setType(ContributionType.COLLABORATION);
                contribution.setPoints(100);
            } else {
                contribution.setType(ContributionType.REPORT);
                contribution.setPoints(25);
            }
            contributionRepository.save(contributionMapper.toEntity(contribution));
            updateUserBadges(user, editionDTO.getFestivalId(), Boolean.TRUE);
        });
    }

    private void updateUserBadges(User user, Long festivalId, boolean collaboration) {
        EnumMap<Badge, BadgeRank> userBadges = commentMapper.mapBadgesToEnumMap(user.getBadges());
        if (festivalId != null) {
            updateLoyalBadge(user.getId(), festivalId, userBadges);
            updateGlobetrotterBadge(user.getId(), userBadges);
        }
        updateHeadlinerBadge(user.getId(), userBadges);
        if (collaboration) {
            updateCollaboratorBadge(user.getId(), userBadges);
        } else {
            updateCommentatorBadge(user.getId(), userBadges);
        }
        try {
            user.setBadges(mapper.writeValueAsString(userBadges));
            userRepository.save(user);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private void updateLoyalBadge(Long userId, Long festivalId, EnumMap<Badge, BadgeRank> userBadges) {
        if (userBadges.getOrDefault(Badge.LOYAL, BadgeRank.BRONZE) != BadgeRank.GOLD) {
            long points = contributionRepository.sumPointsByUserAndFestival(userId, festivalId);
            if (points >= 1250) {
                userBadges.put(Badge.LOYAL, BadgeRank.GOLD);
            } else if (points >= 100) {
                userBadges.put(Badge.LOYAL, points >= 500 ? BadgeRank.SILVER : BadgeRank.BRONZE);
            }
        }
    }

    private void updateGlobetrotterBadge(Long userId, EnumMap<Badge, BadgeRank> userBadges) {
        if (userBadges.getOrDefault(Badge.GLOBETROTTER, BadgeRank.BRONZE) != BadgeRank.GOLD) {
            long festivals = contributionRepository.countDistinctFestivalsByUser(userId);
            if (festivals >= 25) {
                userBadges.put(Badge.GLOBETROTTER, BadgeRank.GOLD);
            } else if (festivals >= 2) {
                userBadges.put(Badge.GLOBETROTTER, festivals >= 10 ? BadgeRank.SILVER : BadgeRank.BRONZE);
            }
        }
    }

    private void updateHeadlinerBadge(Long userId, EnumMap<Badge, BadgeRank> userBadges) {
        if (userBadges.getOrDefault(Badge.HEADLINER, BadgeRank.BRONZE) != BadgeRank.GOLD) {
            long points = contributionRepository.sumPointsByUser(userId);
            if (points >= 10000) {
                userBadges.put(Badge.HEADLINER, BadgeRank.GOLD);
            } else if (points >= 500) {
                userBadges.put(Badge.HEADLINER, points >= 3000 ? BadgeRank.SILVER : BadgeRank.BRONZE);
            }
        }
    }

    private void updateCollaboratorBadge(Long userId, EnumMap<Badge, BadgeRank> userBadges) {
        if (userBadges.getOrDefault(Badge.COLLABORATOR, BadgeRank.BRONZE) != BadgeRank.GOLD) {
            long collaborations = contributionRepository.countByUserIdAndType(userId, ContributionType.COLLABORATION);
            if (collaborations >= 25) {
                userBadges.put(Badge.COLLABORATOR, BadgeRank.GOLD);
            } else if (collaborations >= 2) {
                userBadges.put(Badge.COLLABORATOR, collaborations >= 10 ? BadgeRank.SILVER : BadgeRank.BRONZE);
            }
        }
    }

    private void updateCommentatorBadge(Long userId, EnumMap<Badge, BadgeRank> userBadges) {
        if (userBadges.getOrDefault(Badge.COMMENTATOR, BadgeRank.BRONZE) != BadgeRank.GOLD) {
            long comments = contributionRepository.countByUserIdAndType(userId, ContributionType.COMMENT);
            if (comments >= 1000) {
                userBadges.put(Badge.COMMENTATOR, BadgeRank.GOLD);
            } else if (comments >= 20) {
                userBadges.put(Badge.COMMENTATOR, comments >= 250 ? BadgeRank.SILVER : BadgeRank.BRONZE);
            }
        }
    }
}
