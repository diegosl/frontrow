package es.festis.frontrow.service;

import es.festis.frontrow.domain.Festival;
import es.festis.frontrow.domain.User;
import es.festis.frontrow.repository.FestivalRepository;
import es.festis.frontrow.repository.UserRepository;
import es.festis.frontrow.security.SecurityUtils;
import es.festis.frontrow.service.dto.FestivalDTO;
import es.festis.frontrow.service.mapper.FestivalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Festival}.
 */
@Service
@Transactional
public class FestivalService {

    private final Logger log = LoggerFactory.getLogger(FestivalService.class);

    private final FestivalRepository festivalRepository;

    private final FestivalMapper festivalMapper;

    private final UserRepository userRepository;

    public FestivalService(FestivalRepository festivalRepository, FestivalMapper festivalMapper,
                           UserRepository userRepository) {
        this.festivalRepository = festivalRepository;
        this.festivalMapper = festivalMapper;
        this.userRepository = userRepository;
    }

    /**
     * Save a festival.
     *
     * @param festivalDTO the entity to save.
     * @return the persisted entity.
     */
    public FestivalDTO save(FestivalDTO festivalDTO) {
        log.debug("Request to save Festival : {}", festivalDTO);
        Festival festival = festivalMapper.toEntity(festivalDTO);
        festival = festivalRepository.save(festival);
        return festivalMapper.toDto(festival);
    }

    /**
     * Get all the festivals.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FestivalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Festivals");
        return festivalRepository.findAll(pageable)
            .map(festivalMapper::toDto);
    }

    /**
     * Get all the festival names.
     *
     * @return the list of names.
     */
    @Transactional(readOnly = true)
    public List<String> findAllNames() {
        log.debug("Request to get all festival names");
        return festivalRepository.findNames();
    }

    /**
     * Get one festival by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FestivalDTO> findOne(Long id) {
        log.debug("Request to get Festival : {}", id);
        return festivalRepository.findById(id)
            .map(festivalMapper::toDto);
    }

    /**
     * Follow a festival by name.
     *
     * @param name the name of the festival.
     */
    public void follow(String name) {
        log.debug("Request to follow Festival : {}", name);
        festivalRepository.findByNameIgnoreCase(name).ifPresent(festival ->
            SecurityUtils.getCurrentUserLogin()
                .flatMap(userRepository::findOneWithFollowsByLogin)
                .ifPresent(user -> {
                    user.addFollow(festival);
                    userRepository.save(user);
                    log.debug("Followed festival: {}", festival.getName());
                })
        );
    }

    /**
     * Unfollow a festival by name.
     *
     * @param name the name of the festival.
     */
    public void unfollow(String name) {
        log.debug("Request to unfollow Festival : {}", name);
        festivalRepository.findByNameIgnoreCase(name).ifPresent(festival ->
            SecurityUtils.getCurrentUserLogin()
                .flatMap(userRepository::findOneWithFollowsByLogin)
                .ifPresent(user -> {
                    user.removeFollow(festival);
                    userRepository.save(user);
                    log.debug("Unfollowed festival: {}", festival.getName());
                })
        );
    }

    /**
     * Get the followed festivals.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<FestivalDTO> findFollowed() {
        log.debug("Request to get followed Festivals");
        Optional<User> currentUser = SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneWithFollowsByLogin);
        return currentUser
            .map(user -> user.getFollows().stream().map(festivalMapper::toDto).collect(Collectors.toList()))
            .orElse(Collections.emptyList());
    }

    /**
     * Delete the festival by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Festival : {}", id);
        festivalRepository.deleteById(id);
    }
}
