package es.festis.frontrow.repository;

public class CacheNames {
    public static final String USERS_BY_LOGIN_CACHE = "usersByLogin";
    public static final String USERS_BY_EMAIL_CACHE = "usersByEmail";

    private CacheNames() {
        throw new IllegalStateException("Utility class");
    }
}
