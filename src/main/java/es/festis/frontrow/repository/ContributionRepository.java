package es.festis.frontrow.repository;

import es.festis.frontrow.domain.Contribution;

import es.festis.frontrow.domain.enumeration.ContributionType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Contribution entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContributionRepository extends JpaRepository<Contribution, Long> {
    @Query(value = "select contribution from Contribution contribution where contribution.user.login = ?#{principal.username}",
    countQuery = "select count(distinct contribution) from Contribution contribution where contribution.user.login = ?#{principal.username}")
    Page<Contribution> findByUserIsCurrentUser(Pageable pageable);

    @Query("select sum(points) from Contribution where user.id = :userId and festival.id = :festivalId")
    Long sumPointsByUserAndFestival(@Param("userId") Long userId, @Param("festivalId") Long festivalId);

    @Query("select sum(points) from Contribution where user.id = :userId")
    Long sumPointsByUser(@Param("userId") Long userId);

    @Query("select sum(points) from Contribution where user.login = ?#{principal.username}")
    Long sumPointsByUserIsCurrentUser();

    @Query("select count(distinct contribution.festival) from Contribution contribution where user.id = :userId")
    Long countDistinctFestivalsByUser(@Param("userId") Long userId);

    Long countByUserIdAndType(Long userId, ContributionType contributionType);
}
