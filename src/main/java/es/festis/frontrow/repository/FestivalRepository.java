package es.festis.frontrow.repository;

import es.festis.frontrow.domain.Festival;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Festival entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FestivalRepository extends JpaRepository<Festival, Long> {

    @Query("SELECT name FROM Festival") 
    List<String> findNames();

    Optional<Festival> findByNameIgnoreCase(String name);
}
