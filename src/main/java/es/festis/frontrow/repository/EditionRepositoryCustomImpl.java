package es.festis.frontrow.repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;
import javax.persistence.criteria.Subquery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import es.festis.frontrow.domain.Artist;
import es.festis.frontrow.domain.Artist_;
import es.festis.frontrow.domain.Edition;
import es.festis.frontrow.domain.Edition_;

public class EditionRepositoryCustomImpl implements EditionRepositoryCustom {

    public static final String RADIANS = "radians";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Edition> findBySearchCriteriaCustom(Float latitude, Float longitude, Long radio, LocalDate fromDate, LocalDate toDate,
        Set<Long> artistIds, Pageable pageable) {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Edition> query = cb.createQuery(Edition.class);
        Root<Edition> root = query.from(Edition.class);

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(cb.isTrue(root.get(Edition_.visible)));

        if (latitude != null && longitude != null && radio != null) {
            Subquery<Double> geoSubquery = query.subquery(Double.class);
            Root<Edition> geoRoot = geoSubquery.from(Edition.class);
            geoSubquery.select(cb.prod(6371D,
                cb.function("acos", Double.class,
                    cb.sum(
                        cb.prod(
                            cb.prod(
                                Math.cos(Math.toRadians(latitude)),
                                cb.function("cos", Double.class, cb.function(RADIANS, Double.class, geoRoot.get(Edition_.latitude)))),
                            cb.function("cos", Double.class,
                                cb.diff(
                                   cb.function(RADIANS, Double.class, geoRoot.get(Edition_.longitude)),
                                   Math.toRadians(longitude)
                                ))),
                        cb.prod(
                            Math.sin(Math.toRadians(latitude)),
                            cb.function("sin", Double.class, cb.function(RADIANS, Double.class, geoRoot.get(Edition_.latitude))))))))
                .where(cb.equal(root.get(Edition_.id), geoRoot.get(Edition_.id)));
            predicates.add(cb.lessThanOrEqualTo(geoSubquery, radio.doubleValue()));
        }

        if (fromDate != null) {
            predicates.add(cb.greaterThanOrEqualTo(root.get(Edition_.endDate), fromDate));
        }

        if (toDate != null) {
            predicates.add(cb.lessThanOrEqualTo(root.get(Edition_.startDate), toDate));
        }

        if (!artistIds.isEmpty()) {
            Subquery<Long> artistsSubquery = query.subquery(Long.class);
            Root<Edition> artistsRoot = artistsSubquery.from(Edition.class);
            SetJoin<Edition, Artist> artists = artistsRoot.join(Edition_.artists, JoinType.INNER);
            In<Long> in = cb.in(artists.get(Artist_.id));
            artistIds.forEach(in::value);
            artistsSubquery.select(artistsRoot.get(Edition_.id)).where(in).groupBy(artistsRoot.get(Edition_.id)).having(cb.equal(cb.count(artistsRoot.get(Edition_.id)), artistIds.size()));
            predicates.add(cb.in(root.get(Edition_.id)).value(artistsSubquery));
        }

        query.distinct(true).where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        query.orderBy(cb.asc(root.get(Edition_.startDate)));
        List<Edition> result = entityManager.createQuery(query)
            .setFirstResult((int) pageable.getOffset()).setMaxResults(pageable.getPageSize()).getResultList();

        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<Edition> editionRootCount = countQuery.from(Edition.class);
        countQuery.distinct(true).select(cb.count(editionRootCount)).where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        Long count = entityManager.createQuery(countQuery).getSingleResult();

        return new PageImpl<>(result, pageable, count);
    }
}
