package es.festis.frontrow.repository;

import es.festis.frontrow.domain.Post;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Post entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    @Query("select post from Post post where post.author.login = ?#{principal.username}")
    List<Post> findByAuthorIsCurrentUser();

    @Query("select post from Post post where post.editor.login = ?#{principal.username}")
    List<Post> findByEditorIsCurrentUser();

    @Query
    List<Post> findAllByOrderByDateDesc(Pageable pageable);

    @Query
    List<Post> findAllByEditionIdOrderByDateDesc(Long edition, Pageable pageable);

    @Query
    Optional<Post> findByHandle(String handle);

    default List<Post> findLatest(int items) {
        return findAllByOrderByDateDesc(PageRequest.of(0,items));
    }

    default List<Post> findLatestByEdition(int items, Long edition) {
        return findAllByEditionIdOrderByDateDesc(edition, PageRequest.of(0,items));
    }
}
