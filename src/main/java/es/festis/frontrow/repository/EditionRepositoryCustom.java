package es.festis.frontrow.repository;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import es.festis.frontrow.domain.Edition;

public interface EditionRepositoryCustom {
    Page<Edition> findBySearchCriteriaCustom(Float latitude, Float longitude, Long radio, LocalDate fromDate, LocalDate toDate,
        Set<Long> artistIds, Pageable pageable);
}
