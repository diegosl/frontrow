package es.festis.frontrow.repository;

import es.festis.frontrow.domain.Edition;

import es.festis.frontrow.domain.Festival;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Spring Data  repository for the Edition entity.
 */
@Repository
public interface EditionRepository extends JpaRepository<Edition, Long>, JpaSpecificationExecutor<Edition>, EditionRepositoryCustom {

    @Query(value = "select distinct edition from Edition edition left join fetch edition.artists",
        countQuery = "select count(distinct edition) from Edition edition")
    Page<Edition> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct edition from Edition edition left join fetch edition.artists")
    List<Edition> findAllWithEagerRelationships();

    @Query("select edition from Edition edition left join fetch edition.artists where edition.id =:id")
    Optional<Edition> findOneWithEagerRelationships(@Param("id") Long id);

    Optional<Edition> findByFestivalNameIgnoreCaseAndStartDateBetweenAndVisibleIsTrue(String festival, LocalDate from, LocalDate to);

    Optional<Edition> findByFestivalAndStartDateBetweenAndCreatedByAndVisibleIsFalse(Festival festival, LocalDate from, LocalDate to, String createdBy);

    List<Edition> findAllByFestivalInAndStartDateGreaterThanEqualOrderByStartDateAsc(Pageable pageable, Set<Festival> festivals, LocalDate startDate);

    default List<Edition> findNextEditionsByFestivalIn(int items, Set<Festival> festivals) {
        return findAllByFestivalInAndStartDateGreaterThanEqualOrderByStartDateAsc(PageRequest.of(0,items), festivals, LocalDate.now());
    }
}
