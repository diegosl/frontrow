package es.festis.frontrow.repository;

import es.festis.frontrow.domain.Artist;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Artist entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ArtistRepository extends JpaRepository<Artist, Long> {

    List<Artist> findByNameStartingWithOrderByName(String name);

    Optional<Artist> findByNameIgnoreCase(String name);
}
