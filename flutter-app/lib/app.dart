import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:frontrow/account/login/bloc/login_bloc.dart';
import 'package:frontrow/account/login/login_repository.dart';
import 'package:frontrow/account/register/bloc/register_bloc.dart';
import 'package:frontrow/account/settings/settings_screen.dart';
import 'package:frontrow/explore/bloc/explore_filter.dart';
import 'package:frontrow/explore/search_screen.dart';
import 'package:frontrow/main/bloc/main_bloc.dart';
import 'package:frontrow/myfestivals/bloc/myfestivals_bloc.dart';
import 'package:frontrow/myfestivals/myfestivals_screen.dart';
import 'package:frontrow/routes.dart';
import 'package:frontrow/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:frontrow/shared/repository/account_repository.dart';
import 'package:frontrow/shared/repository/edition_repository.dart';
import 'package:frontrow/shared/repository/post_repository.dart';
import 'package:frontrow/themes.dart';
import 'account/settings/bloc/settings_bloc.dart';

import 'account/login/login_screen.dart';
import 'account/register/register_screen.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'explore/bloc/explore_bloc.dart';
import 'explore/explore_screen.dart';
import 'festival/festival_screen.dart';
import 'generated/l10n.dart';

class FrontrowApp extends StatelessWidget {
  const FrontrowApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Frontrow',
        theme: Themes.jhLight,
        routes: {
          FrontrowRoutes.login: (context) {
            return BlocProvider<LoginBloc>(create: (context) => LoginBloc(loginRepository: LoginRepository()), child: LoginScreen());
          },
          FrontrowRoutes.register: (context) {
            return BlocProvider<RegisterBloc>(
                create: (context) => RegisterBloc(accountRepository: AccountRepository()), child: RegisterScreen());
          },
          FrontrowRoutes.main: (context) {
            return BlocProvider<MainBloc>(
                create: (context) => MainBloc(accountRepository: AccountRepository(), postRepository: PostRepository())..add(Init()),
                child: MainScreen());
          },
          FrontrowRoutes.settings: (context) {
            return BlocProvider<SettingsBloc>(
                create: (context) => SettingsBloc(accountRepository: AccountRepository())..add(LoadCurrentUser()), child: SettingsScreen());
          },
          FrontrowRoutes.explore: (context) {
            return BlocProvider<ExploreBloc>(
                create: (context) => ExploreBloc(editionRepository: EditionRepository())
                  ..add(ExploreRequested(filters: new ExploreFilter(DateTime.now(), null, null, null, null, null))),
                child: ExploreScreen());
          },
          FrontrowRoutes.festival: (context) => FestivalScreen(),
          FrontrowRoutes.search: (context) => SearchScreen(),
          FrontrowRoutes.myfestivals: (context) {
            return BlocProvider<MyFestivalsBloc>(
                create: (context) => MyFestivalsBloc(editionRepository: EditionRepository())..add(MyFestivalsInit()),
                child: MyFestivalsScreen());
          },
        },
        localizationsDelegates: [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: S.delegate.supportedLocales);
  }
}
