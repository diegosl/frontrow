// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `en`
  String get locale {
    return Intl.message(
      'en',
      name: 'locale',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get pageLoginBar {
    return Intl.message(
      'Login',
      name: 'pageLoginBar',
      desc: '',
      args: [],
    );
  }

  /// `Frontrow`
  String get pageLoginTitle {
    return Intl.message(
      'Frontrow',
      name: 'pageLoginTitle',
      desc: '',
      args: [],
    );
  }

  /// `Sign in`
  String get pageLoginLoginButton {
    return Intl.message(
      'Sign in',
      name: 'pageLoginLoginButton',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get pageLoginRegisterButton {
    return Intl.message(
      'Register',
      name: 'pageLoginRegisterButton',
      desc: '',
      args: [],
    );
  }

  /// `Problem authenticating, verify your credentials`
  String get pageLoginErrorAuthentication {
    return Intl.message(
      'Problem authenticating, verify your credentials',
      name: 'pageLoginErrorAuthentication',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get pageRegisterTitle {
    return Intl.message(
      'Register',
      name: 'pageRegisterTitle',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get pageRegisterFormLogin {
    return Intl.message(
      'Login',
      name: 'pageRegisterFormLogin',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get pageRegisterFormEmail {
    return Intl.message(
      'Email',
      name: 'pageRegisterFormEmail',
      desc: '',
      args: [],
    );
  }

  /// `you@example.com`
  String get pageRegisterFormEmailHint {
    return Intl.message(
      'you@example.com',
      name: 'pageRegisterFormEmailHint',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get pageRegisterFormPassword {
    return Intl.message(
      'Password',
      name: 'pageRegisterFormPassword',
      desc: '',
      args: [],
    );
  }

  /// `Confirm password`
  String get pageRegisterFormConfirmPassword {
    return Intl.message(
      'Confirm password',
      name: 'pageRegisterFormConfirmPassword',
      desc: '',
      args: [],
    );
  }

  /// `I accept the terms of use`
  String get pageRegisterFormTermsConditions {
    return Intl.message(
      'I accept the terms of use',
      name: 'pageRegisterFormTermsConditions',
      desc: '',
      args: [],
    );
  }

  /// `Please accept the terms and conditions`
  String get pageRegisterFormTermsConditionsNotChecked {
    return Intl.message(
      'Please accept the terms and conditions',
      name: 'pageRegisterFormTermsConditionsNotChecked',
      desc: '',
      args: [],
    );
  }

  /// `Sign up`
  String get pageRegisterFormSubmit {
    return Intl.message(
      'Sign up',
      name: 'pageRegisterFormSubmit',
      desc: '',
      args: [],
    );
  }

  /// `Email already exists`
  String get pageRegisterErrorMailExist {
    return Intl.message(
      'Email already exists',
      name: 'pageRegisterErrorMailExist',
      desc: '',
      args: [],
    );
  }

  /// `Login already taken`
  String get pageRegisterErrorLoginExist {
    return Intl.message(
      'Login already taken',
      name: 'pageRegisterErrorLoginExist',
      desc: '',
      args: [],
    );
  }

  /// `The passwords are not identical`
  String get pageRegisterErrorPasswordNotIdentical {
    return Intl.message(
      'The passwords are not identical',
      name: 'pageRegisterErrorPasswordNotIdentical',
      desc: '',
      args: [],
    );
  }

  /// `Register successful`
  String get pageRegisterSuccessAltImg {
    return Intl.message(
      'Register successful',
      name: 'pageRegisterSuccessAltImg',
      desc: '',
      args: [],
    );
  }

  /// `Congratulations`
  String get pageRegisterSuccess {
    return Intl.message(
      'Congratulations',
      name: 'pageRegisterSuccess',
      desc: '',
      args: [],
    );
  }

  /// `You have successfully registered`
  String get pageRegisterSuccessSub {
    return Intl.message(
      'You have successfully registered',
      name: 'pageRegisterSuccessSub',
      desc: '',
      args: [],
    );
  }

  /// `The login has to contain more than {min} characters`
  String pageRegisterLoginValidationError(Object min) {
    return Intl.message(
      'The login has to contain more than $min characters',
      name: 'pageRegisterLoginValidationError',
      desc: '',
      args: [min],
    );
  }

  /// `Please enter a valid email address`
  String get pageRegisterMailValidationError {
    return Intl.message(
      'Please enter a valid email address',
      name: 'pageRegisterMailValidationError',
      desc: '',
      args: [],
    );
  }

  /// `Rules: 1 uppercase, 1 number and {min} characters`
  String pageRegisterPasswordValidationError(Object min) {
    return Intl.message(
      'Rules: 1 uppercase, 1 number and $min characters',
      name: 'pageRegisterPasswordValidationError',
      desc: '',
      args: [min],
    );
  }

  /// `Rules: 1 uppercase, 1 number and {min} characters`
  String pageRegisterConfirmationPasswordValidationError(Object min) {
    return Intl.message(
      'Rules: 1 uppercase, 1 number and $min characters',
      name: 'pageRegisterConfirmationPasswordValidationError',
      desc: '',
      args: [min],
    );
  }

  /// `News`
  String get pageMainTitle {
    return Intl.message(
      'News',
      name: 'pageMainTitle',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get pageSearchSearchButton {
    return Intl.message(
      'Search',
      name: 'pageSearchSearchButton',
      desc: '',
      args: [],
    );
  }

  /// `Location`
  String get pageSearchLocation {
    return Intl.message(
      'Location',
      name: 'pageSearchLocation',
      desc: '',
      args: [],
    );
  }

  /// `Radius`
  String get pageSearchRadio {
    return Intl.message(
      'Radius',
      name: 'pageSearchRadio',
      desc: '',
      args: [],
    );
  }

  /// `You aren't following any festivals`
  String get pageMyFestivalsNotFollowing {
    return Intl.message(
      'You aren\'t following any festivals',
      name: 'pageMyFestivalsNotFollowing',
      desc: '',
      args: [],
    );
  }

  /// `Schedule`
  String get pageFestivalScheduleButton {
    return Intl.message(
      'Schedule',
      name: 'pageFestivalScheduleButton',
      desc: '',
      args: [],
    );
  }

  /// `Canceled`
  String get pageFestivalCancelled {
    return Intl.message(
      'Canceled',
      name: 'pageFestivalCancelled',
      desc: '',
      args: [],
    );
  }

  /// `Confirmed artists:`
  String get pageFestivalArtists {
    return Intl.message(
      'Confirmed artists:',
      name: 'pageFestivalArtists',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get drawerSettingsTitle {
    return Intl.message(
      'Settings',
      name: 'drawerSettingsTitle',
      desc: '',
      args: [],
    );
  }

  /// `Sign out`
  String get drawerLogoutTitle {
    return Intl.message(
      'Sign out',
      name: 'drawerLogoutTitle',
      desc: '',
      args: [],
    );
  }

  /// `Menu`
  String get drawerMenuTitle {
    return Intl.message(
      'Menu',
      name: 'drawerMenuTitle',
      desc: '',
      args: [],
    );
  }

  /// `News`
  String get drawerMenuMain {
    return Intl.message(
      'News',
      name: 'drawerMenuMain',
      desc: '',
      args: [],
    );
  }

  /// `My festivals`
  String get drawerMenuMyFestivals {
    return Intl.message(
      'My festivals',
      name: 'drawerMenuMyFestivals',
      desc: '',
      args: [],
    );
  }

  /// `Explore`
  String get drawerMenuExplore {
    return Intl.message(
      'Explore',
      name: 'drawerMenuExplore',
      desc: '',
      args: [],
    );
  }

  /// `Error communicating with the server`
  String get errorNetwork {
    return Intl.message(
      'Error communicating with the server',
      name: 'errorNetwork',
      desc: '',
      args: [],
    );
  }

  /// `Explore`
  String get pageExploreTitle {
    return Intl.message(
      'Explore',
      name: 'pageExploreTitle',
      desc: '',
      args: [],
    );
  }

  /// `We couldn't find any festival matching that criteria.`
  String get pageExploreNotFound {
    return Intl.message(
      'We couldn\'t find any festival matching that criteria.',
      name: 'pageExploreNotFound',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get pageSettingsTitle {
    return Intl.message(
      'Settings',
      name: 'pageSettingsTitle',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get pageSettingsFormEmail {
    return Intl.message(
      'Email',
      name: 'pageSettingsFormEmail',
      desc: '',
      args: [],
    );
  }

  /// `Languages`
  String get pageSettingsFormLanguages {
    return Intl.message(
      'Languages',
      name: 'pageSettingsFormLanguages',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get pageSettingsFormSave {
    return Intl.message(
      'Save',
      name: 'pageSettingsFormSave',
      desc: '',
      args: [],
    );
  }

  /// `Settings saved!`
  String get pageSettingsSave {
    return Intl.message(
      'Settings saved!',
      name: 'pageSettingsSave',
      desc: '',
      args: [],
    );
  }

  /// `Email format incorrect`
  String get pageSettingsEmailErrorValidation {
    return Intl.message(
      'Email format incorrect',
      name: 'pageSettingsEmailErrorValidation',
      desc: '',
      args: [],
    );
  }

  /// `{artist} joins {festival}`
  String postTemplateOneArtist(Object artist, Object festival) {
    return Intl.message(
      '$artist joins $festival',
      name: 'postTemplateOneArtist',
      desc: '',
      args: [artist, festival],
    );
  }

  /// `{artist1} and {artist2} join {festival}`
  String postTemplateTwoArtists(Object artist1, Object artist2, Object festival) {
    return Intl.message(
      '$artist1 and $artist2 join $festival',
      name: 'postTemplateTwoArtists',
      desc: '',
      args: [artist1, artist2, festival],
    );
  }

  /// `{artists} and more join {festival}`
  String postTemplateMoreArtists(Object artists, Object festival) {
    return Intl.message(
      '$artists and more join $festival',
      name: 'postTemplateMoreArtists',
      desc: '',
      args: [artists, festival],
    );
  }

  /// `Error communicating with the server`
  String get genericErrorBadRequest {
    return Intl.message(
      'Error communicating with the server',
      name: 'genericErrorBadRequest',
      desc: '',
      args: [],
    );
  }

  /// `Error communicating with the server`
  String get genericErrorServer {
    return Intl.message(
      'Error communicating with the server',
      name: 'genericErrorServer',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'es'),
      Locale.fromSubtags(languageCode: 'gl'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}