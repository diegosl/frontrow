// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static m0(min) => "Rules: 1 uppercase, 1 number and ${min} characters";

  static m1(min) => "The login has to contain more than ${min} characters";

  static m2(min) => "Rules: 1 uppercase, 1 number and ${min} characters";

  static m3(artists, festival) => "${artists} and more join ${festival}";

  static m4(artist, festival) => "${artist} joins ${festival}";

  static m5(artist1, artist2, festival) => "${artist1} and ${artist2} join ${festival}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "drawerLogoutTitle" : MessageLookupByLibrary.simpleMessage("Sign out"),
    "drawerMenuExplore" : MessageLookupByLibrary.simpleMessage("Explore"),
    "drawerMenuMain" : MessageLookupByLibrary.simpleMessage("News"),
    "drawerMenuMyFestivals" : MessageLookupByLibrary.simpleMessage("My festivals"),
    "drawerMenuTitle" : MessageLookupByLibrary.simpleMessage("Menu"),
    "drawerSettingsTitle" : MessageLookupByLibrary.simpleMessage("Settings"),
    "errorNetwork" : MessageLookupByLibrary.simpleMessage("Error communicating with the server"),
    "genericErrorBadRequest" : MessageLookupByLibrary.simpleMessage("Error communicating with the server"),
    "genericErrorServer" : MessageLookupByLibrary.simpleMessage("Error communicating with the server"),
    "locale" : MessageLookupByLibrary.simpleMessage("en"),
    "pageExploreNotFound" : MessageLookupByLibrary.simpleMessage("We couldn\'t find any festival matching that criteria."),
    "pageExploreTitle" : MessageLookupByLibrary.simpleMessage("Explore"),
    "pageFestivalArtists" : MessageLookupByLibrary.simpleMessage("Confirmed artists:"),
    "pageFestivalCancelled" : MessageLookupByLibrary.simpleMessage("Canceled"),
    "pageFestivalScheduleButton" : MessageLookupByLibrary.simpleMessage("Schedule"),
    "pageLoginBar" : MessageLookupByLibrary.simpleMessage("Login"),
    "pageLoginErrorAuthentication" : MessageLookupByLibrary.simpleMessage("Problem authenticating, verify your credentials"),
    "pageLoginLoginButton" : MessageLookupByLibrary.simpleMessage("Sign in"),
    "pageLoginRegisterButton" : MessageLookupByLibrary.simpleMessage("Register"),
    "pageLoginTitle" : MessageLookupByLibrary.simpleMessage("Frontrow"),
    "pageMainTitle" : MessageLookupByLibrary.simpleMessage("News"),
    "pageMyFestivalsNotFollowing" : MessageLookupByLibrary.simpleMessage("You aren\'t following any festivals"),
    "pageRegisterConfirmationPasswordValidationError" : m0,
    "pageRegisterErrorLoginExist" : MessageLookupByLibrary.simpleMessage("Login already taken"),
    "pageRegisterErrorMailExist" : MessageLookupByLibrary.simpleMessage("Email already exists"),
    "pageRegisterErrorPasswordNotIdentical" : MessageLookupByLibrary.simpleMessage("The passwords are not identical"),
    "pageRegisterFormConfirmPassword" : MessageLookupByLibrary.simpleMessage("Confirm password"),
    "pageRegisterFormEmail" : MessageLookupByLibrary.simpleMessage("Email"),
    "pageRegisterFormEmailHint" : MessageLookupByLibrary.simpleMessage("you@example.com"),
    "pageRegisterFormLogin" : MessageLookupByLibrary.simpleMessage("Login"),
    "pageRegisterFormPassword" : MessageLookupByLibrary.simpleMessage("Password"),
    "pageRegisterFormSubmit" : MessageLookupByLibrary.simpleMessage("Sign up"),
    "pageRegisterFormTermsConditions" : MessageLookupByLibrary.simpleMessage("I accept the terms of use"),
    "pageRegisterFormTermsConditionsNotChecked" : MessageLookupByLibrary.simpleMessage("Please accept the terms and conditions"),
    "pageRegisterLoginValidationError" : m1,
    "pageRegisterMailValidationError" : MessageLookupByLibrary.simpleMessage("Please enter a valid email address"),
    "pageRegisterPasswordValidationError" : m2,
    "pageRegisterSuccess" : MessageLookupByLibrary.simpleMessage("Congratulations"),
    "pageRegisterSuccessAltImg" : MessageLookupByLibrary.simpleMessage("Register successful"),
    "pageRegisterSuccessSub" : MessageLookupByLibrary.simpleMessage("You have successfully registered"),
    "pageRegisterTitle" : MessageLookupByLibrary.simpleMessage("Register"),
    "pageSearchLocation" : MessageLookupByLibrary.simpleMessage("Location"),
    "pageSearchRadio" : MessageLookupByLibrary.simpleMessage("Radius"),
    "pageSearchSearchButton" : MessageLookupByLibrary.simpleMessage("Search"),
    "pageSettingsEmailErrorValidation" : MessageLookupByLibrary.simpleMessage("Email format incorrect"),
    "pageSettingsFormEmail" : MessageLookupByLibrary.simpleMessage("Email"),
    "pageSettingsFormLanguages" : MessageLookupByLibrary.simpleMessage("Languages"),
    "pageSettingsFormSave" : MessageLookupByLibrary.simpleMessage("Save"),
    "pageSettingsSave" : MessageLookupByLibrary.simpleMessage("Settings saved!"),
    "pageSettingsTitle" : MessageLookupByLibrary.simpleMessage("Settings"),
    "postTemplateMoreArtists" : m3,
    "postTemplateOneArtist" : m4,
    "postTemplateTwoArtists" : m5
  };
}
