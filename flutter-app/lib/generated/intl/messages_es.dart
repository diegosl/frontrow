// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  static m0(min) => "Validaciones: 1 mayúscula, 1 número y ${min} caracteres";

  static m1(min) => "El nombre de usuario debe contener más de ${min} caracteres";

  static m2(min) => "Validaciones: 1 mayúscula, 1 número y ${min} caracteres";

  static m3(artists, festival) => "${artists} y más, al ${festival}";

  static m4(artist, festival) => "${artist}, al ${festival}";

  static m5(artist1, artist2, festival) => "${artist1} y ${artist2}, al ${festival}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "drawerLogoutTitle" : MessageLookupByLibrary.simpleMessage("Salir"),
    "drawerMenuExplore" : MessageLookupByLibrary.simpleMessage("Explorar"),
    "drawerMenuMain" : MessageLookupByLibrary.simpleMessage("Noticias"),
    "drawerMenuMyFestivals" : MessageLookupByLibrary.simpleMessage("Mis festivales"),
    "drawerMenuTitle" : MessageLookupByLibrary.simpleMessage("Menú"),
    "drawerSettingsTitle" : MessageLookupByLibrary.simpleMessage("Ajustes"),
    "errorNetwork" : MessageLookupByLibrary.simpleMessage("Error en la comunicación con el servidor"),
    "genericErrorBadRequest" : MessageLookupByLibrary.simpleMessage("Error en la comunicación con el servidor"),
    "genericErrorServer" : MessageLookupByLibrary.simpleMessage("Error en la comunicación con el servidor"),
    "locale" : MessageLookupByLibrary.simpleMessage("es"),
    "pageExploreNotFound" : MessageLookupByLibrary.simpleMessage("No encontramos ningún festival que cumpla los criterios."),
    "pageExploreTitle" : MessageLookupByLibrary.simpleMessage("Explorar"),
    "pageFestivalArtists" : MessageLookupByLibrary.simpleMessage("Artistas confirmados:"),
    "pageFestivalCancelled" : MessageLookupByLibrary.simpleMessage("Cancelado"),
    "pageFestivalScheduleButton" : MessageLookupByLibrary.simpleMessage("Horarios"),
    "pageLoginBar" : MessageLookupByLibrary.simpleMessage("Login"),
    "pageLoginErrorAuthentication" : MessageLookupByLibrary.simpleMessage("Problema al autenticar, verifica tus credenciales"),
    "pageLoginLoginButton" : MessageLookupByLibrary.simpleMessage("Entrar"),
    "pageLoginRegisterButton" : MessageLookupByLibrary.simpleMessage("Registro"),
    "pageLoginTitle" : MessageLookupByLibrary.simpleMessage("Frontrow"),
    "pageMainTitle" : MessageLookupByLibrary.simpleMessage("Noticias"),
    "pageMyFestivalsNotFollowing" : MessageLookupByLibrary.simpleMessage("No sigues ningún festival."),
    "pageRegisterConfirmationPasswordValidationError" : m0,
    "pageRegisterErrorLoginExist" : MessageLookupByLibrary.simpleMessage("Usuario no disponible"),
    "pageRegisterErrorMailExist" : MessageLookupByLibrary.simpleMessage("Email en uso"),
    "pageRegisterErrorPasswordNotIdentical" : MessageLookupByLibrary.simpleMessage("Contraseñas no coincidentes"),
    "pageRegisterFormConfirmPassword" : MessageLookupByLibrary.simpleMessage("Confirmar contraseña"),
    "pageRegisterFormEmail" : MessageLookupByLibrary.simpleMessage("Email"),
    "pageRegisterFormEmailHint" : MessageLookupByLibrary.simpleMessage("tu@ejemplo.es"),
    "pageRegisterFormLogin" : MessageLookupByLibrary.simpleMessage("Usuario"),
    "pageRegisterFormPassword" : MessageLookupByLibrary.simpleMessage("Contraseña"),
    "pageRegisterFormSubmit" : MessageLookupByLibrary.simpleMessage("Registro"),
    "pageRegisterFormTermsConditions" : MessageLookupByLibrary.simpleMessage("Acepto los términos de uso"),
    "pageRegisterFormTermsConditionsNotChecked" : MessageLookupByLibrary.simpleMessage("Por favor, acepta los términos y condiciones"),
    "pageRegisterLoginValidationError" : m1,
    "pageRegisterMailValidationError" : MessageLookupByLibrary.simpleMessage("Por favor, introduce un email correcto"),
    "pageRegisterPasswordValidationError" : m2,
    "pageRegisterSuccess" : MessageLookupByLibrary.simpleMessage("Enhorabuena"),
    "pageRegisterSuccessAltImg" : MessageLookupByLibrary.simpleMessage("Registro correcto"),
    "pageRegisterSuccessSub" : MessageLookupByLibrary.simpleMessage("Su cuenta se ha registrado correctamente"),
    "pageRegisterTitle" : MessageLookupByLibrary.simpleMessage("Registro"),
    "pageSearchLocation" : MessageLookupByLibrary.simpleMessage("Ubicación"),
    "pageSearchRadio" : MessageLookupByLibrary.simpleMessage("Radio"),
    "pageSearchSearchButton" : MessageLookupByLibrary.simpleMessage("Buscar"),
    "pageSettingsEmailErrorValidation" : MessageLookupByLibrary.simpleMessage("Formato de email incorrecto"),
    "pageSettingsFormEmail" : MessageLookupByLibrary.simpleMessage("Email"),
    "pageSettingsFormLanguages" : MessageLookupByLibrary.simpleMessage("Idiomas"),
    "pageSettingsFormSave" : MessageLookupByLibrary.simpleMessage("Guardar"),
    "pageSettingsSave" : MessageLookupByLibrary.simpleMessage("¡Ajustes guardados!"),
    "pageSettingsTitle" : MessageLookupByLibrary.simpleMessage("Ajustes"),
    "postTemplateMoreArtists" : m3,
    "postTemplateOneArtist" : m4,
    "postTemplateTwoArtists" : m5
  };
}
