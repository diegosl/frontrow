// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a gl locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'gl';

  static m0(min) => "Validacións: 1 maiúscula, 1 número e ${min} caracteres";

  static m1(min) => "O nome de usuario debe ter máis de ${min} caracteres";

  static m2(min) => "Validacións: 1 maiúscula, 1 número e ${min} caracteres";

  static m3(artists, festival) => "${artists} e máis, ao ${festival}";

  static m4(artist, festival) => "${artist}, ao ${festival}";

  static m5(artist1, artist2, festival) => "${artist1} e ${artist2}, ao ${festival}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "drawerLogoutTitle" : MessageLookupByLibrary.simpleMessage("Saír"),
    "drawerMenuExplore" : MessageLookupByLibrary.simpleMessage("Explorar"),
    "drawerMenuMain" : MessageLookupByLibrary.simpleMessage("Novas"),
    "drawerMenuMyFestivals" : MessageLookupByLibrary.simpleMessage("Os meus festivais"),
    "drawerMenuTitle" : MessageLookupByLibrary.simpleMessage("Menú"),
    "drawerSettingsTitle" : MessageLookupByLibrary.simpleMessage("Axustes"),
    "errorNetwork" : MessageLookupByLibrary.simpleMessage("Erro na comunicación co servidor"),
    "genericErrorBadRequest" : MessageLookupByLibrary.simpleMessage("Problema na comunicación co servidor"),
    "genericErrorServer" : MessageLookupByLibrary.simpleMessage("Problema na comunicación co servidor"),
    "locale" : MessageLookupByLibrary.simpleMessage("gl"),
    "pageExploreNotFound" : MessageLookupByLibrary.simpleMessage("Non atopamos ningún festival que cumpra os criterios."),
    "pageExploreTitle" : MessageLookupByLibrary.simpleMessage("Explorar"),
    "pageFestivalArtists" : MessageLookupByLibrary.simpleMessage("Artistas confirmados:"),
    "pageFestivalCancelled" : MessageLookupByLibrary.simpleMessage("Cancelado"),
    "pageFestivalScheduleButton" : MessageLookupByLibrary.simpleMessage("Horarios"),
    "pageLoginBar" : MessageLookupByLibrary.simpleMessage("Login"),
    "pageLoginErrorAuthentication" : MessageLookupByLibrary.simpleMessage("Problema ao autenticar, verifica as tuas credenciais"),
    "pageLoginLoginButton" : MessageLookupByLibrary.simpleMessage("Entrar"),
    "pageLoginRegisterButton" : MessageLookupByLibrary.simpleMessage("Rexistro"),
    "pageLoginTitle" : MessageLookupByLibrary.simpleMessage("Frontrow"),
    "pageMainTitle" : MessageLookupByLibrary.simpleMessage("Novas"),
    "pageMyFestivalsNotFollowing" : MessageLookupByLibrary.simpleMessage("Non estas a seguir ningún festival."),
    "pageRegisterConfirmationPasswordValidationError" : m0,
    "pageRegisterErrorLoginExist" : MessageLookupByLibrary.simpleMessage("Usuario non dispoñible"),
    "pageRegisterErrorMailExist" : MessageLookupByLibrary.simpleMessage("Email en uso"),
    "pageRegisterErrorPasswordNotIdentical" : MessageLookupByLibrary.simpleMessage("Contrasinais non concidintes"),
    "pageRegisterFormConfirmPassword" : MessageLookupByLibrary.simpleMessage("Confirmar contrasinal"),
    "pageRegisterFormEmail" : MessageLookupByLibrary.simpleMessage("Email"),
    "pageRegisterFormEmailHint" : MessageLookupByLibrary.simpleMessage("ti@exemplo.gal"),
    "pageRegisterFormLogin" : MessageLookupByLibrary.simpleMessage("Usuario"),
    "pageRegisterFormPassword" : MessageLookupByLibrary.simpleMessage("Contrasinal"),
    "pageRegisterFormSubmit" : MessageLookupByLibrary.simpleMessage("Rexistro"),
    "pageRegisterFormTermsConditions" : MessageLookupByLibrary.simpleMessage("Acepto os términos de uso"),
    "pageRegisterFormTermsConditionsNotChecked" : MessageLookupByLibrary.simpleMessage("Por favor, acepta os términos e condicións"),
    "pageRegisterLoginValidationError" : m1,
    "pageRegisterMailValidationError" : MessageLookupByLibrary.simpleMessage("Por favor, introduce un email correcto"),
    "pageRegisterPasswordValidationError" : m2,
    "pageRegisterSuccess" : MessageLookupByLibrary.simpleMessage("Noraboa"),
    "pageRegisterSuccessAltImg" : MessageLookupByLibrary.simpleMessage("Rexistro correcto"),
    "pageRegisterSuccessSub" : MessageLookupByLibrary.simpleMessage("A tua conta foi rexistrada correctamente"),
    "pageRegisterTitle" : MessageLookupByLibrary.simpleMessage("Rexistro"),
    "pageSearchLocation" : MessageLookupByLibrary.simpleMessage("Ubicación"),
    "pageSearchRadio" : MessageLookupByLibrary.simpleMessage("Radio"),
    "pageSearchSearchButton" : MessageLookupByLibrary.simpleMessage("Buscar"),
    "pageSettingsEmailErrorValidation" : MessageLookupByLibrary.simpleMessage("Formato de email incorrecto"),
    "pageSettingsFormEmail" : MessageLookupByLibrary.simpleMessage("Email"),
    "pageSettingsFormLanguages" : MessageLookupByLibrary.simpleMessage("Idiomas"),
    "pageSettingsFormSave" : MessageLookupByLibrary.simpleMessage("Gardar"),
    "pageSettingsSave" : MessageLookupByLibrary.simpleMessage("Axustes gardados!"),
    "pageSettingsTitle" : MessageLookupByLibrary.simpleMessage("Axustes"),
    "postTemplateMoreArtists" : m3,
    "postTemplateOneArtist" : m4,
    "postTemplateTwoArtists" : m5
  };
}
