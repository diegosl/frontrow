import 'package:flutter/widgets.dart';

class FrontrowKeys {
  static const loginScreen = Key('__loginScreen__');
  static const registerScreen = Key('__registerScreen__');
  static const mainScreen = Key('__mainScreen__');
  static const settingsScreen = Key('__settingsScreen__');
  static const searchScreen = Key('__searchScreen__');
  static const exploreScreen = Key('__exploreScreen__');
  static const festivalScreen = Key('__festivalScreen__');
  static const myfestivalsScreen = Key('__myfestivalsScreen__');
}
