class FrontrowRoutes {
  static final login = '/';
  static final register = '/register';
  static final main = '/main';
  static final settings = '/settings';
  static final explore = '/explore';
  static final search = '/search';
  static final festival = '/festival';
  static final myfestivals = '/myfestivals';
}
