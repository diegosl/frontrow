import 'dart:async';
import 'dart:convert';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:frontrow/account/login/login_repository.dart';
import 'package:frontrow/generated/l10n.dart';
import 'package:frontrow/keys.dart';
import 'package:frontrow/main/bloc/main_bloc.dart';
import 'package:frontrow/shared/models/post.dart';
import 'package:frontrow/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:frontrow/shared/widgets/drawer/drawer_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MainScreen extends StatefulWidget {
  @override
  State<MainScreen> createState() => _MainScreen();

  MainScreen({Key key}) : super(key: FrontrowKeys.mainScreen);
}

class _MainScreen extends State<MainScreen> {
  Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainBloc, MainState>(
      builder: (context, state) {
        return Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Text(S.of(context).pageMainTitle),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.refresh),
                  onPressed: () async {
                    BlocProvider.of<MainBloc>(context)
                        .add(NewsRefreshRequested());
                  },
                )
              ],
            ),
            body: body(context),
            drawer: BlocProvider<DrawerBloc>(
                create: (context) =>
                    DrawerBloc(loginRepository: LoginRepository()),
                child: FrontrowDrawer()));
      },
    );
  }

  Widget body(BuildContext context) {
    return SafeArea(child: postsList(context));
  }

  Widget postsList(BuildContext context) {
    return BlocConsumer<MainBloc, MainState>(listener: (context, state) {
      if (state is NewsLoadSuccess) {
        _refreshCompleter?.complete();
        _refreshCompleter = Completer();
      }
    }, builder: (context, state) {
      if (state is NewsLoadInProgress) {
        return Center(child: CircularProgressIndicator());
      } else if (state is NewsLoadSuccess) {
        final posts = state.posts;
        return RefreshIndicator(
          onRefresh: () {
            BlocProvider.of<MainBloc>(context).add(
              NewsRefreshRequested(),
            );
            return _refreshCompleter.future;
          },
          child: ListView(children: <Widget>[
            for (int index = 0; index < posts.length; index++)
              Card(
                clipBehavior: Clip.antiAlias,
                child: InkWell(
                  onTap: () {
                    print('Card was tapped');
                  },
                  splashColor:
                      Theme.of(context).colorScheme.onSurface.withOpacity(0.12),
                  highlightColor: Colors.transparent,
                  child: PostContent(post: posts[index]),
                ),
              ),
          ]),
        );
      } else {
        return Center(
          child: Text(
            S.of(context).errorNetwork,
            style: TextStyle(color: Theme.of(context).errorColor),
          ),
        );
      }
    });
  }
}

class PostContent extends StatelessWidget {
  const PostContent({Key key, @required this.post})
      : assert(post != null),
        super(key: key);

  final Post post;

  String getTitle(String template, String title, BuildContext context) {
    String title = post.title;
    if (post.template != null) {
      switch (post.template) {
        case "frontrowApp.post.templates.oneArtist":
          {
            title = S.of(context).postTemplateOneArtist(
                post.title, "${post.editionName} ${post.editionYear}");
          }
          break;

        case "frontrowApp.post.templates.twoArtists":
          {
            List<String> artists = post.title.split(',');
            title = S.of(context).postTemplateTwoArtists(artists[0].trim(),
                artists[1].trim(), "${post.editionName} ${post.editionYear}");
          }
          break;

        default:
          {
            title = S.of(context).postTemplateMoreArtists(
                post.title, "${post.editionName} ${post.editionYear}");
          }
          break;
      }
    }
    return title;
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final titleStyle = theme.textTheme.headline5.copyWith(color: Colors.white);
    final dateStyle = theme.textTheme.subtitle2.copyWith(color: Colors.white);
    final dateFormat = DateFormat(DateFormat.YEAR_ABBR_MONTH_DAY).add_Hm();
    final title = getTitle(post.template, post.title, context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 128,
          child: Stack(
            children: [
              Stack(
                children: [
                  Positioned.fill(
                    child: Ink.image(
                      image: MemoryImage(base64Decode(post.logo)),
                      fit: BoxFit.cover,
                      child: Container(),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.width,
                    width: MediaQuery.of(context).size.width,
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                      child: Container(color: Colors.black.withOpacity(0.5)),
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        child: AutoSizeText(
                          dateFormat.format(post.date),
                          style: dateStyle,
                          maxLines: 1,
                          textAlign: TextAlign.right,
                        ),
                      ),
                    ),
                  ),
                  Expanded(child: Container()),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: AutoSizeText(
                      title,
                      style: titleStyle,
                      maxLines: 2,
                      textAlign: TextAlign.justify,
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}
