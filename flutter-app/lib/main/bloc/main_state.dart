part of 'main_bloc.dart';

abstract class MainState extends Equatable {
  const MainState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class NewsLoadInProgress extends MainState {}

class NewsLoadSuccess extends MainState {
  final List<Post> posts;

  const NewsLoadSuccess({@required this.posts}) : assert(posts != null);

  @override
  List<Object> get props => [posts];
}

class NewsLoadFailure extends MainState {}
