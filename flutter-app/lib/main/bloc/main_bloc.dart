import 'dart:async';
import 'dart:ui';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:frontrow/generated/l10n.dart';
import 'package:frontrow/shared/models/post.dart';
import 'package:frontrow/shared/models/user.dart';
import 'package:frontrow/shared/repository/account_repository.dart';
import 'package:frontrow/shared/repository/post_repository.dart';

part 'main_events.dart';
part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  final AccountRepository _accountRepository;
  final PostRepository _postRepository;

  MainBloc({@required AccountRepository accountRepository, @required PostRepository postRepository}) : assert(accountRepository != null && postRepository != null),
        _accountRepository = accountRepository, _postRepository = postRepository, super(NewsLoadInProgress());

  @override
  void onTransition(Transition<MainEvent, MainState> transition) {
    super.onTransition(transition);
  }

  @override
  Stream<MainState> mapEventToState(MainEvent event) async* {
    if (event is Init) {
       yield* onInit(event);
       yield* onNewsRefreshRequested();
    }
    if (event is NewsRefreshRequested) {
        yield* onNewsRefreshRequested();
    }
  }

  Stream<MainState> onNewsRefreshRequested() async* {
      yield NewsLoadInProgress();
      try {
          final List<Post> posts = await _postRepository.getLatest(25);
          yield NewsLoadSuccess(posts: posts);
      } catch (_) {
          yield NewsLoadFailure();
      }
  }

  Stream<MainState> onInit(Init event) async* {
    User currentUser = await _accountRepository.getIdentity();

    if(currentUser.langKey.compareTo(S.current.locale) != 0) {
      S.load(Locale(currentUser.langKey));
    }
  }
}
