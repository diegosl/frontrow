import 'package:flutter/material.dart';
import 'package:frontrow/explore/bloc/explore_filter.dart';
import 'package:frontrow/explore/places_custom_autocomplete_field.dart';
import 'package:frontrow/generated/l10n.dart';
import 'package:frontrow/keys.dart';
import 'package:google_maps_webservice/places.dart';

class SearchScreen extends StatefulWidget {
  @override
  State<SearchScreen> createState() => _SearchScreenState();

  SearchScreen({Key key}) : super(key: FrontrowKeys.searchScreen);
}

class _SearchScreenState extends State<SearchScreen> {
  final TextEditingController _radioController = TextEditingController(text: "50");
  double latitude;
  double longitude;

  _getLatLng(prediction) async {
    if (prediction != null) {
      GoogleMapsPlaces _places = new GoogleMapsPlaces(apiKey: "AIzaSyBisAwzOze0y4pomCETSn3t0PLrE6jtkIg");
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(prediction.placeId, fields: ["geometry"]);
      latitude = detail.result.geometry.location.lat;
      longitude = detail.result.geometry.location.lng;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).pageExploreTitle),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Form(
            child: Wrap(
              runSpacing: 15,
              children: [
                Row(
                  children: [
                    Flexible(
                      flex: 2,
                      child: PlacesCustomAutocompleteField(
                          apiKey: "AIzaSyBisAwzOze0y4pomCETSn3t0PLrE6jtkIg",
                          hint: "A Coruña",
                          language: S.of(context).locale,
                          types: ["(cities)"],
                          inputDecoration: InputDecoration(labelText: S.of(context).pageSearchLocation),
                          onSelected: (prediction) {
                            _getLatLng(prediction);
                          }),
                    ),
                    SizedBox(width: 10),
                    Flexible(
                      flex: 1,
                      child: TextFormField(
                        controller: _radioController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: S.of(context).pageSearchRadio,
                          hintText: '20',
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: [
                    RaisedButton(
                      child: Container(
                          width: MediaQuery.of(context).size.width - 30 * MediaQuery.of(context).devicePixelRatio,
                          height: 50,
                          child: Center(
                            child: Text(S.of(context).pageSearchSearchButton.toUpperCase()),
                          )),
                      onPressed: () {
                        Navigator.pop(
                            context, new ExploreFilter(DateTime.now(), null, null, latitude, longitude, int.tryParse(_radioController.text)));
                      },
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
