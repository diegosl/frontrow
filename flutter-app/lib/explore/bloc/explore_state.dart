part of 'explore_bloc.dart';

abstract class ExploreState extends Equatable {
    const ExploreState();

    @override
    List<Object> get props => [];

    @override
    bool get stringify => true;
}

class EditionsLoadInProgress extends ExploreState {}

class EditionsLoadSuccess extends ExploreState {
    final List<Edition> editions;

    const EditionsLoadSuccess({@required this.editions}) : assert(editions != null);

    @override
    List<Object> get props => [editions];
}

class EditionsLoadFailure extends ExploreState {}
