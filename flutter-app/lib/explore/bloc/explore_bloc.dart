import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:frontrow/shared/models/edition.dart';
import 'package:frontrow/shared/repository/edition_repository.dart';
import 'explore_filter.dart';

part 'explore_events.dart';
part 'explore_state.dart';

class ExploreBloc extends Bloc<ExploreEvent, ExploreState> {
    final EditionRepository _editionRepository;

    ExploreBloc({@required EditionRepository editionRepository}) : assert(editionRepository != null),
            _editionRepository = editionRepository, super(EditionsLoadInProgress());

    @override
    void onTransition(Transition<ExploreEvent, ExploreState> transition) {
        super.onTransition(transition);
    }

    @override
    Stream<ExploreState> mapEventToState(ExploreEvent event) async* {
        if (event is ExploreRequested) {
            yield* onExploreRequested(event);
        }
    }

    Stream<ExploreState> onExploreRequested(ExploreRequested event) async* {
        yield EditionsLoadInProgress();
        try {
            final List<Edition> editions = await _editionRepository.getFiltered(event.filters);
            yield EditionsLoadSuccess(editions: editions);
        } catch (_) {
            yield EditionsLoadFailure();
        }
    }
}
