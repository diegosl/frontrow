class ExploreFilter {
  DateTime fromDate;
  DateTime toDate;
  List<int> artists;
  double latitude;
  double longitude;
  int radio;

  ExploreFilter(this.fromDate, this.toDate, this.artists, this.latitude,
      this.longitude, this.radio);
}
