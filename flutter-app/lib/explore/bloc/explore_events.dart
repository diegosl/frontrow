part of 'explore_bloc.dart';

abstract class ExploreEvent extends Equatable {
    const ExploreEvent();

    @override
    List<Object> get props => [];

    @override
    bool get stringify => true;
}

class ExploreRequested extends ExploreEvent {
    final ExploreFilter filters;

    const ExploreRequested({@required this.filters}) : assert(filters != null);

    @override
    List<Object> get props => [filters];
}
