import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:frontrow/account/login/login_repository.dart';
import 'package:frontrow/myfestivals/bloc/myfestivals_bloc.dart';
import 'package:frontrow/generated/l10n.dart';
import 'package:frontrow/keys.dart';
import 'package:frontrow/routes.dart';
import 'package:frontrow/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:frontrow/shared/widgets/drawer/drawer_widget.dart';
import 'package:flutter/material.dart';
import 'package:frontrow/shared/widgets/edition_content.dart';

class MyFestivalsScreen extends StatelessWidget {
    MyFestivalsScreen({Key key}) : super(key: FrontrowKeys.myfestivalsScreen);

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                centerTitle: true,
                title: Text(S.of(context).drawerMenuMyFestivals),
            ),
            body: body(context),
            drawer: BlocProvider<DrawerBloc>(create: (context) => DrawerBloc(loginRepository: LoginRepository()), child: FrontrowDrawer()));
    }

    Widget body(BuildContext context) {
        return SafeArea(child: editionsList(context));
    }

    Widget editionsList(BuildContext context) {
        return BlocBuilder<MyFestivalsBloc, MyFestivalsState>(builder: (context, state) {
            if (state is EditionsLoadInProgress) {
                return Center(child: CircularProgressIndicator());
            } else if (state is EditionsLoadSuccess) {
                final editions = state.editions;
                if (editions.isEmpty) {
                    return Center(
                        child: Text(
                            S.of(context).pageMyFestivalsNotFollowing,
                            style: TextStyle(color: Theme.of(context).errorColor),
                        ));
                } else {
                    return ListView(children: <Widget>[
                        for (int index = 0; index < editions.length; index++)
                            Card(
                                clipBehavior: Clip.antiAlias,
                                child: InkWell(
                                    onTap: () => Navigator.pushNamed(context, FrontrowRoutes.festival, arguments: editions[index]),
                                    splashColor: Theme.of(context).colorScheme.onSurface.withOpacity(0.12),
                                    highlightColor: Colors.transparent,
                                    child: EditionContent(edition: editions[index]),
                                ),
                            ),
                    ]);
                }
            } else {
                return Center(
                    child: Text(
                        S.of(context).errorNetwork,
                        style: TextStyle(color: Theme.of(context).errorColor),
                    ),
                );
            }
        });
    }
}
