import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:frontrow/shared/models/edition.dart';
import 'package:frontrow/shared/repository/edition_repository.dart';

part 'myfestivals_events.dart';
part 'myfestivals_state.dart';

class MyFestivalsBloc extends Bloc<MyFestivalsEvent, MyFestivalsState> {
    final EditionRepository _editionRepository;

    MyFestivalsBloc({@required EditionRepository editionRepository}) : assert(editionRepository != null),
            _editionRepository = editionRepository, super(EditionsLoadInProgress());

    @override
    void onTransition(Transition<MyFestivalsEvent, MyFestivalsState> transition) {
        super.onTransition(transition);
    }

    @override
    Stream<MyFestivalsState> mapEventToState(MyFestivalsEvent event) async* {
        if (event is MyFestivalsInit) {
            yield* onInit();
        }
    }

    Stream<MyFestivalsState> onInit() async* {
        yield EditionsLoadInProgress();
        try {
            final List<Edition> editions = await _editionRepository.getFollowed(25);
            yield EditionsLoadSuccess(editions: editions);
        } catch (_) {
            yield EditionsLoadFailure();
        }
    }
}
