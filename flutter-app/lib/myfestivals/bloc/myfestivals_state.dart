part of 'myfestivals_bloc.dart';

abstract class MyFestivalsState extends Equatable {
    const MyFestivalsState();

    @override
    List<Object> get props => [];

    @override
    bool get stringify => true;
}

class EditionsLoadInProgress extends MyFestivalsState {}

class EditionsLoadSuccess extends MyFestivalsState {
    final List<Edition> editions;

    const EditionsLoadSuccess({@required this.editions}) : assert(editions != null);

    @override
    List<Object> get props => [editions];
}

class EditionsLoadFailure extends MyFestivalsState {}
