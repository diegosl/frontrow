part of 'myfestivals_bloc.dart';

abstract class MyFestivalsEvent extends Equatable {
    const MyFestivalsEvent();

    @override
    List<Object> get props => [];

    @override
    bool get stringify => true;
}

class MyFestivalsInit extends MyFestivalsEvent {}
