import 'dart:convert';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:frontrow/generated/l10n.dart';
import 'package:frontrow/shared/models/edition.dart';
import 'package:intl/intl.dart';

class EditionContent extends StatelessWidget {
  const EditionContent({Key key, @required this.edition})
      : assert(edition != null),
        super(key: key);

  final Edition edition;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final titleStyle = theme.textTheme.headline2.copyWith(color: Colors.white);
    final dateStyle = theme.textTheme.subtitle2.copyWith(color: Colors.white);
    final dateFormat = DateFormat(DateFormat.ABBR_MONTH_DAY);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 128,
          child: Stack(
            children: [
              Stack(
                children: [
                  Positioned.fill(
                    child: Ink.image(
                      image: MemoryImage(base64Decode(edition.logo)),
                      fit: BoxFit.cover,
                      child: Container(),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.width,
                    width: MediaQuery.of(context).size.width,
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                      child: Container(color: Colors.black.withOpacity(0.5)),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    if (edition.canceled != null && edition.canceled)
                      Center(
                        child: Chip(
                          avatar: Icon(
                            Icons.error_outline,
                            color: Colors.white,
                          ),
                          backgroundColor: Theme.of(context).errorColor.withOpacity(0.9),
                          label: Text(
                            S.of(context).pageFestivalCancelled,
                            style: Theme.of(context).textTheme.bodyText2,
                          ),
                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        ),
                      ),
                    if (edition.canceled != null && edition.canceled) SizedBox(height: 5.0),
                    Center(
                      child: AutoSizeText(
                        "${edition.festivalName} ${edition.startDate.year}",
                        style: titleStyle,
                        maxLines: 2,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 8.0),
                    Center(
                      child: AutoSizeText(
                        "${dateFormat.format(edition.startDate)} - ${dateFormat.format(edition.endDate)}",
                        style: dateStyle,
                        maxLines: 1,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
