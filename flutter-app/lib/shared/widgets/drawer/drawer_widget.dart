import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:frontrow/generated/l10n.dart';
import 'package:frontrow/routes.dart';
import 'package:frontrow/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:flutter/material.dart';

class FrontrowDrawer extends StatelessWidget {
  FrontrowDrawer({Key key}) : super(key: key);

  static final double iconSize = 30;

  @override
  Widget build(BuildContext context) {
    return BlocListener<DrawerBloc, DrawerState>(
      listener: (context, state) {
        if (state.isLogout) {
          Navigator.popUntil(context, ModalRoute.withName(FrontrowRoutes.login));
          Navigator.pushNamed(context, FrontrowRoutes.login);
        }
      },
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            header(context),
            ListTile(
              leading: Icon(
                Icons.explore,
                size: iconSize,
              ),
              title: Text(S.of(context).drawerMenuExplore),
              onTap: () => Navigator.pushNamed(context, FrontrowRoutes.explore),
            ),
            ListTile(
              leading: Icon(
                Icons.star,
                size: iconSize,
              ),
              title: Text(S.of(context).drawerMenuMyFestivals),
              onTap: () => Navigator.pushNamed(context, FrontrowRoutes.myfestivals),
            ),
            ListTile(
              leading: Icon(
                Icons.new_releases,
                size: iconSize,
              ),
              title: Text(S.of(context).drawerMenuMain),
              onTap: () => Navigator.pushNamed(context, FrontrowRoutes.main),
            ),
            ListTile(
              leading: Icon(
                Icons.settings,
                size: iconSize,
              ),
              title: Text(S.of(context).drawerSettingsTitle),
              onTap: () => Navigator.pushNamed(context, FrontrowRoutes.settings),
            ),
            ListTile(
                leading: Icon(
                  Icons.exit_to_app,
                  size: iconSize,
                ),
                title: Text(S.of(context).drawerLogoutTitle),
                onTap: () => context.bloc<DrawerBloc>().add(Logout()))
          ],
        ),
      ),
    );
  }

  Widget header(BuildContext context) {
    return DrawerHeader(
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
      ),
      child: Text(
        S.of(context).drawerMenuTitle,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.headline2,
      ),
    );
  }
}
