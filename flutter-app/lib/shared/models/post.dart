import 'package:dart_json_mapper/dart_json_mapper.dart';

@jsonSerializable
class Post {
  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'date')
  final DateTime date;

  @JsonProperty(name: 'title')
  final String title;

  @JsonProperty(name: 'handle')
  final String handle;

  @JsonProperty(name: 'template')
  final String template;

  @JsonProperty(name: 'logo')
  final String logo;

  @JsonProperty(name: 'logoContentType')
  final String logoContentType;

  @JsonProperty(name: 'editionName')
  final String editionName;

  @JsonProperty(name: 'editionYear')
  final int editionYear;

  @JsonProperty(name: 'content')
  final String content;

  @JsonProperty(name: 'createdBy')
  final String createdBy;

  const Post(
      this.id,
      this.title,
      this.date,
      this.template,
      this.logo,
      this.logoContentType,
      this.content,
      this.handle,
      this.editionName,
      this.editionYear,
      this.createdBy);

  @override
  String toString() {
    return 'Post{id: $id, title: $title, template: $template}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Post && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode ^ title.hashCode;
}
