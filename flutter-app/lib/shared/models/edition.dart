import 'package:dart_json_mapper/dart_json_mapper.dart';

import 'artist.dart';

@jsonSerializable
class Edition {
  @JsonProperty(name: 'id')
  final int id;

  @JsonProperty(name: 'startDate')
  final DateTime startDate;

  @JsonProperty(name: 'endDate')
  final DateTime endDate;

  @JsonProperty(name: 'festivalName')
  final String festivalName;

  @JsonProperty(name: 'location')
  final String location;

  @JsonProperty(name: 'spotifyPlaylist')
  final String spotifyPlaylist;

  @JsonProperty(name: 'canceled')
  final bool canceled;

  @JsonProperty(name: 'followed')
  final bool followed;

  @JsonProperty(name: 'price')
  final String price;

  @JsonProperty(name: 'schedule')
  final String schedule;

  @JsonProperty(name: 'poster')
  final String poster;

  @JsonProperty(name: 'posterContentType')
  final String posterContentType;

  @JsonProperty(name: 'logo')
  final String logo;

  @JsonProperty(name: 'logoContentType')
  final String logoContentType;

  @JsonProperty(name: 'web')
  final String web;

  @JsonProperty(name: 'facebook')
  final String facebook;

  @JsonProperty(name: 'instagram')
  final String instagram;

  @JsonProperty(name: 'twitter')
  final String twitter;

  @JsonProperty(name: 'artists')
  final List<Artist> artists;

  const Edition(
      this.id,
      this.startDate,
      this.endDate,
      this.festivalName,
      this.location,
      this.spotifyPlaylist,
      this.canceled,
      this.followed,
      this.price,
      this.schedule,
      this.poster,
      this.posterContentType,
      this.logo,
      this.logoContentType,
      this.web,
      this.facebook,
      this.instagram,
      this.twitter,
      this.artists);

  @override
  String toString() {
    return 'Edition{id: $id, festivalName: $festivalName, startDate: $startDate}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Edition && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode ^ festivalName.hashCode;
}
