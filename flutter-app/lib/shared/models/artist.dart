import 'package:dart_json_mapper/dart_json_mapper.dart';

@jsonSerializable
class Artist {
    @JsonProperty(name: 'id')
    final int id;

    @JsonProperty(name: 'name')
    final String name;

    const Artist(this.id, this.name);

    @override
    String toString() {
        return 'Artist{id: $id, name: $name}';
    }

    @override
    bool operator ==(Object other) =>
        identical(this, other) ||
            other is Artist && runtimeType == other.runtimeType &&
                id == other.id;

    @override
    int get hashCode => id.hashCode ^ name.hashCode;
}
