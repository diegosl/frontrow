import 'dart:convert';

import 'package:dart_json_mapper/dart_json_mapper.dart';
import 'package:frontrow/explore/bloc/explore_filter.dart';
import 'package:frontrow/shared/models/edition.dart';
import 'package:frontrow/shared/repository/http_utils.dart';
import 'package:intl/intl.dart';

class EditionRepository {
  EditionRepository();

  Future<List<Edition>> getFiltered(ExploreFilter filters) async {
    final dateFormat = DateFormat('yyyy-MM-dd');
    var endpoint = new StringBuffer("/editions/filtered?page=0&size=50");
    if (filters.longitude != null && filters.latitude != null && filters.radio != null) {
      endpoint.write("&latitude=${filters.latitude}&longitude=${filters.longitude}&radio=${filters.radio}");
    }
    if (filters.fromDate != null) {
      endpoint.write("&fromDate=${dateFormat.format(filters.fromDate)}");
    }
    if (filters.toDate != null) {
      endpoint.write("&toDate=${dateFormat.format(filters.toDate)}");
    }
    if (filters.artists != null && filters.artists.isNotEmpty) {
      endpoint.write("&artists=${filters.artists.join(",")}");
    }

    final exploreRequest = await HttpUtils.getRequest(endpoint.toString());

    if (exploreRequest.statusCode != 200) {
      throw Exception('error getting filtered editions');
    }

    String body = Utf8Decoder().convert(exploreRequest.bodyBytes);
    List<Edition> editions = JsonMapper.deserialize<List<Edition>>(body);
    return editions;
  }

  Future<List<Edition>> getFollowed(int number) async {
    final followedRequest = await HttpUtils.getRequest("/editions/followed/$number");

    if (followedRequest.statusCode != 200) {
      throw Exception('error getting followed editions');
    }

    String body = Utf8Decoder().convert(followedRequest.bodyBytes);
    List<Edition> editions = JsonMapper.deserialize<List<Edition>>(body);

    return editions;
  }
}
