import 'dart:convert';

import 'package:dart_json_mapper/dart_json_mapper.dart';
import 'package:frontrow/shared/models/post.dart';
import 'package:frontrow/shared/repository/http_utils.dart';

class PostRepository {
  PostRepository();

  Future<List<Post>> getLatest(int number) async {
    final latestRequest = await HttpUtils.getRequest("/posts/latest/$number");

    if (latestRequest.statusCode != 200) {
        throw Exception('error getting latest news');
    }

    String body = Utf8Decoder().convert(latestRequest.bodyBytes);
    List<Post> posts = JsonMapper.deserialize<List<Post>>(body);

    return posts;
  }
}
