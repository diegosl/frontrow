import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:frontrow/account/login/login_repository.dart';
import 'package:frontrow/generated/l10n.dart';
import 'package:frontrow/keys.dart';
import 'package:frontrow/shared/models/edition.dart';
import 'package:frontrow/shared/widgets/drawer/bloc/drawer_bloc.dart';
import 'package:frontrow/shared/widgets/drawer/drawer_widget.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class FestivalScreen extends StatelessWidget {
  FestivalScreen({Key key}) : super(key: FrontrowKeys.festivalScreen);

  @override
  Widget build(BuildContext context) {
    final Edition edition = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("${edition.festivalName} ${edition.startDate.year}"),
        ),
        body: body(context, edition),
        drawer: BlocProvider<DrawerBloc>(create: (context) => DrawerBloc(loginRepository: LoginRepository()), child: FrontrowDrawer()));
  }

  Widget body(BuildContext context, Edition edition) {
    return SafeArea(child: editionDetail(context, edition));
  }

  Widget editionDetail(BuildContext context, Edition edition) {
    final titleStyle = Theme.of(context).textTheme.headline5.copyWith(color: Colors.black);
    final textStyle = Theme.of(context).textTheme.subtitle1.copyWith(color: Colors.black);
    final dateFormat = DateFormat(DateFormat.ABBR_MONTH_DAY);

    _launchURL(String url) async {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }

    return SingleChildScrollView(
        child: Padding(
      padding: const EdgeInsets.all(15.0),
      child: Wrap(runSpacing: 15, children: [
        if (edition.canceled != null && edition.canceled)
          Row(
            children: [
              Expanded(
                child: Chip(
                  avatar: Icon(
                    Icons.error_outline,
                    color: Colors.white,
                  ),
                  backgroundColor: Theme.of(context).errorColor,
                  label: Text(
                    S.of(context).pageFestivalCancelled,
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ),
              ),
            ],
          ),
        Row(
          children: [
            Flexible(
              flex: 1,
              child: Image(
                image: MemoryImage(base64Decode(edition.logo)),
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(width: 8),
            Flexible(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AutoSizeText(
                    "${edition.festivalName} ${edition.startDate.year}",
                    style: titleStyle,
                    maxLines: 2,
                  ),
                  Row(
                    children: [
                      Icon(Icons.today, color: Colors.black),
                      Expanded(
                          child: Text("${dateFormat.format(edition.startDate)} - ${dateFormat.format(edition.endDate)}", style: textStyle)),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(Icons.location_on, color: Colors.black),
                      Expanded(child: Text(edition.location ?? "-", style: textStyle, overflow: TextOverflow.ellipsis))
                    ],
                  ),
                  Row(
                    children: [Icon(Icons.local_offer, color: Colors.black), Expanded(child: Text(edition.price ?? "-", style: textStyle))],
                  ),
                ],
              ),
            )
          ],
        ),
        Row(
          children: [
            Expanded(
              child: RaisedButton(
                onPressed: edition.schedule != null ? () => _launchURL(edition.schedule) : null,
                child: Center(
                  child: Text(S.of(context).pageFestivalScheduleButton.toUpperCase()),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: Image(
                image: MemoryImage(base64Decode(edition.poster)),
                fit: BoxFit.cover,
              ),
            )
          ],
        ),
        Row(
          children: [
            Expanded(
              child: RaisedButton(
                onPressed: edition.spotifyPlaylist != null ? () => _launchURL(edition.spotifyPlaylist) : null,
                child: Center(
                  child: Text("PLAYLIST"),
                ),
              ),
            ),
          ],
        ),
        Row(children: [
          Flexible(
            flex: 1,
            child: RaisedButton(
              onPressed: edition.web != null ? () => _launchURL(edition.web) : null,
              child: Center(
                child: Text("WEB"),
              ),
            ),
          ),
          SizedBox(
            width: 8,
          ),
          Flexible(
            flex: 1,
            child: RaisedButton(
              onPressed: edition.facebook != null ? () => _launchURL(edition.facebook) : null,
              child: Center(
                child: Text("FACEBOOK"),
              ),
            ),
          ),
        ]),
        Row(
          children: [
            Flexible(
              flex: 1,
              child: RaisedButton(
                onPressed: edition.twitter != null ? () => _launchURL(edition.twitter) : null,
                child: Center(
                  child: Text("TWITTER"),
                ),
              ),
            ),
            SizedBox(
              width: 8,
            ),
            Flexible(
              flex: 1,
              child: RaisedButton(
                onPressed: edition.instagram != null ? () => _launchURL(edition.instagram) : null,
                child: Center(
                  child: Text("INSTAGRAM"),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Expanded(
              child: Column(
                children: <Widget>[
                  Center(
                      child: Text(
                    S.of(context).pageFestivalArtists,
                    style: titleStyle,
                  )),
                  SizedBox(height: 8),
                  Text(
                    "${edition.artists.map((e) => e.name).join(", ")}.",
                    style: textStyle,
                    textAlign: TextAlign.justify,
                  )
                ],
              ),
            ),
          ],
        )
      ]),
    ));
  }
}
