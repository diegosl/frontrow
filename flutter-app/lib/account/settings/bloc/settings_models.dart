import 'package:formz/formz.dart';
import 'package:frontrow/generated/l10n.dart';

enum EmailValidationError { invalid }

extension EmailValidationErrorX on EmailValidationError {
  String get invalidMessage => S.current.pageSettingsEmailErrorValidation;
}

class EmailInput extends FormzInput<String, EmailValidationError> {
  const EmailInput.pure() : super.pure('');

  const EmailInput.dirty([String value = '']) : super.dirty(value);

  @override
  EmailValidationError validator(String value) {
    return value.contains('@') ? null : EmailValidationError.invalid;
  }
}
