part of 'settings_bloc.dart';

enum SettingsAction { none, reloadForLanguage }

class SettingsState extends Equatable {
  final EmailInput email;
  final String language;
  final FormzStatus formStatus;
  final SettingsAction action;
  final String generalNotificationKey;
  final User currentUser;

  const SettingsState(
      {this.email = const EmailInput.pure(),
      this.language = 'en',
      this.action = SettingsAction.none,
      this.formStatus = FormzStatus.pure,
      this.generalNotificationKey = HttpUtils.generalNoErrorKey,
      this.currentUser = const User('', '', '', '')});

  SettingsState copyWith(
      {EmailInput email, String language, FormzStatus status, String generalNotificationKey, SettingsAction action, User currentUser}) {
    return SettingsState(
        email: email ?? this.email,
        language: language ?? this.language,
        formStatus: status ?? this.formStatus,
        generalNotificationKey: generalNotificationKey ?? this.generalNotificationKey,
        action: action ?? this.action,
        currentUser: currentUser ?? this.currentUser);
  }

  @override
  List<Object> get props => [email, language, formStatus, generalNotificationKey];

  @override
  bool get stringify => true;
}
