import 'package:formz/formz.dart';
import 'package:frontrow/generated/l10n.dart';

enum LoginValidationError { invalid }

extension LoginValidationErrorX on LoginValidationError {
  String get invalidMessage => S.current.pageRegisterLoginValidationError(LoginInput.numberMin);
}

class LoginInput extends FormzInput<String, LoginValidationError> {
  const LoginInput.pure() : super.pure('');
  const LoginInput.dirty([String value = '']) : super.dirty(value);

  static final int numberMin = 3;

  @override
  LoginValidationError validator(String value) {
    return value.length >= 3 ? null : LoginValidationError.invalid;
  }
}

enum PasswordValidationError { invalid }

extension PasswordValidationErrorX on PasswordValidationError {
  String get invalidMessage => S.current.pageRegisterPasswordValidationError(PasswordInput.numberMin);
}

class PasswordInput extends FormzInput<String, PasswordValidationError> {
  const PasswordInput.pure() : super.pure('');
  const PasswordInput.dirty([String value = '']) : super.dirty(value);

  static final int numberMin = 3;

  @override
  PasswordValidationError validator(String value) {
      return value.length >= 3 ? null : PasswordValidationError.invalid;
  }
}
